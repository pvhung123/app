-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 24, 2014 at 04:29 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `tests`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `admin_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `address` varchar(255) NOT NULL,
  `phone` varchar(20) NOT NULL,
  PRIMARY KEY (`admin_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `name`, `email`, `password`, `address`, `phone`) VALUES
(3, 'Hung', 'pvhung123@gmail.com', '4cb9c8a8048fd02294477fcb1a41191a', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`) VALUES
(1, 'online'),
(2, 'thi chung');

-- --------------------------------------------------------

--
-- Table structure for table `exam`
--

CREATE TABLE IF NOT EXISTS `exam` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text COLLATE utf8_unicode_ci,
  `category_id` smallint(1) NOT NULL,
  `expired_date` datetime DEFAULT NULL,
  `subject_id` int(11) DEFAULT NULL,
  `level_id` int(11) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `lesson_id` int(11) DEFAULT NULL,
  `number_of_question` smallint(3) DEFAULT NULL,
  `time_to_test` time DEFAULT NULL,
  `created_date` int(9) DEFAULT NULL,
  `updated_date` int(9) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_exam` (`category_id`,`level_id`,`subject_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `exam`
--

INSERT INTO `exam` (`id`, `title`, `category_id`, `expired_date`, `subject_id`, `level_id`, `type_id`, `lesson_id`, `number_of_question`, `time_to_test`, `created_date`, `updated_date`, `is_active`) VALUES
(1, 'Kỳ thi 1', 1, '2014-11-05 00:00:00', 1, 1, NULL, NULL, 10, '00:00:15', 1416563322, 1416563322, 1);

-- --------------------------------------------------------

--
-- Table structure for table `examdetail`
--

CREATE TABLE IF NOT EXISTS `examdetail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exam_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `result` smallint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_examdetail` (`exam_id`,`user_id`,`question_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE IF NOT EXISTS `feedback` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `title` text COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `created_date` int(9) NOT NULL,
  `updated_date` int(9) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `language`
--

CREATE TABLE IF NOT EXISTS `language` (
  `phrase_id` int(11) NOT NULL AUTO_INCREMENT,
  `phrase` longtext COLLATE utf8_unicode_ci NOT NULL,
  `english` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`phrase_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=909 ;

--
-- Dumping data for table `language`
--

INSERT INTO `language` (`phrase_id`, `phrase`, `english`) VALUES
(1, 'Signin', 'Signin'),
(2, 'Signup', 'Signup'),
(3, 'Admin_log_in', 'Admin log in'),
(4, 'Toggle_navigation', 'Toggle navigation'),
(5, 'Home', 'Home'),
(6, 'Category', 'Category'),
(7, 'Featured_Items', 'Featured Items'),
(8, 'Latest_Items', 'Latest Items'),
(9, 'Contact', 'Contact'),
(10, 'Search_a_product', 'Search a product'),
(11, 'All_categories', 'All categories'),
(12, 'search', 'search'),
(13, 'Post_an_advertise', 'Post an advertise'),
(14, 'Featured_products', 'Featured products'),
(15, 'View', 'View'),
(16, 'Categories', 'Categories'),
(17, 'Search_products', 'Search products'),
(18, 'Search_for_products', 'Search for products'),
(19, 'Search', 'Search'),
(20, 'Most_viewed', 'Most viewed'),
(21, 'Latest_product', 'Latest product'),
(22, 'View_detail', 'View detail'),
(23, 'View_all_advertisement', 'View all advertisement'),
(24, 'terms_&_conditions', 'terms and conditions'),
(28, 'login', 'login'),
(29, 'email', 'email'),
(30, 'password', 'password'),
(31, 'forgot_password ?', 'forgot password ?'),
(32, 'reset_password', 'reset password'),
(33, 'reset', 'reset'),
(39, 'Product_page', 'Product page'),
(40, 'E-Mail', 'E-Mail'),
(41, 'Phone', 'Phone'),
(42, 'Address', 'Address'),
(43, 'Name', 'Name'),
(44, 'Model', 'Model'),
(45, 'Tags', 'Tags'),
(46, 'Condition', 'Condition'),
(47, 'Price', 'Price'),
(48, 'Description', 'Description'),
(49, 'Comment', 'Comment'),
(50, 'Zoom', 'Zoom'),
(52, 'Send_us_a_message', 'Send us a message'),
(53, 'Email', 'Email'),
(54, 'Subject', 'Subject'),
(55, 'Message', 'Message'),
(56, 'Send_message', 'Send message'),
(57, 'Business_info', 'Business info'),
(61, 'Please,_sign_in_to_your_account', 'Please, sign in to your account'),
(63, 'Enter_your_email', 'Enter your email'),
(64, 'Password', 'Password'),
(65, 'Enter_your_password', 'Enter your password'),
(66, 'Sign_in', 'Sign in'),
(67, 'Create_a_new_account', 'Create a new account'),
(68, 'Full_name', 'Full name'),
(69, 'Enter_your_name', 'Enter your name'),
(71, 'Enter_email_address', 'Enter email address'),
(72, 'Password', 'Password'),
(73, 'Confirm_password', 'Confirm password'),
(74, 'Register', 'Register'),
(76, 'Password', 'Password'),
(78, 'Password', 'Password'),
(79, 'Profile', 'Profile'),
(80, 'Log_out', 'Log out'),
(83, 'Edit_info', 'Edit info'),
(85, 'Password', 'Password'),
(86, 'Edit', 'Edit'),
(87, 'Uploaded_products', 'Uploaded products'),
(88, 'image', 'image'),
(89, 'Product name', 'Product name'),
(91, 'Password', 'Password'),
(92, 'Welcome_to', 'Welcome to'),
(93, 'Post_your_advetise', 'Post your advetise'),
(94, 'Please_fill_your_personal_information_for_contact_with_customer', 'Please fill your personal information for contact with customer'),
(96, 'Enter_your_email_address', 'Enter your email address'),
(97, 'Phone_number', 'Phone number'),
(98, 'Enter_your_phone_number', 'Enter your phone number'),
(99, 'Enter_your_address', 'Enter your address'),
(100, 'Please_fill_your_product_information', 'Please fill your product information'),
(101, 'select_category', 'select category'),
(102, 'Sub_category', 'Sub category'),
(103, 'select_sub_category', 'select sub category'),
(104, 'Product_title', 'Product title'),
(105, 'Product_model', 'Product model'),
(106, 'Product_description', 'Product description'),
(107, 'Enter_your_product_description', 'Enter your product description'),
(108, 'comma_seperated_tags', 'comma seperated tags'),
(109, 'Product_images', 'Product images'),
(110, 'delete', 'delete'),
(111, 'Product_condition', 'Product condition'),
(112, 'Select_product_condition', 'Select product condition'),
(113, 'Product_price', 'Product price'),
(114, 'I_accept_the', 'I accept the'),
(115, 'terms_and_conditions ', 'terms and conditions '),
(116, 'of_this_website.', 'of this website.'),
(117, 'Post_the_add', 'Post the add'),
(121, 'New', 'New'),
(122, 'Used', 'Used'),
(123, 'terms_and_conditions', 'terms and conditions'),
(126, 'Search_result_for', 'Search result for'),
(128, 'admin_dashboard', 'admin dashboard'),
(129, 'account', 'account'),
(131, 'change_password', 'change password'),
(132, 'logout', 'logout'),
(133, 'dashboard', 'dashboard'),
(134, 'users', 'users'),
(135, 'categories', 'categories'),
(136, 'sub_categories', 'sub categories'),
(137, 'all_product', 'all product'),
(138, 'home_page', 'home page'),
(139, 'contact', 'contact'),
(140, 'header_text', 'header text'),
(141, 'footer_text', 'footer text'),
(142, 'social_links', 'social links'),
(143, 'general_settings', 'general settings'),
(144, 'language_settings', 'language settings'),
(145, 'logo_settings', 'logo settings'),
(146, 'profile_help', 'profile help'),
(148, 'home', 'home'),
(149, 'user', 'user'),
(150, 'product', 'product'),
(151, 'frontend', 'frontend'),
(152, 'category', 'category'),
(153, 'settings', 'settings'),
(158, 'home', 'home'),
(160, 'manage_users', 'manage users'),
(165, 'approved_users', 'approved users'),
(166, 'name', 'name'),
(167, 'phone', 'phone'),
(168, 'address', 'address'),
(169, 'options', 'options'),
(197, 'phone', 'phone'),
(198, 'address', 'address'),
(222, 'phone', ''),
(223, 'address', ''),
(225, 'phone', ''),
(226, 'address', ''),
(227, 'edit_users', ''),
(229, 'phone', ''),
(230, 'address', ''),
(231, 'manage_category', ''),
(236, 'category_list', ''),
(237, 'add_category', ''),
(240, 'description', ''),
(268, 'edit_category', ''),
(271, 'manage_sub_category', ''),
(276, 'sub_category_list', ''),
(277, 'add_sub_category', ''),
(278, 'sub_category', ''),
(280, 'parant_category_id', ''),
(303, 'parent_category', ''),
(304, 'select_parent_category', ''),
(337, 'edit_sub_category', ''),
(340, 'manage_product', ''),
(345, 'approved_products', ''),
(346, 'waiting_products', ''),
(347, 'title', ''),
(348, 'price', ''),
(351, 'date', ''),
(352, 'condition', ''),
(353, 'featured', ''),
(354, 'pending', ''),
(355, 'view', ''),
(445, 'non-_featured', ''),
(576, 'status', ''),
(578, 'tag', ''),
(579, 'address', ''),
(584, 'select_user', ''),
(585, 'select_if_featured', ''),
(586, 'yes', ''),
(587, 'no', ''),
(588, 'select_a_status', ''),
(589, 'approved', ''),
(591, 'address', ''),
(592, 'images', ''),
(593, 'edit_product', ''),
(599, 'address', ''),
(717, 'address', ''),
(830, 'manage_home', ''),
(835, 'color', ''),
(836, 'home_version', ''),
(837, 'first', ''),
(838, 'second', ''),
(839, 'backups', ''),
(840, 'download_all_images', ''),
(841, 'download_system_scripts', ''),
(842, 'save_changes', ''),
(848, 'map', ''),
(849, 'address', ''),
(850, 'phone', ''),
(851, 'contact_email', ''),
(852, 'change_contact_info', ''),
(853, 'term_condition', ''),
(858, 'terms_and_conditions', ''),
(859, 'change_terms', ''),
(864, 'header_settings', ''),
(865, 'change_header', ''),
(870, 'footer_settings', ''),
(871, 'change_footer', ''),
(876, 'save', ''),
(881, 'system_settings', ''),
(882, 'manage_language', ''),
(887, 'phrase_list', ''),
(888, 'add_phrase', ''),
(889, 'add_language', ''),
(890, 'language', ''),
(891, 'option', ''),
(892, 'edit_phrase', ''),
(893, 'delete_language', ''),
(894, 'phrase', ''),
(899, 'logo', ''),
(900, 'manage_profile', ''),
(905, 'update_profile', ''),
(906, 'new_password', ''),
(907, 'confirm_new_password', ''),
(908, 'update_password', '');

-- --------------------------------------------------------

--
-- Table structure for table `lesson`
--

CREATE TABLE IF NOT EXISTS `lesson` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject_id` int(11) NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `lesson`
--

INSERT INTO `lesson` (`id`, `subject_id`, `name`) VALUES
(1, 1, 'Bai toan 1'),
(2, 1, 'Bai toan 2'),
(3, 1, 'Bai toan 3');

-- --------------------------------------------------------

--
-- Table structure for table `level`
--

CREATE TABLE IF NOT EXISTS `level` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `level`
--

INSERT INTO `level` (`id`, `name`, `description`) VALUES
(1, '6/12', 'trinh do 6/12'),
(2, '7/12', 'trinh do 6/12'),
(3, '8/12', 'trinh do 6/12'),
(4, '9/12', 'trinh do 6/12'),
(5, '10/12', 'trinh do 6/12');

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE IF NOT EXISTS `notification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `created_date` int(9) NOT NULL,
  `updated_date` int(9) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `notification`
--

INSERT INTO `notification` (`id`, `content`, `created_date`, `updated_date`, `is_active`) VALUES
(2, 'Thông báo số 3', 1416472888, 1416554991, 0),
(3, 'Thông báo số 2', 1416474743, 1416554962, 1),
(4, 'Thông báo số 1', 1416478025, 1416554603, 1);

-- --------------------------------------------------------

--
-- Table structure for table `question`
--

CREATE TABLE IF NOT EXISTS `question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `level_id` int(11) DEFAULT NULL,
  `lesson_id` int(11) DEFAULT NULL,
  `title` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `result1` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `result2` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `result3` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `result4` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `result` smallint(1) NOT NULL,
  `exam_id` int(11) NOT NULL,
  `created_date` int(9) NOT NULL,
  `updated_date` int(9) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `question_index` (`level_id`,`lesson_id`,`exam_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `result`
--

CREATE TABLE IF NOT EXISTS `result` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exam_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `total_score` smallint(3) NOT NULL,
  `time_finish` time NOT NULL,
  `created_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `resutl_index` (`exam_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `settings_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) NOT NULL,
  `description` varchar(1000) NOT NULL,
  PRIMARY KEY (`settings_id`),
  KEY `Settings_index` (`type`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=145 ;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`settings_id`, `type`, `description`) VALUES
(1, 'system_name', 'PAROOZLE | Safer & Easier local classifieds'),
(2, 'system_email', 'support@paroozle.com'),
(3, 'system_title', 'PAROOZLE'),
(4, 'address', 'CHICAGO, IL 60601'),
(5, 'phone', '+1.312.555.1212'),
(6, 'paypal_email', 'payments@paroozle.com'),
(7, 'currency', '$'),
(8, 'buyer', '312'),
(9, 'purchase_code', '5a58c0f7-856d-4d97-914c-067ca7ed041b'),
(10, 'language', 'english'),
(11, 'text_align', 'left-to-right'),
(12, 'color', '3'),
(13, 'layout', '0'),
(14, 'contact_email', 'support@paroozle.com'),
(16, 'home_ver', 'home'),
(17, 'footer', '2014 © PAROOZLE.com | All rights reserved. \n\n<a href="https://paroozle.com/index.php/home/privacy_policy">Privacy Policy</a> | <a href="http://paroozle.local/home/id_verifications">ID Verifications</a> | <a href="https://paroozle.com/index.php/home/terms_conditions">Terms</a> | <a href="https://paroozle.com/blog">Blog</a>'),
(18, 'header', 'SAFER & EASIER local classifieds');

-- --------------------------------------------------------

--
-- Table structure for table `sms`
--

CREATE TABLE IF NOT EXISTS `sms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `subject`
--

CREATE TABLE IF NOT EXISTS `subject` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_id` int(11) NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `subject`
--

INSERT INTO `subject` (`id`, `type_id`, `name`) VALUES
(1, 1, 'Toan'),
(2, 1, 'Ly'),
(3, 1, 'Hoa'),
(4, 2, 'Van'),
(5, 2, 'Su'),
(6, 2, 'Dia');

-- --------------------------------------------------------

--
-- Table structure for table `type`
--

CREATE TABLE IF NOT EXISTS `type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `type`
--

INSERT INTO `type` (`id`, `name`) VALUES
(1, 'Tu nhien'),
(2, 'Xa Hoi'),
(3, 'Ngoai Ngu'),
(4, 'Kien Thuc Chung');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `level_id` int(11) NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `created_date` int(9) NOT NULL,
  `updated_date` int(9) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `level_id`, `email`, `phone`, `created_date`, `updated_date`) VALUES
(1, 'hungpham', 'hungpham', 3, 'hungpv@gmail.com', '123123123', 111111111, 111111111),
(2, 'hung', '546cb004524c792856b4bbba7192456b', 2, 'hung123', '123123', 1416282027, 1416282027);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
