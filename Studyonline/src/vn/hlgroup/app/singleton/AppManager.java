package vn.hlgroup.app.singleton;

import java.util.ArrayList;

import vn.hlgroup.app.model.ExamOnline;
import vn.hlgroup.app.model.HighScore;
import vn.hlgroup.app.model.Lesson;
import vn.hlgroup.app.model.LevelId;
import vn.hlgroup.app.model.Notification;
import vn.hlgroup.app.model.Question;
import vn.hlgroup.app.model.ResultBoard;
import vn.hlgroup.app.model.Subject;
import vn.hlgroup.app.model.Type;
import vn.hlgroup.app.model.User;
import vn.hlgroup.app.model.nullobject.NullUser;
import vn.hlgroup.app.utils.Utils;
import android.app.Activity;

public class AppManager
{
    private static AppManager manager = null;

    private User user = new NullUser();

    private ArrayList<Type> typeList = new ArrayList<Type>();

    private ArrayList<Subject> subjectList = new ArrayList<Subject>();

    private ArrayList<Lesson> lessonList = new ArrayList<Lesson>();

    private ArrayList<Question> questions = new ArrayList<Question>();

    private ResultBoard result = new ResultBoard();

    private ArrayList<LevelId> levelId = new ArrayList<LevelId>();

    private ArrayList<ExamOnline> onlineExams = new ArrayList<ExamOnline>();

    private ArrayList<ExamOnline> onlineExamsDid = new ArrayList<ExamOnline>();

    private ArrayList<Subject> subjectAll = new ArrayList<Subject>();

    private ArrayList<HighScore> highScoremainExam = new ArrayList<HighScore>();

    private ArrayList<HighScore> highScoreonline = new ArrayList<HighScore>();

    private ArrayList<Question> onlineQuestionDid = new ArrayList<Question>();

    private ArrayList<Notification> notifications = new ArrayList<Notification>();

    private String scoreExamOnlineDid;

    private int questionPage;

    private long timeRemain;

    private int idOfOnlineExam;

    private String newMailContent;

    private AppManager()
    {

    }

    public static AppManager getInstance()
    {
        if (manager == null)
        {
            manager = new AppManager();
        }
        return manager;
    }

    public User getUser()
    {
        return user;
    }

    public void setUser(User user)
    {
        this.user = user;
    }

    public ArrayList<Type> getTypes()
    {
        return typeList;
    }

    public void setTypes(ArrayList<Type> typeList)
    {
        this.typeList = typeList;
    }

    public ArrayList<Subject> getSubjectsAll()
    {
        return subjectAll;
    }

    public void setSubjectsAll(ArrayList<Subject> subjectAll)
    {
        this.subjectAll = subjectAll;
    }

    public ArrayList<Subject> getSubjects()
    {
        return subjectList;
    }

    public void setSubjects(ArrayList<Subject> subjectList)
    {
        this.subjectList = subjectList;
    }

    public ArrayList<Lesson> getLessons()
    {
        return this.lessonList;
    }

    public void setLessons(ArrayList<Lesson> lessonList)
    {
        this.lessonList = lessonList;
    }

    public ArrayList<Question> getQuestions()
    {
        return this.questions;
    }

    public void setQuestions(ArrayList<Question> questions)
    {
        this.questions = questions;
    }

    public ResultBoard getResultBoard()
    {
        return this.result;
    }

    public void setResultBoard(ResultBoard result)
    {
        this.result = result;
    }

    public void resetResultBoard()
    {
        questionPage = 0;
        if (result.getExamId() != 0)
        {
            result = new ResultBoard();
        }
    }

    public int getQuestionPage()
    {
        return this.questionPage;
    }

    public void setQuestionPage(int questionPage)
    {
        this.questionPage = questionPage;
    }

    public ArrayList<LevelId> getLevelId()
    {
        return this.levelId;
    }

    public void setLevelId(ArrayList<LevelId> levelId)
    {
        this.levelId = levelId;
    }

    public int getRightAnswer()
    {
        int rightQuestions = 0;
        for (int i = 0; i < questions.size(); i++)
        {
            Question q = questions.get(i);
            int right = Integer.parseInt(q.getResult());

            int choose = result.getExamDetails().get(i).getResult();
            if (choose == right)
            {
                rightQuestions++;
                result.getExamDetails().get(i).setIsTrue(1);
            }
        }
        return rightQuestions;
    }

    public void setTimeRemain(long timeRemain)
    {
        this.timeRemain = timeRemain;
    }

    public long getTimeRemain()
    {
        return this.timeRemain;
    }

    public ArrayList<ExamOnline> getOnlineExams()
    {
        return this.onlineExams;
    }

    public void setOnlineExams(ArrayList<ExamOnline> onlineExams)
    {
        this.onlineExams = onlineExams;
    }

    public int getIdOfOnlineExam()
    {
        return this.idOfOnlineExam;
    }

    public void setIdOfOnlineExam(int idOfOnlineExam)
    {
        this.idOfOnlineExam = idOfOnlineExam;
    }

    public boolean HaveNewMail(Activity _context)
    {
        // return haveNewMail;

        String saveMailArrayLength = Utils.ReadData(_context,
                "saveMailArrayLength");
        if (saveMailArrayLength != null)
        {
            int saveMailArrayLengthNum = Integer.parseInt(saveMailArrayLength);

            ArrayList<Integer> saveMailIds = new ArrayList<Integer>();

            for (int i = 0; i < saveMailArrayLengthNum; i++)
            {
                saveMailIds.add(Integer.parseInt(Utils.ReadData(_context,
                        "saveMailArrayId_" + i)));
            }

            for (int i = 0; i < notifications.size(); i++)
            {
                int j = 0;
                for (j = 0; j < saveMailIds.size(); j++)
                {
                    if (notifications.get(i).getId() == saveMailIds.get(j)
                            .intValue())
                    {
                        break;
                    }
                }

                if (j >= saveMailIds.size())
                {
                    return true;
                }
            }

            return false;
        }
        else
        {
            Utils.WriteData(_context, "saveMailArrayLength", "0");
            if (notifications.size() > 0)
            {
                return true;
            }
            return false;
        }
    }

    public void setHaveNewMail(boolean haveNewmail)
    {
    }

    public String getNewMailContent()
    {
        return this.newMailContent;
    }

    public void setNewMailContent(String newMailContent)
    {
        this.newMailContent = newMailContent;
    }

    public ArrayList<HighScore> getHighScoremainExam()
    {
        return this.highScoremainExam;
    }

    public void setHighScoremainExam(ArrayList<HighScore> highScoremainExam)
    {
        this.highScoremainExam = highScoremainExam;
    }

    public ArrayList<HighScore> getHighScoreonline()
    {
        return this.highScoreonline;
    }

    public void setHighScoreonline(ArrayList<HighScore> highScoreonline)
    {
        this.highScoreonline = highScoreonline;
    }

    public ArrayList<ExamOnline> getOnlineExamsDid()
    {
        return onlineExamsDid;
    }

    public void setOnlineExamsDid(ArrayList<ExamOnline> onlineExamsDid)
    {
        this.onlineExamsDid = onlineExamsDid;
    }

    public ArrayList<Question> getOnlineQuestionsDid()
    {
        return this.onlineQuestionDid;
    }

    public void setOnlineQuestionsDid(ArrayList<Question> onlineQuestionDid)
    {
        this.onlineQuestionDid = onlineQuestionDid;
    }

    public String getScoreExamOnlineDid()
    {
        return scoreExamOnlineDid;
    }

    public void setScoreExamOnlineDid(String scoreExamOnlineDid)
    {
        this.scoreExamOnlineDid = scoreExamOnlineDid;
    }

    public ArrayList<Notification> getNotifications()
    {
        return notifications;
    }

    public void setNotifications(ArrayList<Notification> _notifications)
    {
        notifications = _notifications;
    }

    public String getMsgContentById(int _id)
    {
        for (int i = 0; i < notifications.size(); i++)
        {
            if (_id == notifications.get(i).getId())
            {
                return notifications.get(i).getContent();
            }
        }
        return "content not found";
    }
}
