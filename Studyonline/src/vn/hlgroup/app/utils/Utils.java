package vn.hlgroup.app.utils;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import vn.hlgroup.app.R;
import vn.hlgroup.app.model.ExamDetail;
import vn.hlgroup.app.singleton.AppManager;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.widget.Toast;

public class Utils
{
    private Utils()
    {
    }

    /**
     * Returns a MD5 hash String
     * 
     * @param input
     *            string need to hash
     * @return return MD5 hash String if success, return null if error
     */
    public static String getMd5Hash(String input)
    {
        if (input == null)
        {
            return null;
        }
        try
        {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] messageDigest = md.digest(input.getBytes());
            BigInteger number = new BigInteger(1, messageDigest);
            String md5 = number.toString(16);

            while (md5.length() < 32)
            {
                md5 = "0" + md5;
            }

            return md5;
        }
        catch (NoSuchAlgorithmException e)
        {
            Log.e("MD5", e.getLocalizedMessage());
            return null;
        }
    }

    public static String GET(String url)
    {
        InputStream inputStream = null;
        String result = "";
        try
        {

            // create HttpClient
            HttpClient httpclient = new DefaultHttpClient();

            // make GET request to the given URL
            HttpResponse httpResponse = httpclient.execute(new HttpGet(url));

            // receive response as inputStream
            inputStream = httpResponse.getEntity().getContent();

            // convert inputstream to string
            if (inputStream != null)
            {
                result = convertInputStreamToString(inputStream);
            }
            else
            {
                result = "Did not work!";
            }

        }
        catch (Exception e)
        {
            Log.d("InputStream", e.getLocalizedMessage());
        }

        return result;
    }

    public static String POST(String url, HashMap<String, String> str)
    {

        // Create a new HttpClient and Post Header
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(url);
        // InputStream inputStream = null;
        // String result = "";
        try
        {
            Set<String> keySet = str.keySet();
            String[] keys = (String[]) keySet.toArray();
            // Add your data
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            for (int i = 0; i < str.size(); i++)
            {
                String key = keys[i];
                String value = str.get(key);
                nameValuePairs.add(new BasicNameValuePair(key, value));
            }
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

            httpclient.execute(httppost);

            // receive response as inputStream
            // inputStream = response.getEntity().getContent();

            // convert inputstream to string
            // if (inputStream != null)
            // result = convertInputStreamToString(inputStream);
            // else
            // result = "Did not work!";
            return "suscess";
        }
        catch (ClientProtocolException e)
        {
            // TODO Auto-generated catch block
        }
        catch (IOException e)
        {
            Log.d("InputStream", e.getLocalizedMessage());
        }

        return "fail";

    }

    public static String POST(String url, JSONObject jo)
    {

        StringEntity se;
        try
        {
            DefaultHttpClient httpclient = new DefaultHttpClient();
            HttpPost httppostreq = new HttpPost(url);

            se = new StringEntity(jo.toString());

            httppostreq.setEntity(se);

            HttpResponse httpresponse = httpclient.execute(httppostreq);

            String responseText = EntityUtils
                    .toString(httpresponse.getEntity());

            return responseText;

        }
        catch (UnsupportedEncodingException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (ClientProtocolException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (IOException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return null;

    }

    public static String POST(String url, String key, String value)
    {

        try
        {
            DefaultHttpClient httpclient = new DefaultHttpClient();
            HttpPost httppostreq = new HttpPost(url);

            List<NameValuePair> list = new ArrayList<NameValuePair>();
            list.add(new BasicNameValuePair(key, value));

            httppostreq.setEntity(new UrlEncodedFormEntity(list));

            HttpResponse httpresponse = httpclient.execute(httppostreq);

            String responseText = EntityUtils
                    .toString(httpresponse.getEntity());

            return responseText;

        }
        catch (UnsupportedEncodingException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (ClientProtocolException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (IOException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return null;

    }

    private static String convertInputStreamToString(InputStream inputStream)
            throws IOException
    {
        BufferedReader bufferedReader = new BufferedReader(
                new InputStreamReader(inputStream));
        String line = "";
        StringBuilder result = new StringBuilder();
        while ((line = bufferedReader.readLine()) != null)
        {

            result.append(line);
        }

        inputStream.close();
        return result.toString();

    }

    public static String postData()
    {

        AppManager app = AppManager.getInstance();

        JSONObject jsonResponse = null;
        try
        {
            JSONObject json = new JSONObject();
            json.put("user_id", String.valueOf(app.getUser().getId()));
            json.put("exam_id",
                    String.valueOf(app.getResultBoard().getExamId()));
            json.put("time_finish", "0");
            json.put("total_score", app.getRightAnswer());
            json.put("total_question", app.getResultBoard().getExamDetails()
                    .size());

            ArrayList<ExamDetail> exams = app.getResultBoard().getExamDetails();

            JSONArray jso = new JSONArray();
            for (int i = 0; i < exams.size(); i++)
            {
                JSONObject jsonchild1 = new JSONObject();
                jsonchild1.put("question_id", exams.get(i).getQuestionId());
                jsonchild1.put("result", exams.get(i).getResult());
                jsonchild1.put("is_true", exams.get(i).getIsTrue());

                jso.put(jsonchild1);
            }

            json.put("exam_details", jso);

            HttpClient client = new DefaultHttpClient();
            HttpConnectionParams.setConnectionTimeout(client.getParams(),
                    100000);

            HttpPost post = new HttpPost(
                    "http://app.camemis.vn/index.php/service/save_result");

            StringEntity se = new StringEntity("json=" + json.toString());
            post.addHeader("content-type", "application/x-www-form-urlencoded");
            post.setEntity(se);

            HttpResponse response;
            response = client.execute(post);
            Log.d("abc", "beginmmmmmmmmmmmmmmmmm");
            String resFromServer = org.apache.http.util.EntityUtils
                    .toString(response.getEntity());
            Log.d("abc", "endmmmmmmmmmmmmmmmmm");
            Log.d("abc", resFromServer);
            Log.d("abc", "beginmmm22222");
            jsonResponse = new JSONObject(resFromServer);
            Log.i("Response from server", jsonResponse.getString("msg"));
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return jsonResponse.toString();
    }

    public static String postDataMainExam(int time)
    {

        AppManager app = AppManager.getInstance();

        JSONObject jsonResponse = null;
        try
        {
            JSONObject json = new JSONObject();
            json.put("user_id", String.valueOf(app.getUser().getId()));
            json.put("exam_id",
                    String.valueOf(app.getResultBoard().getExamId()));
            json.put("time_finish", String.valueOf(time));
            json.put("total_score", app.getRightAnswer());
            json.put("total_question", app.getResultBoard().getExamDetails()
                    .size());

            ArrayList<ExamDetail> exams = app.getResultBoard().getExamDetails();

            JSONArray jso = new JSONArray();
            for (int i = 0; i < exams.size(); i++)
            {
                JSONObject jsonchild1 = new JSONObject();
                jsonchild1.put("question_id", exams.get(i).getQuestionId());
                jsonchild1.put("result", exams.get(i).getResult());
                jsonchild1.put("is_true", exams.get(i).getIsTrue());

                jso.put(jsonchild1);
            }

            json.put("exam_details", jso);

            HttpClient client = new DefaultHttpClient();
            HttpConnectionParams.setConnectionTimeout(client.getParams(),
                    100000);

            HttpPost post = new HttpPost(
                    "http://app.camemis.vn/index.php/service/save_result");

            StringEntity se = new StringEntity("json=" + json.toString());
            post.addHeader("content-type", "application/x-www-form-urlencoded");
            post.setEntity(se);

            HttpResponse response;
            response = client.execute(post);
            Log.d("abc", "beginmmmmmmmmmmmmmmmmm");
            String resFromServer = org.apache.http.util.EntityUtils
                    .toString(response.getEntity());
            Log.d("abc", "endmmmmmmmmmmmmmmmmm");
            Log.d("abc", resFromServer);
            Log.d("abc", "beginmmm22222");
            jsonResponse = new JSONObject(resFromServer);
            // Log.i("Response from server", jsonResponse.getString("msg"));
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return jsonResponse.toString();
    }

    public static String postData(String url, JSONObject json)
    {

        AppManager.getInstance();

        JSONObject jsonResponse = null;
        try
        {

            HttpClient client = new DefaultHttpClient();
            HttpConnectionParams.setConnectionTimeout(client.getParams(),
                    100000);

            HttpPost post = new HttpPost(url);
            Log.d("abc", "beginmmmmmmmmmmmmmmmmm" + json.toString());
            StringEntity se = new StringEntity("json=" + json.toString(),
                    HTTP.UTF_8);
            post.addHeader("content-type", "application/x-www-form-urlencoded");
            post.setEntity(se);

            HttpResponse response;
            response = client.execute(post);
            Log.d("abc", "beginmmmmmmmmmmmmmmmmm");
            String resFromServer = org.apache.http.util.EntityUtils
                    .toString(response.getEntity());
            Log.d("abc", "endmmmmmmmmmmmmmmmmm");
            Log.d("abc", resFromServer);
            Log.d("abc", "beginmmm22222");
            jsonResponse = new JSONObject(resFromServer);

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return jsonResponse.toString();
    }

    public static String convertToTime(long sec)
    {
        long miniutes = sec / 60;
        long reaminsec = sec % 60;

        return miniutes + ":" + reaminsec;
    }

    public static void ShowToast(Context context, String text)
    {
        Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
    }

    public static void ShowToast(Context context, int res)
    {
        Toast.makeText(context, getStringRes((Activity) context, res),
                Toast.LENGTH_SHORT).show();
    }

    public static boolean IsSavedPassword(Activity context)
    {
        SharedPreferences sharedPref = context.getSharedPreferences("data",
                Context.MODE_PRIVATE);
        boolean defaultValue = false;
        boolean issavepass = sharedPref.getBoolean("issavepass", defaultValue);
        return issavepass;
    }

    public static void WriteIsSavedPassword(Activity context, boolean value)
    {
        SharedPreferences sharedPref = context.getSharedPreferences("data",
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean("issavepass", value);
        editor.commit();
    }

    public static void WriteData(Activity context, String key, String value)
    {
        SharedPreferences sharedPref = context.getSharedPreferences("data",
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static String ReadData(Activity context, String key)
    {
        SharedPreferences sharedPref = context.getSharedPreferences("data",
                Context.MODE_PRIVATE);
        String defaultValue = null;
        String data = sharedPref.getString(key, defaultValue);
        return data;
    }

    public static Drawable drawableFromUrl(String url)
    {
        try
        {
            Bitmap x;

            HttpURLConnection connection = (HttpURLConnection) new URL(url)
                    .openConnection();
            connection.connect();
            InputStream input = connection.getInputStream();

            x = BitmapFactory.decodeStream(input);
            return new BitmapDrawable(x);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }

    public static Bitmap getImageBitmap(String url)
    {
        Bitmap bm = null;
        try
        {
            URL aURL = new URL(url);
            URLConnection conn = aURL.openConnection();
            conn.connect();
            InputStream is = conn.getInputStream();
            BufferedInputStream bis = new BufferedInputStream(is);
            bm = BitmapFactory.decodeStream(bis);
            bis.close();
            is.close();

        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return bm;
    }

    public static String getStringRes(Activity context, int res)
    {
        return context.getResources().getString(res);
    }

    public static String showErr(Activity context)
    {
        return context.getResources().getString(R.string.error);
    }

    public static Rect GetBounds(BitmapDrawable bd, int bottomFixed)
    {
        Rect r = bd.getBounds();

        return new Rect(0, 0, r.right * (bottomFixed / r.bottom), bottomFixed);
    }

    public static boolean IsNetworkAvailable(Context context)
    {
        ConnectivityManager connManager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mWifi = connManager
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        if (mWifi == null)
        {
            return false;
        }

        if (mWifi.isConnected())
        {
            return true;
        }

        NetworkInfo g3 = connManager
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

        if (g3 == null)
        {
            return false;
        }

        if (g3.isConnected())
        {
            return true;
        }

        return false;
    }

    public static String roundTwoDigit(String score)
    {
        if (!score.contains("."))
        {
            return score;
        }

        int indexDot = score.indexOf('.');
        return score.substring(0, indexDot + 2);
    }
}
