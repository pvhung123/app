package vn.hlgroup.app.adapter;

import java.util.List;

import vn.hlgroup.app.R;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;
import android.widget.TextView;

public class CustomSpinnerAdapter extends ArrayAdapter<MapItem>
{
    private Context context;

    private List<MapItem> content;

    public CustomSpinnerAdapter(Context context, int textViewResourceId,
            List<MapItem> objects)
    {
        super(context, textViewResourceId, objects);
        // TODO Auto-generated constructor stub

        this.context = context;
        this.content = objects;
    }

    @Override
    public int getCount()
    {
        return content.size();
    }

    @Override
    public MapItem getItem(int position)
    {
        return content.get(position);
    }

    @Override
    public long getItemId(int position)
    {
        return content.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        // I created a dynamic TextView here, but you can reference your own
        // custom layout for each spinner item
        TextView label = (TextView) View.inflate(context,
                R.drawable.custom_spinner_item, null);
        // label.setTextColor(Color.RED);
        label.setText(content.get(position).getValue());

        // And finally return your dynamic (or custom) view for each spinner
        // item
        return label;
    }

    // And here is when the "chooser" is popped up
    // Normally is the same view, but you can customize it if you want
    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent)
    {
        CheckedTextView label = (CheckedTextView) super.getDropDownView(
                position, convertView, parent);
        // label.setTextColor(Color.RED);
        label.setText(content.get(position).getValue());

        // label.setTextSize(20f);

        return label;
    }

}
