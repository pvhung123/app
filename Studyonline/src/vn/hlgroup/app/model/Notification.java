package vn.hlgroup.app.model;

import java.io.Serializable;

public class Notification implements Serializable
{

    private int id;

    private String title;

    private String content;

    private long created_date;

    private long updated_date;

    private int is_active;

    public Notification()
    {
    }

    public int getId()
    {
        return id;
    }

    public void setId(int _id)
    {
        id = _id;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String _title)
    {
        title = _title;
    }

    public String getContent()
    {
        return content;
    }

    public void setContent(String _content)
    {
        content = _content;
    }

    public long getCreatedDate()
    {
        return created_date;
    }

    public void setCreatedDate(long _created_date)
    {
        created_date = _created_date;
    }

    public long getUpdatedDate()
    {
        return updated_date;
    }

    public void setUpdatedDate(long _updated_date)
    {
        updated_date = _updated_date;
    }

    public int isActive()
    {
        return is_active;
    }

    public void setActive(int _is_active)
    {
        is_active = _is_active;
    }

}
