package vn.hlgroup.app.model;

import java.io.Serializable;

public class LevelId implements Serializable
{

    private int id;
    private String name;
    private String description;

    public LevelId()
    {
    }

    public int getId()
    {
        return this.id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getName()
    {
        return this.name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getDescription()
    {
        return this.description;
    }

    public void setdescription(String description)
    {
        this.description = description;
    }
}
