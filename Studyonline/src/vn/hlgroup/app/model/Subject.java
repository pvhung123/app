package vn.hlgroup.app.model;

import java.io.Serializable;

public class Subject implements Serializable
{
    private int id;

    private int type_id;

    private String name;

    public Subject()
    {

    }

    public int getId()
    {
        return this.id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public int getType_Id()
    {
        return this.type_id;
    }

    public void setType_Id(int type_id)
    {
        this.type_id = type_id;
    }

    public String getName()
    {
        return this.name;
    }

    public void setName(String name)
    {
        this.name = name;
    }
}
