package vn.hlgroup.app.model;

import java.io.Serializable;

public class HighScore implements Serializable
{
    private String username;

    private String score;

    public HighScore()
    {

    }

    public String getUsername()
    {
        return this.username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getScore()
    {
        return this.score;
    }

    public void setScore(String score)
    {
        this.score = score;
    }
}
