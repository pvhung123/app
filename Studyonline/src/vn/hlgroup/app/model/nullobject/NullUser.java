package vn.hlgroup.app.model.nullobject;

import vn.hlgroup.app.model.User;

public class NullUser extends User
{
    public NullUser()
    {
        super();
    }

    @Override
    public int getId()
    {
        return 0;
    }

    @Override
    public String getUsername()
    {
        return "User invalid";
    }

    @Override
    public String getPassword()
    {
        return "User invalid";
    }

    @Override
    public int getLevel_id()
    {
        return 0;
    }

    @Override
    public String getEmail()
    {
        return "User invalid";
    }

    @Override
    public String getPhone()
    {
        return "User invalid";
    }

    @Override
    public int getCreated_date()
    {
        return 0;
    }

    @Override
    public int getUpdated_date()
    {
        return 0;
    }

    /*
     * public String toJSON() { return "{id:0," + "username:\"User invalid\"," +
     * "password:\"User invalid\"," + "level_id:\"User invalid\"," +
     * "email:\"User invalid\"," + "phone:\"User invalid\"," + "created_date:0,"
     * + "updated_date:0}"; }
     */
}
