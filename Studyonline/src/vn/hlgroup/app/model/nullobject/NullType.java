package vn.hlgroup.app.model.nullobject;

import vn.hlgroup.app.model.Type;

public class NullType extends Type
{
    public NullType()
    {
        super();
    }

    @Override
    public int getId()
    {
        return 0;
    }

    @Override
    public String getName()
    {
        return "Type invalid";
    }

    /*
     * public String toJSON() { return "{id:0," + "name:\"Type invalid\"}"; }
     */
}
