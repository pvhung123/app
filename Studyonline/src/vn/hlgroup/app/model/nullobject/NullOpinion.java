package vn.hlgroup.app.model.nullobject;

import vn.hlgroup.app.model.Opinion;

public class NullOpinion extends Opinion
{
    public NullOpinion()
    {
        super();
    }

    @Override
    public int getIdo()
    {
        return 0;
    }

    @Override
    public String getContento()
    {
        return "Opinion invalid";
    }

    /*
     * public String toJSON() { return "{ido:\"Opinion invalid\"," +
     * "contento:\"Opinion invalid\"}"; }
     */
}
