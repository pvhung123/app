package vn.hlgroup.app.model;

import java.io.Serializable;
import java.util.ArrayList;

import vn.hlgroup.app.singleton.AppManager;

public class ResultBoard implements Serializable
{

    private int examId;
    private String timeFinish;
    private int totalscore;
    private ArrayList<ExamDetail> examDetail = new ArrayList<ExamDetail>();

    public ResultBoard()
    {

    }

    public int getExamId()
    {
        return this.examId;
    }

    public void setExamId(int examId)
    {
        this.examId = examId;
    }

    public String getTimeFinish()
    {
        return this.timeFinish;
    }

    public void setTimeFinish(String timeFinish)
    {
        this.timeFinish = timeFinish;
    }

    public int getTotalscore()
    {
        return this.totalscore;
    }

    public void setTotalscore(int totalscore)
    {
        this.totalscore = totalscore;
    }

    public ArrayList<ExamDetail> getExamDetails()
    {
        return this.examDetail;
    }

    public void setexamDetails(ArrayList<ExamDetail> examDetails)
    {
        this.examDetail = examDetails;
    }

    public void initExamDetails(int numberOfQuestions)
    {
        examDetail.clear();
        examDetail = new ArrayList<ExamDetail>(numberOfQuestions);
        for (int i = 0; i < numberOfQuestions; i++)
        {
            ExamDetail ex = new ExamDetail();
            examDetail.add(ex);
        }
    }

    public void caculateTotalScore()
    {
        int numberOfRight = 0;
        ArrayList<Question> questions = AppManager.getInstance().getQuestions();
        int totalquestion = questions.size();
        for (int i = 0; i < totalquestion; i++)
        {
            Question q = questions.get(i);
            int result = Integer.parseInt(q.getResult());

            ExamDetail ex = examDetail.get(i);

            if (result == ex.getResult())
            {
                numberOfRight++;
            }
        }

        // String s = String.format("%.2f", numberOfRight*10.0/totalquestion);

        // totalscore = Float.parseFloat(s);
        totalscore = numberOfRight;
    }

}
