package vn.hlgroup.app.model;

import java.io.Serializable;

public class User implements Serializable // user table
{
    private int id;
    private String username;
    private String password;
    private int level_id;
    private String email;
    private String phone;
    private int created_date;
    private int updated_date;

    private String passDonthash;

    public User()
    {
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public int getId()
    {
        return this.id;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getUsername()
    {
        return this.username;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getPassword()
    {
        return this.password;
    }

    public void setPasswordDontHash(String passDonthash)
    {
        this.passDonthash = passDonthash;
    }

    public String getPasswordDontHash()
    {
        return this.passDonthash;
    }

    public void setLevel_id(int level_id)
    {
        this.level_id = level_id;
    }

    public int getLevel_id()
    {
        return this.level_id;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getEmail()
    {
        return this.email;
    }

    public void setPhone(String phone)
    {
        this.phone = phone;
    }

    public String getPhone()
    {
        return this.phone;
    }

    public void setCreated_date(int created_date)
    {
        this.created_date = created_date;
    }

    public int getCreated_date()
    {
        return this.created_date;
    }

    public void setUpdated_date(int updated_date)
    {
        this.updated_date = updated_date;
    }

    public int getUpdated_date()
    {
        return this.updated_date;
    }
}
