package vn.hlgroup.app.model;

import java.io.Serializable;

public class ExamOnline implements Serializable
{

    private int id;

    private String title;

    private int catalog_id;

    private long expired_date;

    private int subject_id;

    private int level_id;

    private int type_id;

    private int lesson_id;

    private int number_of_question;

    private int time_to_test;

    private long created_date;

    private long updated_date;

    private boolean is_active;

    private boolean is_did_exam;

    private int total_score;

    private long beginDate;

    private int total_question;

    public ExamOnline()
    {
    }

    public int getId()
    {
        return this.id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getTitle()
    {
        return this.title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public int getCatalogId()
    {
        return this.catalog_id;
    }

    public void setCataloid(int catalog_id)
    {
        this.catalog_id = catalog_id;
    }

    public long getExpiredDate()
    {
        return this.expired_date;
    }

    public void setExpiredDate(long expired_date)
    {
        this.expired_date = expired_date;
    }

    public int getSubjectId()
    {
        return this.subject_id;
    }

    public void setSubjectId(int subject_id)
    {
        this.subject_id = subject_id;
    }

    public int getLevelId()
    {
        return this.level_id;
    }

    public void setLevelId(int level_id)
    {
        this.level_id = level_id;
    }

    public int getTypeId()
    {
        return this.type_id;
    }

    public void setTypeId(int type_id)
    {
        this.type_id = type_id;
    }

    public int getLessonId()
    {
        return this.lesson_id;
    }

    public void setLessonId(int lesson_id)
    {
        this.lesson_id = lesson_id;
    }

    public int getNumberOfQuestion()
    {
        return this.number_of_question;
    }

    public void setNumberOfQuestion(int number_of_question)
    {
        this.number_of_question = number_of_question;
    }

    public int getTimeToTest()
    {
        return this.time_to_test;
    }

    public void setTimeToTest(int time_to_test)
    {
        this.time_to_test = time_to_test;
    }

    public long getCreatedDate()
    {
        return this.created_date;
    }

    public void setCreatedDate(long created_date)
    {
        this.created_date = created_date;
    }

    public long getUpdatedDate()
    {
        return this.updated_date;
    }

    public void setUpdatedDate(long updated_date)
    {
        this.updated_date = updated_date;
    }

    public boolean IsActive()
    {
        return this.is_active;
    }

    public void setActive(boolean value)
    {
        this.is_active = value;
    }

    public boolean IsDidExam()
    {
        return this.is_did_exam;
    }

    public void setDidExam(boolean is_did_exam)
    {
        this.is_did_exam = is_did_exam;
    }

    public void setTotal_score(int _total_score)
    {
        total_score = _total_score;
    }

    public int getTotal_score()
    {
        return total_score;
    }

    public long getBeginDate()
    {
        return beginDate;
    }

    public void setBeginDate(long _beginDate)
    {
        beginDate = _beginDate;
    }

    public void setTotal_question(int _total_question)
    {
        total_question = _total_question;
    }

    public int getTotal_question()
    {
        return total_question;
    }

}
