package vn.hlgroup.app.model;

import java.io.Serializable;

public class Question implements Serializable
{
    private int id;

    private int levelId;

    private int type_id;// new

    private int subject_id;// new

    private int lessonId;

    private String title;

    private String result;

    private String result1;

    private String result2;

    private String result3;

    private String result4;

    private int examId;

    private int is_image;// new

    private int created_date;

    private int updated_date;

    private String question_result;

    public Question()
    {

    }

    public int getId()
    {
        return this.id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public int getTypeId()
    {
        return this.type_id;
    }

    public void setTypeId(int type_id)
    {
        this.type_id = type_id;
    }

    public int getLevelId()
    {
        return this.levelId;
    }

    public void setLevelId(int levelId)
    {
        this.levelId = levelId;
    }

    public int getSubjectId()
    {
        return this.subject_id;
    }

    public void setSubjectId(int subject_id)
    {
        this.subject_id = subject_id;
    }

    public int getLessonId()
    {
        return this.lessonId;
    }

    public void setLessonId(int lessonId)
    {
        this.lessonId = lessonId;
    }

    public String getTitle()
    {
        return this.title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getResult()
    {
        return this.result;
    }

    public void setResult(String result)
    {
        this.result = result;
    }

    public String getResult1()
    {
        return this.result1;
    }

    public void setResult1(String result1)
    {
        this.result1 = result1;
    }

    public String getResult2()
    {
        return this.result2;
    }

    public void setResult2(String result2)
    {
        this.result2 = result2;
    }

    public String getResult3()
    {
        return this.result3;
    }

    public void setResult3(String result3)
    {
        this.result3 = result3;
    }

    public String getResult4()
    {
        return this.result4;
    }

    public void setResult4(String result4)
    {
        this.result4 = result4;
    }

    public int getExamId()
    {
        return this.examId;
    }

    public void setExamId(int examId)
    {
        this.examId = examId;
    }

    public int getIsImage()
    {
        return this.is_image;
    }

    public void setIsImage(int is_image)
    {
        this.is_image = is_image;
    }

    public int getCreated_date()
    {
        return this.created_date;
    }

    public void setCreated_date(int created_date)
    {
        this.created_date = created_date;
    }

    public int getUpdated_date()
    {
        return this.updated_date;
    }

    public void setUpdated_date(int updated_date)
    {
        this.updated_date = updated_date;
    }

    public String getQuestion_result()
    {
        return this.question_result;
    }

    public void setQuestion_result(String _question_result)
    {
        question_result = _question_result;
    }

}
