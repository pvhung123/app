package vn.hlgroup.app.model;

import java.io.Serializable;

public class ExamDetail implements Serializable
{
    public static final int ANSWER_NONE = 0;
    public static final int ANSWER_ONE = 1;
    public static final int ANSWER_TWO = 2;
    public static final int ANSWER_THREE = 3;
    public static final int ANSWER_FOUR = 4;

    private int questionId;

    private int result;// abcd = > 1,2,3,4

    private int isTrue;

    public ExamDetail()
    {
        result = ExamDetail.ANSWER_NONE;
    }

    public int getQuestionId()
    {
        return this.questionId;
    }

    public void setQuestionid(int questionId)
    {
        this.questionId = questionId;
    }

    public int getResult()
    {
        return this.result;
    }

    public void setResult(int result)
    {
        this.result = result;
    }

    public int getIsTrue()
    {
        return this.isTrue;
    }

    public void setIsTrue(int isTrue)
    {
        this.isTrue = isTrue;
    }
}
