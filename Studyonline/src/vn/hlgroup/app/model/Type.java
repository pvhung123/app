package vn.hlgroup.app.model;

import java.io.Serializable;

public class Type implements Serializable // type table
{
    private int id;
    private String name;

    public Type()
    {
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public int getId()
    {
        return this.id;
    }

    public String getName()
    {
        return this.name;
    }

    /*
     * public String toJSON() { try { JSONObject jo = new JSONObject();
     * 
     * jo.put("id", this.id); jo.put("name", this.name);
     * 
     * return jo.toString(); } catch(JSONException e) { e.printStackTrace(); }
     * 
     * return new NullType().toJSON(); }
     */

}
