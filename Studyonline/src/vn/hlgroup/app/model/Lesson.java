package vn.hlgroup.app.model;

import java.io.Serializable;

public class Lesson implements Serializable
{
    private int id;

    private int subjectId;

    private String name;

    public Lesson()
    {

    }

    public int getId()
    {
        return this.id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public int getSubjectId()
    {
        return this.subjectId;
    }

    public void setSubjectId(int subjectId)
    {
        this.subjectId = subjectId;
    }

    public String getName()
    {
        return this.name;
    }

    public void setName(String name)
    {
        this.name = name;
    }
}
