package vn.hlgroup.app.model;

import java.io.Serializable;

public class Opinion implements Serializable // feedback table
{
    private int ido;
    private String contento;

    public Opinion()
    {
    }

    public void setIdo(int ido)
    {
        this.ido = ido;
    }

    public int getIdo()
    {
        return this.ido;
    }

    public void setContento(String contento)
    {
        this.contento = contento;
    }

    public String getContento()
    {
        return this.contento;
    }

    /*
     * public String toJSON() { try { JSONObject jo = new JSONObject();
     * 
     * jo.put("ido", this.ido); jo.put("contento", this.contento);
     * 
     * return jo.toString(); } catch(JSONException e) { e.printStackTrace(); }
     * 
     * return new NullOpinion().toJSON(); }
     */

}
