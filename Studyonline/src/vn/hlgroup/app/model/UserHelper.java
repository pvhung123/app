package vn.hlgroup.app.model;

import org.json.JSONObject;

import vn.hlgroup.app.model.nullobject.NullUser;

public class UserHelper
{
    public UserHelper()
    {
    }

    // public User getUser(String username, String password)
    // {
    // return new NullUser();
    // }

    public String toJSON(User user)
    {
        try
        {
            JSONObject jo = new JSONObject();

            jo.put("id", user.getId());
            jo.put("username", user.getUsername());
            jo.put("password", user.getPassword());
            jo.put("level_id", user.getLevel_id());
            jo.put("email", user.getEmail());
            jo.put("phone", user.getPhone());
            jo.put("created_date", user.getCreated_date());
            jo.put("updated_date", user.getUpdated_date());

            return jo.toString();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return "{}";
    }

    public User parse(String json)
    {
        try
        {
            JSONObject reader = new JSONObject(json);

            User user = new User();

            user.setUsername(reader.getString("username"));
            user.setPassword(reader.getString("password"));
            user.setLevel_id(reader.getInt("level_id"));
            user.setEmail(reader.getString("email"));
            user.setPhone(reader.getString("phone"));
            user.setCreated_date(reader.getInt("created_date"));
            user.setUpdated_date(reader.getInt("updated_date"));
            user.setId(reader.getInt("id"));

            return user;

        }
        catch (Exception e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return new NullUser();
    }

}
