package vn.hlgroup.app;

public interface IDefine
{
    public static final boolean isDebugLogin = false;
    public static int CATALOG_TRAIN = 3;
    public static int CATALOG_MAIN_TEST = 2;
    public static int CATALOG_ONLINE_TEST = 1;

    public static int TEST_TIME_DEFAULT = 3600;// 3600;//in seconds
    public static String HEADER_LINK_SERVICE = "http://app.camemis.vn/index.php/service/";
    public static String HEADER_LINK_UPLOAD = "http://app.camemis.vn";
    public static String ADMOB_ID = "ca-app-pub-3703540955981839/7069538105";

    public static long SESSION_TIME_OUT = 3600000;// 3h in milliseconds

    public static boolean HAVE_ADS = true;

    public static boolean IS_THREE_DAY_ONLINE_STUDY_LIMIT = false;
}
