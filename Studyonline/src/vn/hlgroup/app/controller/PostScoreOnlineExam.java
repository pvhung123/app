package vn.hlgroup.app.controller;

import org.json.JSONException;
import org.json.JSONObject;

import vn.hlgroup.app.R;
import vn.hlgroup.app.activity.OnlineExamActivity;
import vn.hlgroup.app.activity.OnlineScoreActivity;
import vn.hlgroup.app.utils.Utils;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.Toast;

public class PostScoreOnlineExam extends AsyncTask<Integer, Integer, String>
{
    private Activity context;

    public PostScoreOnlineExam(Activity context)
    {
        this.context = context;
    }

    @Override
    protected String doInBackground(Integer... params)
    {
        {
        }

        String result = Utils.postDataMainExam(params[0]);
        Log.d("abceeeeee", result);

        return result;

    }

    @Override
    protected void onPreExecute()
    {
        OnlineExamActivity mai = (OnlineExamActivity) context;

        Button pre = (Button) mai.findViewById(R.id.btnPreOnlineExam);

        Button next = (Button) mai.findViewById(R.id.btnNextOnlineExam);

        Button finish = (Button) mai.findViewById(R.id.btnFinishOnlineExam);

        pre.setEnabled(false);
        next.setEnabled(false);
        finish.setEnabled(false);

        ProgressBar pro = (ProgressBar) mai.findViewById(R.id.proTrainning);
        pro.setVisibility(View.VISIBLE);

        RadioButton result1 = (RadioButton) mai
                .findViewById(R.id.radio0OnlineExam);

        RadioButton result2 = (RadioButton) mai
                .findViewById(R.id.radio1OnlineExam);

        RadioButton result3 = (RadioButton) mai
                .findViewById(R.id.radio2OnlineExam);

        RadioButton result4 = (RadioButton) mai
                .findViewById(R.id.radio3OnlineExam);

        result1.setEnabled(false);
        result2.setEnabled(false);
        result3.setEnabled(false);
        result4.setEnabled(false);

    }

    @Override
    protected void onPostExecute(String result)
    {

        OnlineExamActivity mai = (OnlineExamActivity) context;

        Button pre = (Button) mai.findViewById(R.id.btnPreOnlineExam);

        Button next = (Button) mai.findViewById(R.id.btnNextOnlineExam);

        Button finish = (Button) mai.findViewById(R.id.btnFinishOnlineExam);

        pre.setEnabled(true);
        next.setEnabled(true);
        finish.setEnabled(true);

        ProgressBar pro = (ProgressBar) mai.findViewById(R.id.proTrainning);
        pro.setVisibility(View.INVISIBLE);

        RadioButton result1 = (RadioButton) mai
                .findViewById(R.id.radio0OnlineExam);

        RadioButton result2 = (RadioButton) mai
                .findViewById(R.id.radio1OnlineExam);

        RadioButton result3 = (RadioButton) mai
                .findViewById(R.id.radio2OnlineExam);

        RadioButton result4 = (RadioButton) mai
                .findViewById(R.id.radio3OnlineExam);

        result1.setEnabled(true);
        result2.setEnabled(true);
        result3.setEnabled(true);
        result4.setEnabled(true);

        try
        {

            JSONObject jo = new JSONObject(result);
            jo.getString("message");

            int resultCode = jo.getInt("error");

            if (resultCode == 1)
            {
                // Toast.makeText(context, jomsg, Toast.LENGTH_SHORT).show();
                Toast.makeText(context, Utils.showErr(context),
                        Toast.LENGTH_SHORT).show();
            }
            else
            {
                // Toast.makeText(context, jomsg, Toast.LENGTH_SHORT).show();
                Utils.ShowToast(context, R.string.saveok);
                mai.finish();

                Intent intent = new Intent(mai, OnlineScoreActivity.class);
                mai.startActivity(intent);
            }
        }
        catch (JSONException e)
        {
            Utils.showErr(context);
            e.printStackTrace();
        }

    }

}
