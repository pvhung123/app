package vn.hlgroup.app.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import vn.hlgroup.app.IDefine;
import vn.hlgroup.app.R;
import vn.hlgroup.app.activity.ListResultOnlineActivity;
import vn.hlgroup.app.model.ExamOnline;
import vn.hlgroup.app.singleton.AppManager;
import vn.hlgroup.app.utils.Utils;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

public class GetOnlineExamDid extends AsyncTask<Void, Integer, String>
{
    private Activity context;

    public GetOnlineExamDid(Activity context)
    {
        this.context = context;
    }

    @Override
    protected String doInBackground(Void... params)
    {
        // TODO Auto-generated method stub
        // check login from server
        try
        {

            String url = IDefine.HEADER_LINK_SERVICE + "get_exams_user/"
                    + AppManager.getInstance().getUser().getId();
            Log.d("abc", "" + AppManager.getInstance().getUser().getId());
            String examsonlinejson = Utils.GET(url);

            return examsonlinejson;
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;

    }

    @Override
    protected void onPreExecute()
    {

    }

    @Override
    protected void onPostExecute(String result)
    {
        AppManager app = AppManager.getInstance();
        try
        {
            JSONObject jo = new JSONObject(result);

            int err = jo.getInt("error");

            if (err == 0)
            {
                ArrayList<ExamOnline> onlineExmas = app.getOnlineExamsDid();
                onlineExmas.clear();
                JSONArray joamsg = jo.getJSONArray("message");

                for (int i = 0; i < joamsg.length(); i++)
                {
                    JSONObject ele = joamsg.getJSONObject(i);
                    ExamOnline eo = new ExamOnline();

                    eo.setId(Integer.parseInt(ele.getString("id")));
                    eo.setTitle(ele.getString("title"));
                    eo.setCataloid(Integer.parseInt(ele
                            .getString("category_id")));
                    String exday = ele.getString("expired_date");

                    SimpleDateFormat formatter = new SimpleDateFormat(
                            "yyyy-MM-dd HH:mm:ss");

                    Date a = formatter.parse(exday);
                    eo.setExpiredDate(a.getTime());
                    eo.setSubjectId(Integer.parseInt(ele
                            .getString("subject_id")));
                    eo.setLevelId(Integer.parseInt(ele.getString("level_id")));
                    eo.setTypeId(Integer.parseInt(ele.getString("type_id")));
                    eo.setLessonId(Integer.parseInt(ele.getString("lesson_id")));
                    eo.setNumberOfQuestion(Integer.parseInt(ele
                            .getString("number_of_question")));
                    eo.setTimeToTest(Integer.parseInt(ele
                            .getString("time_to_test")));
                    eo.setCreatedDate(Long.parseLong(ele
                            .getString("created_date")));
                    eo.setUpdatedDate(Long.parseLong(ele
                            .getString("updated_date")));
                    eo.setActive(Integer.parseInt(ele.getString("is_active")) == 1 ? true
                            : false);

                    eo.setTotal_score(Integer.parseInt(ele
                            .getString("total_score")));

                    eo.setTotal_question(Integer.parseInt(ele
                            .getString("total_question")));

                    onlineExmas.add(eo);
                }

                context.finish();

                Intent intent = new Intent(context,
                        ListResultOnlineActivity.class);
                context.startActivity(intent);

            }
            else
            {
                Utils.ShowToast(context, R.string.error);
            }
        }
        catch (JSONException e)
        {
            Utils.ShowToast(context, R.string.online_dont_have_did);
            e.printStackTrace();
        }
        catch (ParseException e)
        {
            Utils.ShowToast(context, R.string.error);
            e.printStackTrace();
        }

    }
}
