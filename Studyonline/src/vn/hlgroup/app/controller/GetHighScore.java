package vn.hlgroup.app.controller;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import vn.hlgroup.app.IDefine;
import vn.hlgroup.app.R;
import vn.hlgroup.app.activity.AvgScoreActivity;
import vn.hlgroup.app.activity.AvgScoreOnlineActivity;
import vn.hlgroup.app.model.HighScore;
import vn.hlgroup.app.singleton.AppManager;
import vn.hlgroup.app.utils.Utils;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.Toast;

public class GetHighScore extends AsyncTask<Integer, Integer, Integer>
{
    private Activity context;

    // private int indexActivity;

    public GetHighScore(Activity context)
    {
        this.context = context;
    }

    @Override
    protected Integer doInBackground(Integer... params)
    {
        int indexActivity = params[0];

        AppManager app = AppManager.getInstance();

        if (indexActivity == 0)// result main
        {
            String urlMainExam = IDefine.HEADER_LINK_SERVICE + "high_score/2";
            String result1 = Utils.GET(urlMainExam);

            try
            {
                JSONObject jo = new JSONObject(result1);
                int err = jo.getInt("error");

                if (err == 0)
                {
                    ArrayList<HighScore> highscoremainexam = app
                            .getHighScoremainExam();
                    highscoremainexam.clear();
                    JSONArray joarr = jo.getJSONArray("message");

                    for (int i = 0; i < joarr.length(); i++)
                    {
                        HighScore l = new HighScore();
                        JSONObject joarrObject = joarr.getJSONObject(i);

                        l.setUsername(joarrObject.getString("username"));
                        l.setScore(joarrObject.getString("score"));

                        highscoremainexam.add(l);
                    }

                    return params[0] * 10 + 0;
                }
            }
            catch (JSONException e)
            {
                return params[0] * 10 + 2;
            }
        }
        else
        {
            String urlOnline = IDefine.HEADER_LINK_SERVICE + "high_score/1";
            String result2 = Utils.GET(urlOnline);
            try
            {
                JSONObject jo1 = new JSONObject(result2);
                int err1 = jo1.getInt("error");

                if (err1 == 0)
                {
                    ArrayList<HighScore> highscoreonline = app
                            .getHighScoreonline();
                    highscoreonline.clear();
                    JSONArray joarr = jo1.getJSONArray("message");

                    for (int i = 0; i < joarr.length(); i++)
                    {
                        HighScore l = new HighScore();
                        JSONObject joarrObject = joarr.getJSONObject(i);

                        l.setUsername(joarrObject.getString("username"));
                        l.setScore(joarrObject.getString("score"));

                        highscoreonline.add(l);
                    }

                    return params[0] * 10 + 0;
                }

            }
            catch (JSONException e)
            {
                return params[0] * 10 + 2;
            }
        }

        return params[0] * 10 + 1;

    }

    @Override
    protected void onPreExecute()
    {

    }

    @Override
    protected void onPostExecute(Integer result)
    {
        int indexActivity = result / 10;
        result = result % 10;
        if (result == 0)
        {
            if (indexActivity == 0)
            {
                context.finish();

                Intent intent = new Intent(context, AvgScoreActivity.class);

                context.startActivity(intent);

            }
            else
            {
                context.finish();

                Intent intent = new Intent(context,
                        AvgScoreOnlineActivity.class);

                context.startActivity(intent);

            }
        }
        else
            if (result == 2)
            {
                Toast.makeText(
                        context,
                        context.getResources().getString(
                                R.string.bangxephangtrong), Toast.LENGTH_SHORT)
                        .show();
            }
            else
            {
                Toast.makeText(context, Utils.showErr(context),
                        Toast.LENGTH_SHORT).show();
            }

    }
}
