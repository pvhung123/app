package vn.hlgroup.app.controller;

import org.json.JSONException;
import org.json.JSONObject;

import vn.hlgroup.app.IDefine;
import vn.hlgroup.app.R;
import vn.hlgroup.app.model.User;
import vn.hlgroup.app.singleton.AppManager;
import vn.hlgroup.app.utils.Utils;
import android.app.Activity;
import android.os.AsyncTask;
import android.widget.Toast;

public class UpdateInfo extends AsyncTask<String, Integer, String> implements
        IDefine
{
    private Activity context;

    public UpdateInfo(Activity context)
    {
        this.context = context;
    }

    private boolean validatePassword(String password, StringBuilder errorMsg)
    {
        if (password == null || password.trim().equals(""))
        {
            String str = context.getResources().getString(
                    R.string.register_passmiss);
            errorMsg.append(str);
            return false;
        }
        else
            if (password.length() < 6)
            {
                String str = context.getResources().getString(
                        R.string.register_passlagersixletter);
                errorMsg.append(str);
                return false;
            }
        return true;
    }

    private boolean validateLevel(String level, StringBuilder errorMsg)
    {
        if (level == null || level.trim().equals(""))
        {

            errorMsg.append(Utils.getStringRes(context,
                    R.string.register_levelmiss));
            return false;
        }

        try
        {
            Integer.parseInt(level);
        }
        catch (NumberFormatException e)
        {

            errorMsg.append(Utils.getStringRes(context,
                    R.string.register_levelwrong));
            return false;
        }
        return true;
    }

    private boolean validateEmail(String email, StringBuilder errorMsg)
    {
        if (email == null || email.trim().equals(""))
        {

            errorMsg.append(Utils.getStringRes(context, R.string.thieuemail));
            return false;
        }

        if (email.indexOf("@") == -1)
        {

            errorMsg.append(Utils.getStringRes(context,
                    R.string.register_emailwrong));
            return false;
        }
        else
        {
            int index = email.indexOf("@");

            try
            {
                String sub = email.substring(index + 1);

                if (sub.indexOf(".") == -1)
                {

                    errorMsg.append(Utils.getStringRes(context,
                            R.string.register_emailwrong));
                    return false;
                }
            }
            catch (IndexOutOfBoundsException e)
            {

                errorMsg.append(Utils.getStringRes(context,
                        R.string.register_emailwrong));
                return false;
            }
        }
        return true;
    }

    private boolean validatePhone(String phone, StringBuilder errorMsg)
    {
        String str = context.getResources().getString(
                R.string.register_phonewrong);
        if (phone == null || phone.trim().equals(""))
        {
            // errorMsg.append("Nháº­p sá»‘ Ä‘iá»‡n thoáº¡i");
            return true;
        }
        else
        {
            try
            {
                Integer.parseInt(phone);
                if (phone.length() < 10)
                {

                    errorMsg.append(str);
                    return false;
                }
            }
            catch (NumberFormatException e)
            {
                e.printStackTrace();
                errorMsg.append(str);
                return false;
            }
        }

        return true;
    }

    public boolean validate(String password, String email, String phone,
            StringBuilder errorMsg)
    {

        if (!validatePassword(password, errorMsg))
        {
            return false;
        }

        // if (!validateLevel(level, errorMsg))
        // {
        // return false;
        // }

        if (!validateEmail(email, errorMsg))
        {
            return false;
        }

        if (!validatePhone(phone, errorMsg))
        {
            return false;
        }

        return true;

    }

    @Override
    protected String doInBackground(String... params)
    {
        // TODO Auto-generated method stub

        String pass = params[0];
        String level = params[1];
        String email = params[2];
        String phone = params[3];

        String password = Utils.getMd5Hash(pass);
        int level_id = Integer.parseInt(level);

        User u = new User();

        u.setId(AppManager.getInstance().getUser().getId());
        u.setPassword(password);
        u.setLevel_id(level_id);
        u.setEmail(email);
        u.setPhone(phone);

        String url = null;

        {
            // url = "http://app.camemis.vn/index.php/service/register/" +
            // u.getUsername() + "/" + u.getPassword() + "/" + u.getLevel_id() +
            // "/" + "ancfjkf" + "/" + u.getPhone();
            url = IDefine.HEADER_LINK_SERVICE + "update_user";
        }

        JSONObject jo = new JSONObject();
        try
        {
            jo.put("id", u.getId());
            jo.put("password", u.getPassword());
            jo.put("levelid", u.getLevel_id());
            jo.put("email", u.getEmail());
            jo.put("phone", u.getPhone());
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        String jsonresult = Utils.postData(url, jo);

        return jsonresult;

    }

    @Override
    protected void onPreExecute()
    {

    }

    @Override
    protected void onPostExecute(String result)
    {

        // show success msg
        try
        {
            JSONObject jo = new JSONObject(result);
            int errCode = Integer.parseInt(jo.getString("error"));
            String msg = jo.getString("message");

            JSONObject m = (JSONObject) jo.getJSONArray("message").get(0);

            // phu thuoc result de set ok or err

            if (errCode == 0)
            {

                Utils.ShowToast(context, R.string.updaresuccess);

                User u = AppManager.getInstance().getUser();
                u.setCreated_date(Integer.parseInt(m.getString("created_date")));
                u.setPhone(m.getString("phone"));
                u.setUpdated_date(Integer.parseInt(m.getString("updated_date")));
                u.setEmail(m.getString("email"));
                u.setPassword(m.getString("password"));
                u.setLevel_id(Integer.parseInt(m.getString("level_id")));
                u.setPasswordDontHash(Utils.ReadData(context, "passwordtmp"));

                Utils.WriteData(context, "password",
                        Utils.ReadData(context, "passwordtmp"));

            }
            else
            {

                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();

            }

        }
        catch (JSONException e)
        {
            Utils.showErr(context);
            e.printStackTrace();
        }

    }
}
