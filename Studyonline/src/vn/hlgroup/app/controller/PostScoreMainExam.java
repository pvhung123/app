package vn.hlgroup.app.controller;

import org.json.JSONException;
import org.json.JSONObject;

import vn.hlgroup.app.R;
import vn.hlgroup.app.activity.MainExamActivity;
import vn.hlgroup.app.activity.MainExamResultActivity;
import vn.hlgroup.app.utils.Utils;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.Toast;

public class PostScoreMainExam extends AsyncTask<Integer, Integer, String>
{
    private Activity context;

    public PostScoreMainExam(Activity context)
    {
        this.context = context;
    }

    @Override
    protected String doInBackground(Integer... params)
    {
        {
        }

        String result = Utils.postDataMainExam(params[0]);

        return result;

    }

    @Override
    protected void onPreExecute()
    {
        MainExamActivity mai = (MainExamActivity) context;

        Button pre = (Button) mai.findViewById(R.id.btnPreMainExam);

        Button next = (Button) mai.findViewById(R.id.btnNextMainExam);

        Button finish = (Button) mai.findViewById(R.id.btnFinishMainExam);

        pre.setEnabled(false);
        next.setEnabled(false);
        finish.setEnabled(false);

        ProgressBar pro = (ProgressBar) mai.findViewById(R.id.proTrainning);
        pro.setVisibility(View.VISIBLE);

        RadioButton result1 = (RadioButton) mai
                .findViewById(R.id.radio0MainExam);

        RadioButton result2 = (RadioButton) mai
                .findViewById(R.id.radio1MainExam);

        RadioButton result3 = (RadioButton) mai
                .findViewById(R.id.radio2MainExam);

        RadioButton result4 = (RadioButton) mai
                .findViewById(R.id.radio3MainExam);

        result1.setEnabled(false);
        result2.setEnabled(false);
        result3.setEnabled(false);
        result4.setEnabled(false);

    }

    @Override
    protected void onPostExecute(String result)
    {

        MainExamActivity mai = (MainExamActivity) context;

        Button pre = (Button) mai.findViewById(R.id.btnPreMainExam);

        Button next = (Button) mai.findViewById(R.id.btnNextMainExam);

        Button finish = (Button) mai.findViewById(R.id.btnFinishMainExam);

        pre.setEnabled(true);
        next.setEnabled(true);
        finish.setEnabled(true);

        ProgressBar pro = (ProgressBar) mai.findViewById(R.id.proTrainning);
        pro.setVisibility(View.INVISIBLE);

        RadioButton result1 = (RadioButton) mai
                .findViewById(R.id.radio0MainExam);

        RadioButton result2 = (RadioButton) mai
                .findViewById(R.id.radio1MainExam);

        RadioButton result3 = (RadioButton) mai
                .findViewById(R.id.radio2MainExam);

        RadioButton result4 = (RadioButton) mai
                .findViewById(R.id.radio3MainExam);

        result1.setEnabled(true);
        result2.setEnabled(true);
        result3.setEnabled(true);
        result4.setEnabled(true);

        try
        {

            JSONObject jo = new JSONObject(result);
            jo.getString("message");

            int resultCode = jo.getInt("error");

            if (resultCode == 1)
            {
                // Toast.makeText(context, jomsg, Toast.LENGTH_SHORT).show();
                Toast.makeText(context, Utils.showErr(context),
                        Toast.LENGTH_SHORT).show();
            }
            else
            {
                // Toast.makeText(context, jomsg, Toast.LENGTH_SHORT).show();
                Utils.ShowToast(context, R.string.saveok);
                mai.finish();

                Intent intent = new Intent(mai, MainExamResultActivity.class);
                mai.startActivity(intent);
            }
        }
        catch (JSONException e)
        {
            Utils.showErr(context);
            e.printStackTrace();
        }

    }

}
