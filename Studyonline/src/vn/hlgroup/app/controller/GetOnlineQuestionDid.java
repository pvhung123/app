package vn.hlgroup.app.controller;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import vn.hlgroup.app.IDefine;
import vn.hlgroup.app.R;
import vn.hlgroup.app.activity.OnlineExamResultActivity;
import vn.hlgroup.app.model.Question;
import vn.hlgroup.app.singleton.AppManager;
import vn.hlgroup.app.utils.Utils;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

public class GetOnlineQuestionDid extends AsyncTask<Integer, Integer, String>
{
    private Activity context;

    public GetOnlineQuestionDid(Activity context)
    {
        this.context = context;
    }

    @Override
    protected String doInBackground(Integer... params)
    {
        // TODO Auto-generated method stub
        // check login from server
        AppManager app = AppManager.getInstance();
        Integer id = params[0];

        String url = null;

        {
            url = IDefine.HEADER_LINK_SERVICE + "get_results_detail/"
                    + app.getUser().getId() + "/" + id;
            Log.d("abc", app.getUser().getId() + ":" + id);
        }

        String jsonresult = Utils.GET(url);

        return jsonresult;

    }

    @Override
    protected void onPreExecute()
    {

    }

    @Override
    protected void onPostExecute(String result)
    {

        AppManager app = AppManager.getInstance();

        try
        {
            JSONObject jo = new JSONObject(result);
            int err = jo.getInt("error");
            if (err == 0)
            {
                // get result
                JSONArray jorear = jo.getJSONArray("result");
                JSONObject jore = jorear.getJSONObject(0);

                int total_score = Integer.parseInt(jore
                        .getString("total_score"));
                int total_question = Integer.parseInt(jore
                        .getString("total_question"));

                app.setScoreExamOnlineDid(total_score
                        + "/"
                        + total_question
                        + ";"
                        + Utils.roundTwoDigit(String.valueOf(total_score * 100f
                                / total_question)));

                ArrayList<Question> questions = AppManager.getInstance()
                        .getOnlineQuestionsDid();
                questions.clear();
                JSONArray joarr = jo.getJSONArray("message");

                for (int i = 0; i < joarr.length(); i++)
                {
                    Question q = new Question();
                    JSONObject joarrObject = joarr.getJSONObject(i);

                    // app.getResultBoard().getExamDetails().get(i).setQuestionid(Integer.parseInt(joarrObject.getString("id")));

                    q.setId(Integer.parseInt(joarrObject.getString("id")));
                    // q.setLevelId(Integer.parseInt(joarrObject.getString("level_id")));

                    // q.setTypeId(Integer.parseInt(joarrObject.getString("type_id")));
                    // q.setSubjectId(Integer.parseInt(joarrObject.getString("subject_id")));

                    // q.setLessonId(Integer.parseInt(joarrObject.getString("lesson_id")));
                    q.setTitle(joarrObject.getString("title"));
                    q.setResult1(joarrObject.getString("result1"));
                    q.setResult2(joarrObject.getString("result2"));
                    q.setResult3(joarrObject.getString("result3"));
                    q.setResult4(joarrObject.getString("result4"));
                    q.setResult(joarrObject.getString("result"));
                    q.setQuestion_result(joarrObject
                            .getString("question_result"));
                    q.setExamId(joarrObject.getString("exam_id").equals("null") ? 0
                            : Integer.parseInt(joarrObject.getString("exam_id")));
                    q.setIsImage(joarrObject.getString("is_image").equals(
                            "null") ? 0 : Integer.parseInt(joarrObject
                            .getString("is_image")));// hardcode
                    // q.setCreated_date(joarrObject.getString("created_date").equals("null")?0:Integer.parseInt(joarrObject.getString("created_date")));
                    // q.setUpdated_date(joarrObject.getString("updated_date").equals("null")?0:Integer.parseInt(joarrObject.getString("updated_date")));

                    questions.add(q);
                }

                context.finish();

                Intent intent = new Intent(context,
                        OnlineExamResultActivity.class);
                context.startActivity(intent);
            }
            else
            {
                Utils.ShowToast(context, R.string.error);
            }

        }
        catch (JSONException e)
        {
            e.printStackTrace();
            Utils.ShowToast(context, R.string.onlinequestion_dont_have_did);
        }

    }
}
