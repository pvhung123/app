package vn.hlgroup.app.controller;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import vn.hlgroup.app.IDefine;
import vn.hlgroup.app.R;
import vn.hlgroup.app.activity.InfoActivity;
import vn.hlgroup.app.activity.RegisterActivity;
import vn.hlgroup.app.model.LevelId;
import vn.hlgroup.app.singleton.AppManager;
import vn.hlgroup.app.utils.Utils;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.Toast;

public class GetLevelId extends AsyncTask<Integer, Integer, String>
{
    private Activity context;

    public GetLevelId(Activity context)
    {
        this.context = context;
    }

    @Override
    protected String doInBackground(Integer... params)
    {
        String url = null;

        {
            url = IDefine.HEADER_LINK_SERVICE + "getLevelId";
        }

        String jsonresult = Utils.GET(url);

        jsonresult = params[0] + jsonresult;

        return jsonresult;

    }

    @Override
    protected void onPreExecute()
    {

    }

    @Override
    protected void onPostExecute(String result)
    {

        AppManager.getInstance();

        int direct = Integer.parseInt(result.substring(0, 1));
        result = result.substring(1);

        try
        {
            JSONObject jo = new JSONObject(result);
            int err = jo.getInt("error");
            if (err == 0)
            {
                ArrayList<LevelId> levelIds = AppManager.getInstance()
                        .getLevelId();
                levelIds.clear();
                JSONArray joarr = jo.getJSONArray("message");

                LevelId ltmp = new LevelId();

                ltmp.setId(-1);
                ltmp.setName(context.getResources().getString(
                        R.string.chooseleveltext));
                ltmp.setdescription(context.getResources().getString(
                        R.string.chooseleveltext));

                levelIds.add(ltmp);

                for (int i = 0; i < joarr.length(); i++)
                {
                    LevelId l = new LevelId();
                    JSONObject joarrObject = joarr.getJSONObject(i);

                    l.setId(Integer.parseInt(joarrObject.getString("id")));
                    l.setName(joarrObject.getString("name"));
                    l.setdescription(joarrObject.getString("description"));

                    levelIds.add(l);
                }

                context.finish();
                Intent intent = null;
                if (direct == 0)
                {
                    intent = new Intent(context, RegisterActivity.class);
                }
                else
                {
                    intent = new Intent(context, InfoActivity.class);
                }
                context.startActivity(intent);
            }
            else
            {
                Toast.makeText(context,
                        Utils.getStringRes(context, R.string.error),
                        Toast.LENGTH_SHORT).show();
            }

        }
        catch (JSONException e)
        {
            e.printStackTrace();
            Toast.makeText(context,
                    Utils.getStringRes(context, R.string.error),
                    Toast.LENGTH_SHORT).show();
        }

    }
}
