package vn.hlgroup.app.controller;

import org.json.JSONException;
import org.json.JSONObject;

import vn.hlgroup.app.IDefine;
import vn.hlgroup.app.R;
import vn.hlgroup.app.activity.CoreActivity;
import vn.hlgroup.app.activity.MainActivity;
import vn.hlgroup.app.singleton.AppManager;
import vn.hlgroup.app.utils.Utils;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.Toast;

public class Logout extends AsyncTask<String, Integer, String>
{
    private Activity context;

    public Logout(Activity context)
    {
        this.context = context;
    }

    @Override
    protected String doInBackground(String... params)
    {
        // TODO Auto-generated method stub
        // check login from server

        AppManager app = AppManager.getInstance();

        String user = params[0];

        String url = null;

        {
            url = IDefine.HEADER_LINK_SERVICE + "logout/";
        }
        // String url = "http://192.168.1.2/appadmin/index.php/service/login/";
        // String url = "http://192.168.1.19/service/login/";
        url += user;
        // url += "hung" + "/" + "hung";

        String jsonresult = Utils.GET(url);

        return jsonresult;

    }

    @Override
    protected void onPreExecute()
    {

    }

    @Override
    protected void onPostExecute(String result)
    {
        try
        {
            JSONObject jo = new JSONObject(result);

            int resultCode = jo.getInt("error");

            if (resultCode == 0)
            {

                context.finish();
                Intent intent = new Intent(context, MainActivity.class);

                ((CoreActivity) context).startActivityBack(intent);

            }
            else
            {
                Toast.makeText(context,
                        Utils.getStringRes(context, R.string.error),
                        Toast.LENGTH_SHORT).show();
            }
        }
        catch (JSONException e)
        {
            e.printStackTrace();
            Toast.makeText(context,
                    Utils.getStringRes(context, R.string.error),
                    Toast.LENGTH_SHORT).show();
        }

    }

}
