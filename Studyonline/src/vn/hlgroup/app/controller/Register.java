package vn.hlgroup.app.controller;

import org.json.JSONException;
import org.json.JSONObject;

import vn.hlgroup.app.IDefine;
import vn.hlgroup.app.R;
import vn.hlgroup.app.activity.MainActivity;
import vn.hlgroup.app.activity.RegisterActivity;
import vn.hlgroup.app.model.User;
import vn.hlgroup.app.utils.Utils;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

public class Register extends AsyncTask<String, Integer, String> implements
        IDefine
{
    private Activity context;

    public Register(Activity context)
    {
        this.context = context;
    }

    private boolean validateUsername(String username, StringBuilder errorMsg)
    {
        if (username == null || username.trim().equals(""))
        {
            String str = context.getResources().getString(
                    R.string.register_usernamemiss);
            errorMsg.append(str);

            return false;
        }
        return true;
    }

    private boolean validatePassword(String password, StringBuilder errorMsg)
    {
        if (password == null || password.trim().equals(""))
        {
            String str = context.getResources().getString(
                    R.string.register_passmiss);
            errorMsg.append(str);
            return false;
        }
        else
            if (password.length() < 6)
            {
                String str = context.getResources().getString(
                        R.string.register_passlagersixletter);
                errorMsg.append(str);
                return false;
            }
        return true;
    }

    private boolean validateLevel(String level, StringBuilder errorMsg)
    {
        if (level == null || level.trim().equals(""))
        {

            errorMsg.append(Utils.getStringRes(context,
                    R.string.register_levelmiss));
            return false;
        }

        try
        {
            Integer.parseInt(level);
        }
        catch (NumberFormatException e)
        {

            errorMsg.append(Utils.getStringRes(context,
                    R.string.register_levelwrong));
            return false;
        }
        return true;
    }

    private boolean validateEmail(String email, StringBuilder errorMsg)
    {
        if (email == null || email.trim().equals(""))
        {

            errorMsg.append(Utils.getStringRes(context, R.string.thieuemail));
            return false;
        }

        if (email.indexOf("@") == -1)
        {

            errorMsg.append(Utils.getStringRes(context,
                    R.string.register_emailwrong));
            return false;
        }
        else
        {
            int index = email.indexOf("@");

            try
            {
                String sub = email.substring(index + 1);

                if (sub.indexOf(".") == -1)
                {

                    errorMsg.append(Utils.getStringRes(context,
                            R.string.register_emailwrong));
                    return false;
                }
            }
            catch (IndexOutOfBoundsException e)
            {

                errorMsg.append(Utils.getStringRes(context,
                        R.string.register_emailwrong));
                return false;
            }
        }
        return true;
    }

    private boolean validatePhone(String phone, StringBuilder errorMsg)
    {
        String str = context.getResources().getString(
                R.string.register_phonewrong);
        if (phone == null || phone.trim().equals(""))
        {
            // errorMsg.append("Nháº­p sá»‘ Ä‘iá»‡n thoáº¡i");
            return true;
        }
        else
        {
            try
            {
                Integer.parseInt(phone);
                if (phone.length() < 10)
                {

                    errorMsg.append(str);
                    return false;
                }
            }
            catch (NumberFormatException e)
            {
                e.printStackTrace();
                errorMsg.append(str);
                return false;
            }
        }

        return true;
    }

    public boolean validate(String username, String password,
            String passwordConfirm, String level, String email, String phone,
            StringBuilder errorMsg)
    {
        if (!validateUsername(username, errorMsg))
        {

            return false;
        }

        if (!validatePassword(password, errorMsg))
        {
            return false;
        }

        if (!password.equals(passwordConfirm))
        {
            String str = context.getResources().getString(
                    R.string.register_regconfirmtext);
            errorMsg.append(str);
            return false;
        }

        if (!validateLevel(level, errorMsg))
        {
            return false;
        }

        if (!validateEmail(email, errorMsg))
        {
            return false;
        }

        if (!validatePhone(phone, errorMsg))
        {
            return false;
        }

        return true;

    }

    @Override
    protected String doInBackground(String... params)
    {
        // TODO Auto-generated method stub

        String user = params[0];
        String pass = params[1];
        String level = params[2];
        String email = params[3];
        String phone = params[4];

        String password = Utils.getMd5Hash(pass);
        int level_id = Integer.parseInt(level);
        int created_date = (int) (System.currentTimeMillis() / 1000);
        int updated_date = created_date;

        User u = new User();

        u.setUsername(user);
        u.setPassword(password);
        u.setLevel_id(level_id);
        u.setEmail(email);
        u.setPhone(phone);
        u.setCreated_date(created_date);
        u.setUpdated_date(updated_date);

        String url = null;

        {
            // url = "http://app.camemis.vn/index.php/service/register/" +
            // u.getUsername() + "/" + u.getPassword() + "/" + u.getLevel_id() +
            // "/" + "ancfjkf" + "/" + u.getPhone();
            url = IDefine.HEADER_LINK_SERVICE + "register_post";
        }

        JSONObject jo = new JSONObject();
        try
        {
            jo.put("username", u.getUsername());
            jo.put("password", u.getPassword());
            jo.put("levelid", u.getLevel_id());
            jo.put("email", u.getEmail());
            jo.put("phone", u.getPhone());
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        String jsonresult = Utils.postData(url, jo);

        return jsonresult;

    }

    @Override
    protected void onPreExecute()
    {

        // show loading bar
        // disable txt, btn
        RegisterActivity resa = (RegisterActivity) context;
        ProgressBar bar = (ProgressBar) resa.findViewById(R.id.proBarRe);

        bar.setVisibility(View.VISIBLE);

        EditText usernameText = (EditText) resa.findViewById(R.id.txtURe);
        usernameText.setEnabled(false);
        usernameText.setText("");
        EditText passwordText = (EditText) resa.findViewById(R.id.txtPRe);
        passwordText.setEnabled(false);
        passwordText.setText("");
        EditText passwordTextConfirm = (EditText) resa
                .findViewById(R.id.txtPReConfirm);
        passwordTextConfirm.setEnabled(false);
        passwordTextConfirm.setText("");
        Spinner levelText = (Spinner) resa.findViewById(R.id.spinLevelRe);
        levelText.setEnabled(false);
        // levelText.setText("");
        EditText emailText = (EditText) resa.findViewById(R.id.txtEmailRe);
        emailText.setEnabled(false);
        emailText.setText("");
        EditText phoneText = (EditText) resa.findViewById(R.id.txtPhoneRe);
        phoneText.setEnabled(false);
        phoneText.setText("");
        Button register = (Button) resa.findViewById(R.id.btnRegisterRe);
        register.setEnabled(false);

        resa.setPressBackey(false);

    }

    @Override
    protected void onPostExecute(String result)
    {
        RegisterActivity resa = (RegisterActivity) context;
        ProgressBar bar = (ProgressBar) resa.findViewById(R.id.proBarRe);

        bar.setVisibility(View.INVISIBLE);

        EditText usernameText = (EditText) resa.findViewById(R.id.txtURe);
        usernameText.setEnabled(true);
        EditText passwordText = (EditText) resa.findViewById(R.id.txtPRe);
        passwordText.setEnabled(true);
        EditText passwordTextConfirm = (EditText) resa
                .findViewById(R.id.txtPReConfirm);
        passwordTextConfirm.setEnabled(true);
        Spinner levelText = (Spinner) resa.findViewById(R.id.spinLevelRe);
        levelText.setEnabled(true);
        EditText emailText = (EditText) resa.findViewById(R.id.txtEmailRe);
        emailText.setEnabled(true);
        EditText phoneText = (EditText) resa.findViewById(R.id.txtPhoneRe);
        phoneText.setEnabled(true);
        Button register = (Button) resa.findViewById(R.id.btnRegisterRe);
        register.setEnabled(true);

        resa.setPressBackey(true);

        // show success msg
        try
        {
            JSONObject jo = new JSONObject(result);
            int errCode = Integer.parseInt(jo.getString("error"));
            String msg = jo.getString("message");

            // phu thuoc result de set ok or err

            if (errCode == 0)
            {

                Toast.makeText(context,
                        Utils.getStringRes(context, R.string.dangkythanhcong),
                        Toast.LENGTH_SHORT).show();

                context.finish();

                Intent intent = new Intent(context, MainActivity.class);
                ((RegisterActivity) context).startActivityBack(intent);

            }
            else
            {

                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();

            }

        }
        catch (JSONException e)
        {
            Toast.makeText(context,
                    Utils.getStringRes(context, R.string.register_fail),
                    Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }

    }
}
