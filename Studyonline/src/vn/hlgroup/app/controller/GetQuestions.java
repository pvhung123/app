package vn.hlgroup.app.controller;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import vn.hlgroup.app.IDefine;
import vn.hlgroup.app.R;
import vn.hlgroup.app.activity.TrainOptionActivity;
import vn.hlgroup.app.activity.TrainningActivity;
import vn.hlgroup.app.model.Question;
import vn.hlgroup.app.singleton.AppManager;
import vn.hlgroup.app.utils.Utils;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Spinner;

public class GetQuestions extends AsyncTask<Integer, Integer, String>
{
    private Activity context;

    public GetQuestions(Activity context)
    {
        this.context = context;
    }

    @Override
    protected String doInBackground(Integer... params)
    {
        // TODO Auto-generated method stub
        // check login from server

        Integer cataog = params[0];
        Integer levelId = params[1];
        Integer type = params[2];
        Integer subject = params[3];
        Integer topic = params[4];
        Integer numberQ = params[5];

        String url = null;

        {
            url = IDefine.HEADER_LINK_SERVICE + "get_questions/" + cataog + "/"
                    + levelId + "/" + type + "/" + subject + "/" + topic + "/"
                    + numberQ;// last param is second time
        }

        String jsonresult = Utils.GET(url);

        return jsonresult;

    }

    @Override
    protected void onPreExecute()
    {

        TrainOptionActivity mai = (TrainOptionActivity) context;

        ProgressBar bar = (ProgressBar) mai
                .findViewById(R.id.proTrainningOption);
        bar.setVisibility(View.VISIBLE);

        Button btnStartTrain = (Button) mai
                .findViewById(R.id.btnStartTrainningOption);
        btnStartTrain.setEnabled(false);

        Spinner spintype = (Spinner) mai.findViewById(R.id.spinKnowledge);
        spintype.setEnabled(false);

        Spinner spinsubject = (Spinner) mai.findViewById(R.id.spinSubject);
        spinsubject.setEnabled(false);

        Spinner spintopic = (Spinner) mai.findViewById(R.id.spintpic);
        spintopic.setEnabled(false);

        Spinner spinnumQ = (Spinner) mai.findViewById(R.id.spinNumberQ);
        spinnumQ.setEnabled(false);

    }

    @Override
    protected void onPostExecute(String result)
    {

        AppManager app = AppManager.getInstance();
        TrainOptionActivity mai = (TrainOptionActivity) context;

        ProgressBar bar = (ProgressBar) mai
                .findViewById(R.id.proTrainningOption);
        bar.setVisibility(View.INVISIBLE);

        Button btnStartTrain = (Button) mai
                .findViewById(R.id.btnStartTrainningOption);
        btnStartTrain.setEnabled(true);

        Spinner spintype = (Spinner) mai.findViewById(R.id.spinKnowledge);
        spintype.setEnabled(true);

        Spinner spinsubject = (Spinner) mai.findViewById(R.id.spinSubject);
        spinsubject.setEnabled(true);

        Spinner spintopic = (Spinner) mai.findViewById(R.id.spintpic);
        spintopic.setEnabled(true);

        Spinner spinnumQ = (Spinner) mai.findViewById(R.id.spinNumberQ);
        spinnumQ.setEnabled(true);

        try
        {
            JSONObject jo = new JSONObject(result);
            int err = jo.getInt("error");
            if (err == 0)
            {
                ArrayList<Question> questions = AppManager.getInstance()
                        .getQuestions();
                questions.clear();
                JSONArray joarr = jo.getJSONArray("message");

                app.getResultBoard().setExamId(jo.getInt("exam_id"));
                app.getResultBoard().initExamDetails(joarr.length());

                // ArrayList<ExamDetail> examDetail = new
                // ArrayList<ExamDetail>(joarr.length());
                // app.getResultBoard().setexamDetails(examDetail);

                for (int i = 0; i < joarr.length(); i++)
                {
                    Question q = new Question();
                    JSONObject joarrObject = joarr.getJSONObject(i);

                    app.getResultBoard()
                            .getExamDetails()
                            .get(i)
                            .setQuestionid(
                                    Integer.parseInt(joarrObject
                                            .getString("id")));

                    q.setId(Integer.parseInt(joarrObject.getString("id")));
                    q.setLevelId(Integer.parseInt(joarrObject
                            .getString("level_id")));

                    q.setTypeId(Integer.parseInt(joarrObject
                            .getString("type_id")));
                    q.setSubjectId(Integer.parseInt(joarrObject
                            .getString("subject_id")));

                    q.setLessonId(Integer.parseInt(joarrObject
                            .getString("lesson_id")));
                    q.setTitle(joarrObject.getString("title"));
                    q.setResult1(joarrObject.getString("result1"));
                    q.setResult2(joarrObject.getString("result2"));
                    q.setResult3(joarrObject.getString("result3"));
                    q.setResult4(joarrObject.getString("result4"));

                    q.setResult(joarrObject.getString("result"));
                    q.setExamId(joarrObject.getString("exam_id").equals("null") ? 0
                            : Integer.parseInt(joarrObject.getString("exam_id")));
                    q.setIsImage(joarrObject.getString("is_image").equals(
                            "null") ? 0 : Integer.parseInt(joarrObject
                            .getString("is_image")));
                    q.setCreated_date(joarrObject.getString("created_date")
                            .equals("null") ? 0 : Integer.parseInt(joarrObject
                            .getString("created_date")));
                    q.setUpdated_date(joarrObject.getString("updated_date")
                            .equals("null") ? 0 : Integer.parseInt(joarrObject
                            .getString("updated_date")));

                    questions.add(q);
                }

                context.finish();

                Intent intent = new Intent(context, TrainningActivity.class);
                context.startActivity(intent);
            }
            else
            {
                Utils.ShowToast(mai, R.string.error);
            }

        }
        catch (JSONException e)
        {
            e.printStackTrace();
            Utils.ShowToast(mai, R.string.question_dont_have);
        }

    }
}
