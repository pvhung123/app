package vn.hlgroup.app.controller;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import vn.hlgroup.app.IDefine;
import vn.hlgroup.app.R;
import vn.hlgroup.app.activity.MainMenuActivity;
import vn.hlgroup.app.activity.TrainOptionActivity;
import vn.hlgroup.app.model.Lesson;
import vn.hlgroup.app.model.Subject;
import vn.hlgroup.app.model.Type;
import vn.hlgroup.app.singleton.AppManager;
import vn.hlgroup.app.utils.Utils;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.Button;

public class GetTypeDefault extends AsyncTask<Void, Integer, String>
{
    private Activity context;

    public GetTypeDefault(Activity context)
    {
        this.context = context;
    }

    @Override
    protected String doInBackground(Void... params)
    {
        // TODO Auto-generated method stub

        String urltype = null;
        String urlsubject = null;
        String urltopic = null;

        {
            urltype = IDefine.HEADER_LINK_SERVICE + "get_types";
            urlsubject = IDefine.HEADER_LINK_SERVICE
                    + "get_subjects_by_type_id/1";
            urltopic = IDefine.HEADER_LINK_SERVICE + "get_lessons";
        }

        String jsonresulttype = Utils.GET(urltype);
        String jsonresulsubject = Utils.GET(urlsubject);
        String jsonresulttopic = Utils.GET(urltopic);

        String finalResult = "{\"final\":[" + jsonresulttype + ","
                + jsonresulsubject + "," + jsonresulttopic + "]}";

        return finalResult;

    }

    @Override
    protected void onPreExecute()
    {

        MainMenuActivity mainmenu = (MainMenuActivity) context;

        Button btnInfo = (Button) mainmenu.findViewById(R.id.btnInfomain);
        btnInfo.setEnabled(false);
        Button btnTrain = (Button) mainmenu.findViewById(R.id.btnOnthiMain);
        btnTrain.setEnabled(false);
        Button btnMainExam = (Button) mainmenu
                .findViewById(R.id.btnThichungMain);
        btnMainExam.setEnabled(false);
        Button btnOnlineExam = (Button) mainmenu
                .findViewById(R.id.btnOnlineMain);
        btnOnlineExam.setEnabled(false);
        // Button btnTut = (Button)mainmenu.findViewById(R.id.btnTutorials);
        // btnTut.setEnabled(false);

        // ProgressBar pro =
        // (ProgressBar)mainmenu.findViewById(R.id.promainmenu);
        // pro.setVisibility(View.VISIBLE);

    }

    @Override
    protected void onPostExecute(String result)
    {

        ArrayList<Type> types = AppManager.getInstance().getTypes();
        ArrayList<Subject> subjects = AppManager.getInstance().getSubjects();
        ArrayList<Lesson> lessons = AppManager.getInstance().getLessons();
        types.clear();
        subjects.clear();
        lessons.clear();
        try
        {
            JSONObject jofinal = new JSONObject(result);
            JSONArray jofinalarr = jofinal.getJSONArray("final");

            // for types
            JSONObject jotypes = jofinalarr.getJSONObject(0);
            JSONObject josubjects = jofinalarr.getJSONObject(1);
            JSONObject jotopics = jofinalarr.getJSONObject(2);

            int err0 = jotypes.getInt("error");
            int err1 = josubjects.getInt("error");
            int err2 = jotopics.getInt("error");

            if (err0 == 0 && err1 == 0 && err2 == 0)
            {
                // save types here
                JSONArray jomsg = jotypes.getJSONArray("message");
                for (int i = 0; i < jomsg.length(); i++)
                {
                    JSONObject jotype = jomsg.getJSONObject(i);
                    Type t = new Type();
                    t.setId(Integer.parseInt(jotype.getString("id")));
                    t.setName(jotype.getString("name"));

                    types.add(t);
                }

                // save subject here
                JSONArray jomsg1 = josubjects.getJSONArray("message");
                for (int i = 0; i < jomsg1.length(); i++)
                {
                    JSONObject josubject = jomsg1.getJSONObject(i);
                    Subject s = new Subject();
                    s.setId(Integer.parseInt(josubject.getString("id")));
                    s.setType_Id(Integer.parseInt(josubject
                            .getString("type_id")));
                    s.setName(josubject.getString("name"));

                    subjects.add(s);
                }

                // save lesson here
                JSONArray jomsg2 = jotopics.getJSONArray("message");
                for (int i = 0; i < jomsg2.length(); i++)
                {
                    JSONObject jolesson = jomsg2.getJSONObject(i);
                    Lesson l = new Lesson();
                    l.setId(Integer.parseInt(jolesson.getString("id")));
                    // l.setSubjectId(Integer.parseInt(jolesson.getString("subject_id")));
                    l.setName(jolesson.getString("name"));

                    lessons.add(l);
                }

                context.finish();

                Intent intent = new Intent(context, TrainOptionActivity.class);
                context.startActivity(intent);
            }

        }
        catch (JSONException e)
        {
            Utils.ShowToast(context, R.string.error);
            e.printStackTrace();
        }

        MainMenuActivity mainmenu = (MainMenuActivity) context;

        Button btnInfo = (Button) mainmenu.findViewById(R.id.btnInfomain);
        btnInfo.setEnabled(true);
        Button btnTrain = (Button) mainmenu.findViewById(R.id.btnOnthiMain);
        btnTrain.setEnabled(true);
        Button btnMainExam = (Button) mainmenu
                .findViewById(R.id.btnThichungMain);
        btnMainExam.setEnabled(true);
        Button btnOnlineExam = (Button) mainmenu
                .findViewById(R.id.btnOnlineMain);
        btnOnlineExam.setEnabled(true);
        // Button btnTut = (Button)mainmenu.findViewById(R.id.btnTutorials);
        // btnTut.setEnabled(true);

        // ProgressBar pro =
        // (ProgressBar)mainmenu.findViewById(R.id.promainmenu);
        // pro.setVisibility(View.INVISIBLE);

    }

}
