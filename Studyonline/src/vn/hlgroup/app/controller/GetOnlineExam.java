package vn.hlgroup.app.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import vn.hlgroup.app.IDefine;
import vn.hlgroup.app.R;
import vn.hlgroup.app.activity.OnlineExamMenuActivity;
import vn.hlgroup.app.model.ExamOnline;
import vn.hlgroup.app.model.Subject;
import vn.hlgroup.app.singleton.AppManager;
import vn.hlgroup.app.utils.Utils;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

public class GetOnlineExam extends AsyncTask<Void, Integer, String>
{
    private Activity context;

    public GetOnlineExam(Activity context)
    {
        this.context = context;
    }

    @Override
    protected String doInBackground(Void... params)
    {
        // TODO Auto-generated method stub
        // check login from server
        try
        {

            String urlsubjectAll = IDefine.HEADER_LINK_SERVICE + "get_subjects";
            String subjectAll = Utils.GET(urlsubjectAll);

            JSONObject josubjectAll = new JSONObject(subjectAll);

            ArrayList<Subject> subjectAllA = AppManager.getInstance()
                    .getSubjectsAll();
            subjectAllA.clear();
            JSONArray josubjectallmsg = josubjectAll.getJSONArray("message");
            for (int i = 0; i < josubjectallmsg.length(); i++)
            {
                JSONObject josubject = josubjectallmsg.getJSONObject(i);
                Subject t = new Subject();
                t.setId(Integer.parseInt(josubject.getString("id")));
                t.setName(josubject.getString("name"));

                subjectAllA.add(t);
            }

            String url = IDefine.HEADER_LINK_SERVICE + "get_exam_onlines/"
                    + AppManager.getInstance().getUser().getId() + "/"
                    + AppManager.getInstance().getUser().getLevel_id();
            Log.d("abc", url);

            String examsonlinejson = Utils.GET(url);

            return examsonlinejson;
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
        return null;

    }

    @Override
    protected void onPreExecute()
    {

    }

    @Override
    protected void onPostExecute(String result)
    {
        AppManager app = AppManager.getInstance();
        try
        {
            JSONObject jo = new JSONObject(result);

            int err = jo.getInt("error");

            if (err == 0)
            {
                ArrayList<String> didId = new ArrayList<String>();
                JSONArray joamsgresult = jo.getJSONArray("results");
                for (int i = 0; i < joamsgresult.length(); i++)
                {
                    JSONObject ro = joamsgresult.getJSONObject(i);
                    didId.add(ro.getString("exam_id"));
                }

                ArrayList<ExamOnline> onlineExmas = app.getOnlineExams();
                onlineExmas.clear();
                JSONArray joamsg = jo.getJSONArray("message");

                for (int i = 0; i < joamsg.length(); i++)
                {
                    JSONObject ele = joamsg.getJSONObject(i);
                    ExamOnline eo = new ExamOnline();

                    eo.setId(Integer.parseInt(ele.getString("id")));
                    eo.setTitle(ele.getString("title"));
                    eo.setCataloid(Integer.parseInt(ele
                            .getString("category_id")));
                    String exday = ele.getString("expired_date");

                    SimpleDateFormat formatter = new SimpleDateFormat(
                            "yyyy-MM-dd HH:mm:ss");

                    Date a = formatter.parse(exday);
                    eo.setExpiredDate(a.getTime());
                    eo.setSubjectId(Integer.parseInt(ele
                            .getString("subject_id")));
                    eo.setLevelId(Integer.parseInt(ele.getString("level_id")));
                    eo.setTypeId(Integer.parseInt(ele.getString("type_id")));
                    eo.setLessonId(Integer.parseInt(ele.getString("lesson_id")));
                    eo.setNumberOfQuestion(Integer.parseInt(ele
                            .getString("number_of_question")));
                    eo.setTimeToTest(Integer.parseInt(ele
                            .getString("time_to_test")));
                    eo.setCreatedDate(Long.parseLong(ele
                            .getString("created_date")));
                    eo.setUpdatedDate(Long.parseLong(ele
                            .getString("updated_date")));
                    eo.setActive(Integer.parseInt(ele.getString("is_active")) == 1 ? true
                            : false);

                    for (int j = 0; j < didId.size(); j++)
                    {
                        if (didId.get(j).equals(ele.getString("id")))
                        {
                            eo.setDidExam(true);
                            break;
                        }
                    }

                    onlineExmas.add(eo);
                }

                context.finish();

                Intent intent = new Intent(context,
                        OnlineExamMenuActivity.class);
                context.startActivity(intent);

            }
            else
            {
                Utils.ShowToast(context, R.string.error);
            }
        }
        catch (JSONException e)
        {
            // Utils.ShowToast(context, R.string.online_dont_have);
            ArrayList<ExamOnline> onlineExmas = app.getOnlineExams();
            onlineExmas.clear();

            context.finish();

            Intent intent = new Intent(context, OnlineExamMenuActivity.class);
            context.startActivity(intent);

            e.printStackTrace();
        }
        catch (ParseException e)
        {
            Utils.ShowToast(context, R.string.error);
            e.printStackTrace();
        }

    }
}
