package vn.hlgroup.app.controller;

import vn.hlgroup.app.R;
import vn.hlgroup.app.activity.MainExamActivity;
import vn.hlgroup.app.activity.MainExamResultActivity;
import vn.hlgroup.app.activity.OnlineExamActivity;
import vn.hlgroup.app.activity.OnlineExamResultActivity;
import vn.hlgroup.app.activity.ResultActivity;
import vn.hlgroup.app.activity.TrainningActivity;
import vn.hlgroup.app.model.ExamDetail;
import vn.hlgroup.app.singleton.AppManager;
import vn.hlgroup.app.utils.Utils;
import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.widget.ImageView;
import android.widget.RadioButton;

public class GetImages extends AsyncTask<String, Integer, Bitmap[]>
{
    private Activity context;

    public GetImages(Activity context)
    {
        this.context = context;
    }

    @Override
    protected Bitmap[] doInBackground(String... params)
    {
        // TODO Auto-generated method stub
        // check login from server

        Bitmap[] bitmaps = new Bitmap[5];

        for (int i = 0; i < 5; i++)
        {
            bitmaps[i] = Utils.getImageBitmap(params[i]);
        }

        return bitmaps;

    }

    @Override
    protected void onPreExecute()
    {

    }

    @Override
    protected void onPostExecute(Bitmap[] result)
    {
        if (context instanceof TrainningActivity)
        {
            TrainningActivity trainAc = (TrainningActivity) context;

            ImageView imgViewTitle = (ImageView) trainAc
                    .findViewById(R.id.imgQuestionTitle);
            imgViewTitle.setImageBitmap(result[0]);

            RadioButton result1 = (RadioButton) trainAc
                    .findViewById(R.id.radio0Tranning);
            BitmapDrawable animg = new BitmapDrawable(result[1]);
            animg.setBounds(0, 0, animg.getIntrinsicWidth() * 3 / 2,
                    animg.getIntrinsicHeight() * 3 / 2);
            result1.setCompoundDrawables(animg, null, null, null);

            RadioButton result2 = (RadioButton) trainAc
                    .findViewById(R.id.radio1Tranning);
            BitmapDrawable animg1 = new BitmapDrawable(result[2]);
            animg1.setBounds(0, 0, animg1.getIntrinsicWidth() * 3 / 2,
                    animg1.getIntrinsicHeight() * 3 / 2);
            result2.setCompoundDrawables(animg1, null, null, null);

            RadioButton result3 = (RadioButton) trainAc
                    .findViewById(R.id.radio2Tranning);
            BitmapDrawable animg2 = new BitmapDrawable(result[3]);
            animg2.setBounds(0, 0, animg2.getIntrinsicWidth() * 3 / 2,
                    animg2.getIntrinsicHeight() * 3 / 2);
            result3.setCompoundDrawables(animg2, null, null, null);

            RadioButton result4 = (RadioButton) trainAc
                    .findViewById(R.id.radio3Tranning);
            BitmapDrawable animg3 = new BitmapDrawable(result[4]);
            animg3.setBounds(0, 0, animg3.getIntrinsicWidth() * 3 / 2,
                    animg3.getIntrinsicHeight() * 3 / 2);
            result4.setCompoundDrawables(animg3, null, null, null);

            int indexTrue = Integer.parseInt(AppManager.getInstance()
                    .getQuestions()
                    .get(AppManager.getInstance().getQuestionPage())
                    .getResult());

            int i = 0;

            switch (indexTrue)
            {
                case ExamDetail.ANSWER_ONE:
                    i = 1;
                    break;
                case ExamDetail.ANSWER_TWO:
                    i = 2;
                    break;
                case ExamDetail.ANSWER_THREE:
                    i = 3;
                    break;
                case ExamDetail.ANSWER_FOUR:
                    i = 4;
                    break;
            }

            trainAc.iView.setImageBitmap(result[i]);
        }
        else
            if (context instanceof ResultActivity)
            {
                ResultActivity trainAc = (ResultActivity) context;

                ImageView imgViewTitle = (ImageView) trainAc
                        .findViewById(R.id.imgQuestionTitleResult);
                imgViewTitle.setImageBitmap(result[0]);

                RadioButton result1 = (RadioButton) trainAc
                        .findViewById(R.id.radio0Result);
                BitmapDrawable animg = new BitmapDrawable(result[1]);
                animg.setBounds(0, 0, animg.getIntrinsicWidth() * 3 / 2,
                        animg.getIntrinsicHeight() * 3 / 2);
                result1.setCompoundDrawables(animg, null, null, null);

                RadioButton result2 = (RadioButton) trainAc
                        .findViewById(R.id.radio1Result);
                BitmapDrawable animg1 = new BitmapDrawable(result[2]);
                animg1.setBounds(0, 0, animg1.getIntrinsicWidth() * 3 / 2,
                        animg1.getIntrinsicHeight() * 3 / 2);
                result2.setCompoundDrawables(animg1, null, null, null);

                RadioButton result3 = (RadioButton) trainAc
                        .findViewById(R.id.radio2Result);
                BitmapDrawable animg2 = new BitmapDrawable(result[3]);
                animg2.setBounds(0, 0, animg2.getIntrinsicWidth() * 3 / 2,
                        animg2.getIntrinsicHeight() * 3 / 2);
                result3.setCompoundDrawables(animg2, null, null, null);

                RadioButton result4 = (RadioButton) trainAc
                        .findViewById(R.id.radio3Result);
                BitmapDrawable animg3 = new BitmapDrawable(result[4]);
                animg3.setBounds(0, 0, animg3.getIntrinsicWidth() * 3 / 2,
                        animg3.getIntrinsicHeight() * 3 / 2);
                result4.setCompoundDrawables(animg3, null, null, null);
            }
            else
                if (context instanceof MainExamActivity)
                {
                    MainExamActivity trainAc = (MainExamActivity) context;

                    ImageView imgViewTitle = (ImageView) trainAc
                            .findViewById(R.id.imgQuestionTitle);
                    imgViewTitle.setImageBitmap(result[0]);

                    RadioButton result1 = (RadioButton) trainAc
                            .findViewById(R.id.radio0MainExam);
                    BitmapDrawable animg = new BitmapDrawable(result[1]);
                    animg.setBounds(0, 0, animg.getIntrinsicWidth() * 3 / 2,
                            animg.getIntrinsicHeight() * 3 / 2);
                    result1.setCompoundDrawables(animg, null, null, null);

                    RadioButton result2 = (RadioButton) trainAc
                            .findViewById(R.id.radio1MainExam);
                    BitmapDrawable animg1 = new BitmapDrawable(result[2]);
                    animg1.setBounds(0, 0, animg1.getIntrinsicWidth() * 3 / 2,
                            animg1.getIntrinsicHeight() * 3 / 2);
                    result2.setCompoundDrawables(animg1, null, null, null);

                    RadioButton result3 = (RadioButton) trainAc
                            .findViewById(R.id.radio2MainExam);
                    BitmapDrawable animg2 = new BitmapDrawable(result[3]);
                    animg2.setBounds(0, 0, animg2.getIntrinsicWidth() * 3 / 2,
                            animg2.getIntrinsicHeight() * 3 / 2);
                    result3.setCompoundDrawables(animg2, null, null, null);

                    RadioButton result4 = (RadioButton) trainAc
                            .findViewById(R.id.radio3MainExam);
                    BitmapDrawable animg3 = new BitmapDrawable(result[4]);
                    animg3.setBounds(0, 0, animg3.getIntrinsicWidth() * 3 / 2,
                            animg3.getIntrinsicHeight() * 3 / 2);
                    result4.setCompoundDrawables(animg3, null, null, null);
                }
                else
                    if (context instanceof MainExamResultActivity)
                    {
                        MainExamResultActivity trainAc = (MainExamResultActivity) context;

                        ImageView imgViewTitle = (ImageView) trainAc
                                .findViewById(R.id.imgQuestionTitleResult);
                        imgViewTitle.setImageBitmap(result[0]);

                        RadioButton result1 = (RadioButton) trainAc
                                .findViewById(R.id.radio0Result);
                        BitmapDrawable animg = new BitmapDrawable(result[1]);
                        animg.setBounds(0, 0,
                                animg.getIntrinsicWidth() * 3 / 2,
                                animg.getIntrinsicHeight() * 3 / 2);
                        result1.setCompoundDrawables(animg, null, null, null);

                        RadioButton result2 = (RadioButton) trainAc
                                .findViewById(R.id.radio1Result);
                        BitmapDrawable animg1 = new BitmapDrawable(result[2]);
                        animg1.setBounds(0, 0,
                                animg1.getIntrinsicWidth() * 3 / 2,
                                animg1.getIntrinsicHeight() * 3 / 2);
                        result2.setCompoundDrawables(animg1, null, null, null);

                        RadioButton result3 = (RadioButton) trainAc
                                .findViewById(R.id.radio2Result);
                        BitmapDrawable animg2 = new BitmapDrawable(result[3]);
                        animg2.setBounds(0, 0,
                                animg2.getIntrinsicWidth() * 3 / 2,
                                animg2.getIntrinsicHeight() * 3 / 2);
                        result3.setCompoundDrawables(animg2, null, null, null);

                        RadioButton result4 = (RadioButton) trainAc
                                .findViewById(R.id.radio3Result);
                        BitmapDrawable animg3 = new BitmapDrawable(result[4]);
                        animg3.setBounds(0, 0,
                                animg3.getIntrinsicWidth() * 3 / 2,
                                animg3.getIntrinsicHeight() * 3 / 2);
                        result4.setCompoundDrawables(animg3, null, null, null);
                    }
                    else
                        if (context instanceof OnlineExamActivity)
                        {
                            OnlineExamActivity trainAc = (OnlineExamActivity) context;

                            ImageView imgViewTitle = (ImageView) trainAc
                                    .findViewById(R.id.imgQuestionTitle);
                            imgViewTitle.setImageBitmap(result[0]);

                            RadioButton result1 = (RadioButton) trainAc
                                    .findViewById(R.id.radio0OnlineExam);
                            BitmapDrawable animg = new BitmapDrawable(result[1]);
                            animg.setBounds(0, 0,
                                    animg.getIntrinsicWidth() * 3 / 2,
                                    animg.getIntrinsicHeight() * 3 / 2);
                            result1.setCompoundDrawables(animg, null, null,
                                    null);

                            RadioButton result2 = (RadioButton) trainAc
                                    .findViewById(R.id.radio1OnlineExam);
                            BitmapDrawable animg1 = new BitmapDrawable(
                                    result[2]);
                            animg1.setBounds(0, 0,
                                    animg1.getIntrinsicWidth() * 3 / 2,
                                    animg1.getIntrinsicHeight() * 3 / 2);
                            result2.setCompoundDrawables(animg1, null, null,
                                    null);

                            RadioButton result3 = (RadioButton) trainAc
                                    .findViewById(R.id.radio2OnlineExam);
                            BitmapDrawable animg2 = new BitmapDrawable(
                                    result[3]);
                            animg2.setBounds(0, 0,
                                    animg2.getIntrinsicWidth() * 3 / 2,
                                    animg2.getIntrinsicHeight() * 3 / 2);
                            result3.setCompoundDrawables(animg2, null, null,
                                    null);

                            RadioButton result4 = (RadioButton) trainAc
                                    .findViewById(R.id.radio3OnlineExam);
                            BitmapDrawable animg3 = new BitmapDrawable(
                                    result[4]);
                            animg3.setBounds(0, 0,
                                    animg3.getIntrinsicWidth() * 3 / 2,
                                    animg3.getIntrinsicHeight() * 3 / 2);
                            result4.setCompoundDrawables(animg3, null, null,
                                    null);
                        }
                        else
                            if (context instanceof OnlineExamResultActivity)
                            {
                                OnlineExamResultActivity trainAc = (OnlineExamResultActivity) context;

                                ImageView imgViewTitle = (ImageView) trainAc
                                        .findViewById(R.id.imgQuestionTitleResult);
                                imgViewTitle.setImageBitmap(result[0]);

                                RadioButton result1 = (RadioButton) trainAc
                                        .findViewById(R.id.radio0Result);
                                BitmapDrawable animg = new BitmapDrawable(
                                        result[1]);
                                animg.setBounds(0, 0,
                                        animg.getIntrinsicWidth() * 3 / 2,
                                        animg.getIntrinsicHeight() * 3 / 2);
                                result1.setCompoundDrawables(animg, null, null,
                                        null);

                                RadioButton result2 = (RadioButton) trainAc
                                        .findViewById(R.id.radio1Result);
                                BitmapDrawable animg1 = new BitmapDrawable(
                                        result[2]);
                                animg1.setBounds(0, 0,
                                        animg1.getIntrinsicWidth() * 3 / 2,
                                        animg1.getIntrinsicHeight() * 3 / 2);
                                result2.setCompoundDrawables(animg1, null,
                                        null, null);

                                RadioButton result3 = (RadioButton) trainAc
                                        .findViewById(R.id.radio2Result);
                                BitmapDrawable animg2 = new BitmapDrawable(
                                        result[3]);
                                animg2.setBounds(0, 0,
                                        animg2.getIntrinsicWidth() * 3 / 2,
                                        animg2.getIntrinsicHeight() * 3 / 2);
                                result3.setCompoundDrawables(animg2, null,
                                        null, null);

                                RadioButton result4 = (RadioButton) trainAc
                                        .findViewById(R.id.radio3Result);
                                BitmapDrawable animg3 = new BitmapDrawable(
                                        result[4]);
                                animg3.setBounds(0, 0,
                                        animg3.getIntrinsicWidth() * 3 / 2,
                                        animg3.getIntrinsicHeight() * 3 / 2);
                                result4.setCompoundDrawables(animg3, null,
                                        null, null);
                            }

    }
}
