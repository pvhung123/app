package vn.hlgroup.app.controller;

import org.json.JSONObject;

import vn.hlgroup.app.IDefine;
import vn.hlgroup.app.singleton.AppManager;
import vn.hlgroup.app.utils.Utils;
import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

public class SaveOpinion extends AsyncTask<String, Integer, String>
{
    private Activity context;

    public SaveOpinion(Activity context)
    {
        this.context = context;
    }

    @Override
    protected String doInBackground(String... params)
    {
        // TODO Auto-generated method stub
        // check login from server

        String content = params[0];
        String header = params[1];

        String url = null;

        // {
        // url =
        // IDefine.HEADER_LINK_SERVICE+"feedback/"+AppManager.getInstance().getUser().getId()+"/"+header+"/"+content;
        // }

        // String jsonresult = Utils.GET(url);

        {
            // url = "http://app.camemis.vn/index.php/service/register/" +
            // u.getUsername() + "/" + u.getPassword() + "/" + u.getLevel_id() +
            // "/" + "ancfjkf" + "/" + u.getPhone();
            url = IDefine.HEADER_LINK_SERVICE + "feedback";
        }

        JSONObject jo = new JSONObject();
        try
        {
            jo.put("userid", AppManager.getInstance().getUser().getId());
            jo.put("title", header);
            jo.put("description", content);
            Log.d("abc", header);
            Log.d("abc", content);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        String jsonresult = Utils.postData(url, jo);

        return jsonresult;

    }

    @Override
    protected void onPreExecute()
    {

    }

    @Override
    protected void onPostExecute(String result)
    {

        AppManager.getInstance();

        try
        {
            JSONObject jo = new JSONObject(result);
            int err = jo.getInt("error");
            if (err == 0)
            {

                Toast.makeText(context, jo.getString("message"),
                        Toast.LENGTH_SHORT).show();

            }
            else
            {
                Utils.showErr(context);
            }

        }
        catch (Exception e)
        {
            e.printStackTrace();
            Utils.showErr(context);
        }

    }
}
