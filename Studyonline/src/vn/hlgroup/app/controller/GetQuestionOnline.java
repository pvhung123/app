package vn.hlgroup.app.controller;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import vn.hlgroup.app.IDefine;
import vn.hlgroup.app.R;
import vn.hlgroup.app.activity.OnlineExamActivity;
import vn.hlgroup.app.model.Question;
import vn.hlgroup.app.singleton.AppManager;
import vn.hlgroup.app.utils.Utils;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;

public class GetQuestionOnline extends AsyncTask<Integer, Integer, String>
{
    private Activity context;

    public GetQuestionOnline(Activity context)
    {
        this.context = context;
    }

    @Override
    protected String doInBackground(Integer... params)
    {
        // TODO Auto-generated method stub
        // check login from server

        Integer id = params[0];

        String url = null;

        {
            url = IDefine.HEADER_LINK_SERVICE + "get_questions_online/" + id;
        }

        String jsonresult = Utils.GET(url);

        return jsonresult;

    }

    @Override
    protected void onPreExecute()
    {

    }

    @Override
    protected void onPostExecute(String result)
    {

        AppManager app = AppManager.getInstance();

        try
        {
            JSONObject jo = new JSONObject(result);
            int err = jo.getInt("error");
            if (err == 0)
            {
                ArrayList<Question> questions = AppManager.getInstance()
                        .getQuestions();
                questions.clear();
                JSONArray joarr = jo.getJSONArray("message");

                app.getResultBoard().setExamId(
                        app.getOnlineExams().get(app.getIdOfOnlineExam())
                                .getId());
                app.getResultBoard().initExamDetails(joarr.length());

                for (int i = 0; i < joarr.length(); i++)
                {
                    Question q = new Question();
                    JSONObject joarrObject = joarr.getJSONObject(i);

                    app.getResultBoard()
                            .getExamDetails()
                            .get(i)
                            .setQuestionid(
                                    Integer.parseInt(joarrObject
                                            .getString("id")));

                    q.setId(Integer.parseInt(joarrObject.getString("id")));
                    q.setLevelId(Integer.parseInt(joarrObject
                            .getString("level_id")));

                    q.setTypeId(Integer.parseInt(joarrObject
                            .getString("type_id")));
                    q.setSubjectId(Integer.parseInt(joarrObject
                            .getString("subject_id")));

                    q.setLessonId(Integer.parseInt(joarrObject
                            .getString("lesson_id")));
                    q.setTitle(joarrObject.getString("title"));
                    q.setResult1(joarrObject.getString("result1"));
                    q.setResult2(joarrObject.getString("result2"));
                    q.setResult3(joarrObject.getString("result3"));
                    q.setResult4(joarrObject.getString("result4"));
                    q.setResult(joarrObject.getString("result"));
                    q.setExamId(joarrObject.getString("exam_id").equals("null") ? 0
                            : Integer.parseInt(joarrObject.getString("exam_id")));
                    q.setIsImage(joarrObject.getString("is_image").equals(
                            "null") ? 0 : Integer.parseInt(joarrObject
                            .getString("is_image")));
                    q.setCreated_date(joarrObject.getString("created_date")
                            .equals("null") ? 0 : Integer.parseInt(joarrObject
                            .getString("created_date")));
                    q.setUpdated_date(joarrObject.getString("updated_date")
                            .equals("null") ? 0 : Integer.parseInt(joarrObject
                            .getString("updated_date")));

                    questions.add(q);
                }

                context.finish();

                Intent intent = new Intent(context, OnlineExamActivity.class);
                context.startActivity(intent);
            }
            else
            {
                Utils.ShowToast(context, R.string.error);
            }

        }
        catch (JSONException e)
        {
            e.printStackTrace();
            Utils.ShowToast(context, R.string.onlinequestion_dont_have);
        }

    }
}
