package vn.hlgroup.app.controller;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import vn.hlgroup.app.IDefine;
import vn.hlgroup.app.R;
import vn.hlgroup.app.activity.MainExamOptionActivity;
import vn.hlgroup.app.adapter.CustomSpinnerAdapter;
import vn.hlgroup.app.adapter.MapItem;
import vn.hlgroup.app.model.Subject;
import vn.hlgroup.app.singleton.AppManager;
import vn.hlgroup.app.utils.Utils;
import android.app.Activity;
import android.os.AsyncTask;

public class GetSubjectDependTypeMainExam extends
        AsyncTask<Integer, Integer, String>
{
    private Activity context;

    public GetSubjectDependTypeMainExam(Activity context)
    {
        this.context = context;
    }

    @Override
    protected String doInBackground(Integer... params)
    {
        // TODO Auto-generated method stub

        String urlsubject = null;

        {

            urlsubject = IDefine.HEADER_LINK_SERVICE
                    + "get_subjects_by_type_id/" + params[0];

        }

        String jsonresulsubject = Utils.GET(urlsubject);

        String finalResult = jsonresulsubject;

        return finalResult;

    }

    @Override
    protected void onPreExecute()
    {

    }

    @Override
    protected void onPostExecute(String result)
    {

        ArrayList<Subject> subjects = AppManager.getInstance().getSubjects();
        subjects.clear();

        try
        {
            JSONObject jofinal = new JSONObject(result);
            int err1 = jofinal.getInt("error");

            if (err1 == 0)
            {

                // save subject here
                JSONArray jomsg1 = jofinal.getJSONArray("message");
                for (int i = 0; i < jomsg1.length(); i++)
                {
                    JSONObject josubject = jomsg1.getJSONObject(i);
                    Subject s = new Subject();
                    s.setId(Integer.parseInt(josubject.getString("id")));
                    s.setType_Id(Integer.parseInt(josubject
                            .getString("type_id")));
                    s.setName(josubject.getString("name"));

                    subjects.add(s);
                }

                // for subject spinner
                List<MapItem> listsubject = new ArrayList<MapItem>();

                int len1 = subjects.size();

                for (int i = 0; i < len1; i++)
                {
                    Subject s = subjects.get(i);
                    MapItem mi = new MapItem(s.getId(), s.getName());
                    listsubject.add(mi);
                }

                MainExamOptionActivity c = (MainExamOptionActivity) context;
                c.SubjectdataAdapter = new CustomSpinnerAdapter(context,
                        R.drawable.custom_spinner_item, listsubject);
                c.SubjectdataAdapter
                        .setDropDownViewResource(R.drawable.custom_spinner_dropdown_item);
                c.spinSubject.setAdapter(c.SubjectdataAdapter);

                /*
                 * c.spinSubject.setOnItemSelectedListener(new
                 * OnItemSelectedListener() {
                 * 
                 * @Override public void onItemSelected(AdapterView<?> arg0,
                 * View arg1, int arg2, long arg3) { // TODO Auto-generated
                 * method stub MapItem item =
                 * (MapItem)TrainOptionActivity.this.SubjectdataAdapter
                 * .getItem(arg2);
                 * 
                 * //Toast.makeText(TrainOptionActivity.this,
                 * String.valueOf(item.getId()), Toast.LENGTH_SHORT).show(); }
                 * 
                 * @Override public void onNothingSelected(AdapterView<?> arg0)
                 * { // TODO Auto-generated method stub
                 * 
                 * }
                 * 
                 * });
                 */
            }

        }
        catch (JSONException e)
        {
            Utils.ShowToast(context, R.string.subjectdependtype_dont_have);
            e.printStackTrace();
        }
    }

}
