package vn.hlgroup.app.controller;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import vn.hlgroup.app.IDefine;
import vn.hlgroup.app.R;
import vn.hlgroup.app.activity.MainActivity;
import vn.hlgroup.app.activity.MainMenuActivity;
import vn.hlgroup.app.model.Notification;
import vn.hlgroup.app.model.User;
import vn.hlgroup.app.singleton.AppManager;
import vn.hlgroup.app.utils.Utils;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

public class Login extends AsyncTask<String, Integer, String>
{
    private Activity context;

    public Login(Activity context)
    {
        this.context = context;
    }

    // private boolean IsLoginSuccess(String username, String password)
    // {
    // String url = "http://jsonplaceholder.typicode.com/users";

    // url+="?username="+username+"&password="+password;

    // String resultjson = new RestApi(null).GET(url);

    // return true;
    // }

    // private boolean validate(String username, String password, String level,
    // String email, String phone, String errorMgs)
    // {
    // return true;
    // }

    @Override
    protected String doInBackground(String... params)
    {
        // TODO Auto-generated method stub
        // check login from server

        AppManager app = AppManager.getInstance();
        ArrayList<Notification> notis = app.getNotifications();
        notis.clear();

        // get new mail

        String newmsgurl = IDefine.HEADER_LINK_SERVICE + "notification";
        String jsonnewmsgstr = Utils.GET(newmsgurl);
        try
        {
            JSONObject jsonnewmsg = new JSONObject(jsonnewmsgstr);
            if (jsonnewmsg.getInt("error") == 0)
            {
                JSONArray jomsg = jsonnewmsg.getJSONArray("message");

                for (int i = 0; i < jomsg.length(); i++)
                {
                    Notification n = new Notification();

                    JSONObject noti = jomsg.getJSONObject(i);
                    n.setId(Integer.parseInt(noti.getString("id")));
                    n.setTitle(noti.getString("title"));
                    n.setContent(noti.getString("content"));
                    n.setCreatedDate(Long.parseLong(noti
                            .getString("created_date")));
                    n.setUpdatedDate(Long.parseLong(noti
                            .getString("updated_date")));
                    n.setActive(noti.getString("is_active").equals("null") ? 0
                            : Integer.parseInt(noti.getString("is_active")));

                    notis.add(n);
                }

                /*
                 * if(jomsg.length()>0) {
                 * 
                 * StringBuilder str = new StringBuilder();
                 * 
                 * for(int i=0;i<jomsg.length();i++) {
                 * str.append(jomsg.getJSONObject
                 * (i).getString("content")+"\n\n"); }
                 * 
                 * AppManager.getInstance().setNewMailContent(str.toString());
                 * AppManager.getInstance().setHaveNewMail(true); } else {
                 * AppManager.getInstance().setNewMailContent(null);
                 * AppManager.getInstance().setHaveNewMail(false); }
                 */

            }
            else
            {
                // AppManager.getInstance().setNewMailContent(null);
                // AppManager.getInstance().setHaveNewMail(false);
            }
        }
        catch (JSONException e)
        {
            e.printStackTrace();
            // AppManager.getInstance().setNewMailContent(null);
            // AppManager.getInstance().setHaveNewMail(false);
            Utils.ShowToast(context, R.string.error);
        }

        String user = params[0];
        String pass = params[1];

        String url = null;

        {
            url = IDefine.HEADER_LINK_SERVICE + "login/";
        }
        // String url = "http://192.168.1.2/appadmin/index.php/service/login/";
        // String url = "http://192.168.1.19/service/login/";
        url += user + "/" + pass;
        // url += "hung" + "/" + "hung";

        String jsonresult = Utils.GET(url);

        return jsonresult;

    }

    @Override
    protected void onPreExecute()
    {

        MainActivity mai = (MainActivity) context;

        ProgressBar probar = (ProgressBar) mai.findViewById(R.id.probar);
        probar.setVisibility(View.VISIBLE);

        EditText userText = (EditText) mai.findViewById(R.id.txtU);
        userText.setEnabled(false);

        EditText passText = (EditText) mai.findViewById(R.id.txtP);
        passText.setEnabled(false);

        Button registerButton = (Button) mai.findViewById(R.id.btnRes);
        Button loginButton = (Button) mai.findViewById(R.id.btnLog);

        registerButton.setEnabled(false);
        loginButton.setEnabled(false);

    }

    @Override
    protected void onPostExecute(String result)
    {

        // close loading bar and show error mgs or go next screen

        MainActivity mai = (MainActivity) context;

        ProgressBar probar = (ProgressBar) mai.findViewById(R.id.probar);
        probar.setVisibility(View.INVISIBLE);

        EditText userText = (EditText) mai.findViewById(R.id.txtU);
        userText.setEnabled(true);

        EditText passText = (EditText) mai.findViewById(R.id.txtP);
        passText.setEnabled(true);

        Button registerButton = (Button) mai.findViewById(R.id.btnRes);
        Button loginButton = (Button) mai.findViewById(R.id.btnLog);

        registerButton.setEnabled(true);
        loginButton.setEnabled(true);

        // compare result and switch screen or show login fail msg

        try
        {

            JSONObject jo = new JSONObject(result);

            int resultCode = jo.getInt("error");

            if (resultCode == 1)
            {
                Utils.ShowToast(mai, R.string.login_fail);
                if (IDefine.isDebugLogin)
                {
                    context.finish();
                    Intent intent = new Intent(context, MainMenuActivity.class);
                    context.startActivity(intent);
                }
            }
            else
                if (resultCode == 2)
                {
                    Utils.ShowToast(mai, R.string.dangnhaproi);
                }
                else
                {
                    JSONObject jomsg = new JSONObject(jo.getString("message"));

                    int id = Integer.parseInt(jomsg.getString("id"));

                    String username = jomsg.getString("username");
                    String password = jomsg.getString("password");
                    Log.d("abc", id + " " + username + " " + password);
                    int levelId = Integer.parseInt(jomsg.getString("level_id"));
                    String email = jomsg.getString("email");
                    String phone = jomsg.getString("phone");
                    int created_date = Integer.parseInt(jomsg
                            .getString("created_date"));
                    int updated_date = Integer.parseInt(jomsg
                            .getString("updated_date"));

                    User user = new User();
                    user.setId(id);
                    user.setUsername(username);
                    user.setPassword(password);
                    user.setLevel_id(levelId);
                    user.setEmail(email);
                    user.setPhone(phone);
                    user.setCreated_date(created_date);
                    user.setUpdated_date(updated_date);
                    user.setPasswordDontHash(Utils.ReadData(context,
                            "passwordtmp"));

                    AppManager.getInstance().setUser(user);

                    // if (Utils.IsSavedPassword(context))
                    {
                        Utils.WriteData(context, "username", user.getUsername());
                        Utils.WriteData(context, "password",
                                Utils.ReadData(context, "passwordtmp"));
                    }

                    context.finish();
                    Intent intent = new Intent(context, MainMenuActivity.class);
                    context.startActivity(intent);
                    Utils.WriteData(context, "newmail", "true");
                }
        }
        catch (JSONException e)
        {

            Utils.ShowToast(mai, R.string.login_fail);
            e.printStackTrace();

        }

    }

    // public boolean IsRegisterSuccess(String username, String password, String
    // level, String email, String phone, String errorMgs)
    // {
    // if (validate(username, password, level, email, phone, errorMgs))
    // {
    // // do register
    // return true;
    // }
    // else
    // {
    // return false;
    // }
    // }
}
