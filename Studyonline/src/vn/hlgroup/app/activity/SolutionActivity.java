package vn.hlgroup.app.activity;

import vn.hlgroup.app.IDefine;
import vn.hlgroup.app.R;
import vn.hlgroup.app.controller.Logout;
import vn.hlgroup.app.singleton.AppManager;
import vn.hlgroup.app.utils.Utils;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.widget.TextView;

public class SolutionActivity extends CoreActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_solution);

        Intent i = getIntent();

        int item = i.getIntExtra("item", -1);

        TextView tv = (TextView) this.findViewById(R.id.txvItemSolution);
        tv.setText(item + "");
    }

    @Override
    public void onResume()
    {
        super.onResume();

        // Resume the AdView.
        // mAdView.resume();

        if (timeHideNotify != 0)
        {
            if (timeShowNotify - timeHideNotify > IDefine.SESSION_TIME_OUT)
            {
                if (Utils.IsNetworkAvailable(this))
                {
                    new Logout(this).execute(AppManager.getInstance().getUser().getUsername());
                }
                // else
                // {

                // this.finish();

                // Intent intent = new Intent(this, MainActivity.class);
                // startActivityBack(intent);
                // }
            }
        }
    }

    @Override
    public void onPause()
    {
        // Pause the AdView.
        // mAdView.pause();

        super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.solution, menu);
        return true;
    }

    @Override
    public void onBackPressed()
    {

        currentClassName = "SolutionActivity";
        showDialog(DIALOG_EXIT);
    }

    @Override
    public void startActivity(Intent intent)
    {
        super.startActivity(intent);
        this.overridePendingTransition(R.anim.anim_activity_go_to_left, R.anim.anim_activity_go_to_right);
    }

    @Override
    public void startActivityBack(Intent intent)
    {
        super.startActivity(intent);
        this.overridePendingTransition(R.anim.anim_activity_go_to_left_back, R.anim.anim_activity_go_to_right_back);
    }

}
