package vn.hlgroup.app.activity;

import vn.hlgroup.app.IDefine;
import vn.hlgroup.app.R;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;

public class TutChooseSolutionActivity extends CoreActivity
{

    private Button _No, _Yes;

    public static final int DIALOG_EXIT = 1;

    private int backeyNumPress = 0;

    private long timeResetbackey = 0;
    private AdView mAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tut_choose_solution);

        // ads
        LinearLayout layout = (LinearLayout) this
                .findViewById(R.id.adViewLayout);

        // Create a banner ad. The ad size and ad unit ID must be set before
        // calling loadAd.
        mAdView = new AdView(this);
        mAdView.setAdSize(AdSize.SMART_BANNER);
        mAdView.setAdUnitId(IDefine.ADMOB_ID);

        // Create an ad request.
        AdRequest.Builder adRequestBuilder = new AdRequest.Builder();

        // Optionally populate the ad request builder.
        // adRequestBuilder.addTestDevice(AdRequest.DEVICE_ID_EMULATOR);

        // Add the AdView to the view hierarchy.
        layout.addView(mAdView);

        // Start loading the ad.
        mAdView.loadAd(adRequestBuilder.build());

        if (!IDefine.HAVE_ADS)
        {
            RelativeLayout v = (RelativeLayout) this.findViewById(R.id.wrapper);

            final int INDEX_FOOTER_VIEW = 2;
            v.removeViewAt(INDEX_FOOTER_VIEW);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.tut_choose_solution, menu);
        return true;
    }

    @Override
    public void onBackPressed()
    {

        /*
         * if (System.currentTimeMillis() - timeResetbackey > 3000) {
         * backeyNumPress = 0; } if (backeyNumPress == 0) { String text =
         * this.getResources().getString(R.string.backey);
         * Toast.makeText(TutChooseSolutionActivity.this, text,
         * Toast.LENGTH_SHORT).show(); backeyNumPress = 1; timeResetbackey =
         * System.currentTimeMillis(); } else if (backeyNumPress == 1) {
         * backeyNumPress = 0; this.finish();
         * 
         * Intent intent = new Intent(this, TutorialsActivity.class);
         * startActivityBack(intent);
         * 
         * }
         */

        currentClassName = "TutChooseSolutionActivity";
        showDialog(DIALOG_EXIT);
    }

    @Override
    public void startActivity(Intent intent)
    {
        super.startActivity(intent);
        this.overridePendingTransition(R.anim.anim_activity_go_to_left,
                R.anim.anim_activity_go_to_right);
    }

    @Override
    public void startActivityBack(Intent intent)
    {
        super.startActivity(intent);
        this.overridePendingTransition(R.anim.anim_activity_go_to_left_back,
                R.anim.anim_activity_go_to_right_back);
    }

    /*
     * @Override protected Dialog onCreateDialog(int id) { // TODO
     * Auto-generated method stub Dialog dialog = null;
     * 
     * dialog = new Dialog(TutChooseSolutionActivity.this);
     * dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
     * dialog.setContentView(R.layout.dialog_exit);
     * 
     * _No = (Button) dialog.findViewById(R.id.btnDialogNo); _Yes = (Button)
     * dialog.findViewById(R.id.btnDialogYes);
     * 
     * if (_No != null) { _No.setOnClickListener(new OnClickListener() {
     * 
     * @SuppressWarnings("deprecation")
     * 
     * @Override public void onClick(View v) { // TODO Auto-generated method
     * stub dismissDialog(DIALOG_EXIT); } }); }
     * 
     * if (_Yes != null) { _Yes.setOnClickListener(new OnClickListener() {
     * 
     * @Override public void onClick(View v) { // TODO Auto-generated method
     * stub // if (TutChooseSolutionActivity.this.isEnablePressBackey) {
     * TutChooseSolutionActivity.this.finish();
     * 
     * Intent intent = new Intent( TutChooseSolutionActivity.this,
     * TutorialsActivity.class); startActivityBack(intent); } } });
     * 
     * }
     * 
     * return dialog; }
     */

}
