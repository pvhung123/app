package vn.hlgroup.app.activity;

import vn.hlgroup.app.IDefine;
import vn.hlgroup.app.R;
import vn.hlgroup.app.controller.GetLevelId;
import vn.hlgroup.app.controller.GetOnlineExam;
import vn.hlgroup.app.controller.GetTypeDefault;
import vn.hlgroup.app.controller.GetTypeDefaultMainOption;
import vn.hlgroup.app.controller.Logout;
import vn.hlgroup.app.singleton.AppManager;
import vn.hlgroup.app.utils.Utils;
import android.content.Intent;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;

public class MainMenuActivity extends CoreActivity
{
    private Button btnInfo;

    private Button btnTrain;

    private Button btnMainExam;

    private Button btnOnlineExam;
    private Button btnXepHang;

    private Button btnNewmail;

    private Button btnLogout;

    // private InterstitialAd interstitial;
    AdView mAdView;

    private Button _No, _Yes;

    public static final int DIALOG_EXIT = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);

        // ads
        LinearLayout layout = (LinearLayout) this
                .findViewById(R.id.adViewLayout);

        // Create a banner ad. The ad size and ad unit ID must be set before
        // calling loadAd.
        mAdView = new AdView(this);
        mAdView.setAdSize(AdSize.SMART_BANNER);
        mAdView.setAdUnitId("ca-app-pub-8820372249061974/8285606845");

        // Create an ad request.
        AdRequest.Builder adRequestBuilder = new AdRequest.Builder();

        // Optionally populate the ad request builder.
        // adRequestBuilder.addTestDevice(AdRequest.DEVICE_ID_EMULATOR);

        // Add the AdView to the view hierarchy.
        layout.addView(mAdView);

        // Start loading the ad.
        mAdView.loadAd(adRequestBuilder.build());

        checkNewMail();

        btnInfo = (Button) this.findViewById(R.id.btnInfomain);
        btnInfo.setOnClickListener(new OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                // TODO Auto-generated method stub
                // MainMenuActivity.this.finish();

                // Intent intent = new Intent(MainMenuActivity.this,
                // InfoActivity.class);
                // startActivity(intent);
                if (!Utils.IsNetworkAvailable(MainMenuActivity.this))
                {
                    Utils.ShowToast(MainMenuActivity.this, R.string.networknot);
                    return;
                }

                new GetLevelId(MainMenuActivity.this).execute(1);// 0 switch to
                                                                 // Infor Screen
            }
        });

        btnTrain = (Button) this.findViewById(R.id.btnOnthiMain);
        btnTrain.setOnClickListener(new OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                // TODO Auto-generated method stub
                // MainMenuActivity.this.finish();

                // Intent intent = new Intent(MainMenuActivity.this,
                // TrainOptionActivity.class);
                // startActivity(intent);
                if (!Utils.IsNetworkAvailable(MainMenuActivity.this))
                {
                    Utils.ShowToast(MainMenuActivity.this, R.string.networknot);
                    return;
                }
                new GetTypeDefault(MainMenuActivity.this).execute();
            }
        });

        btnMainExam = (Button) this.findViewById(R.id.btnThichungMain);
        btnMainExam.setOnClickListener(new OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                // TODO Auto-generated method stub
                if (!Utils.IsNetworkAvailable(MainMenuActivity.this))
                {
                    Utils.ShowToast(MainMenuActivity.this, R.string.networknot);
                    return;
                }
                new GetTypeDefaultMainOption(MainMenuActivity.this).execute();
            }
        });

        btnOnlineExam = (Button) this.findViewById(R.id.btnOnlineMain);
        btnOnlineExam.setOnClickListener(new OnClickListener()
        {

            @Override
            public void onClick(View v)
            {

                if (!Utils.IsNetworkAvailable(MainMenuActivity.this))
                {
                    Utils.ShowToast(MainMenuActivity.this, R.string.networknot);
                    return;
                }
                new GetOnlineExam(MainMenuActivity.this).execute();
            }
        });

        btnXepHang = (Button) this.findViewById(R.id.btnXepHang);
        btnXepHang.setOnClickListener(new OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                // TODO Auto-generated method stub
                MainMenuActivity.this.finish();

                Intent intent = new Intent(MainMenuActivity.this,
                        RankActivity.class);
                startActivity(intent);

                // new GetHighScore(MainMenuActivity.this).execute();
            }
        });

        btnLogout = (Button) this.findViewById(R.id.btnLogout);
        btnLogout.setOnClickListener(new OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                showDialog(DIALOG_EXIT);
            }
        });

        Display display = getWindowManager().getDefaultDisplay();

        Button btnOnthiMain = (Button) this.findViewById(R.id.btnOnthiMain);
        btnOnthiMain.setHeight(display.getWidth() / 2);

        Button btnOnlineMain = (Button) this.findViewById(R.id.btnOnlineMain);
        btnOnlineMain.setHeight(display.getWidth() / 2);

        Button btntut = (Button) this.findViewById(R.id.btnXepHang);
        btntut.setHeight(display.getWidth() / 4);

        if (!IDefine.HAVE_ADS)
        {
            RelativeLayout v = (RelativeLayout) this.findViewById(R.id.wrapper);

            final int INDEX_FOOTER_VIEW = 2;
            v.removeViewAt(INDEX_FOOTER_VIEW);
        }

    }

    @Override
    public void onBackPressed()
    {
        currentClassName = "MainMenuActivity";
        showDialog(DIALOG_EXIT);
    }

    @Override
    public void startActivity(Intent intent)
    {
        super.startActivity(intent);
        this.overridePendingTransition(R.anim.anim_activity_go_to_left,
                R.anim.anim_activity_go_to_right);
    }

    @Override
    public void startActivityBack(Intent intent)
    {
        super.startActivity(intent);
        this.overridePendingTransition(R.anim.anim_activity_go_to_left_back,
                R.anim.anim_activity_go_to_right_back);
    }

    @Override
    public void onResume()
    {
        super.onResume();

        // Resume the AdView.
        mAdView.resume();

        if (timeHideNotify != 0)
        {
            if (timeShowNotify - timeHideNotify > IDefine.SESSION_TIME_OUT)
            {
                if (Utils.IsNetworkAvailable(this))
                {
                    new Logout(this).execute(AppManager.getInstance().getUser()
                            .getUsername());
                }
                // else
                // {

                // this.finish();

                // Intent intent = new Intent(this, MainActivity.class);
                // startActivityBack(intent);
                // }
            }
        }
    }

    @Override
    public void onPause()
    {
        // Pause the AdView.
        mAdView.pause();

        super.onPause();
    }

    @Override
    public void onDestroy()
    {
        // Destroy the AdView.
        mAdView.destroy();

        super.onDestroy();
    }

    /*
     * @Override protected Dialog onCreateDialog(int id) { // TODO
     * Auto-generated method stub Dialog dialog = null;
     * 
     * dialog = new Dialog(MainMenuActivity.this);
     * dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
     * dialog.setContentView(R.layout.dialog_exit);
     * 
     * TextView titleExit = (TextView) dialog.findViewById(R.id.dialog_title);
     * titleExit.setText(R.string.title_back_dialog);
     * 
     * _No = (Button) dialog.findViewById(R.id.btnDialogNo); _Yes = (Button)
     * dialog.findViewById(R.id.btnDialogYes);
     * 
     * if (_No != null) { _No.setOnClickListener(new OnClickListener() {
     * 
     * @SuppressWarnings("deprecation")
     * 
     * @Override public void onClick(View v) { // TODO Auto-generated method
     * stub dismissDialog(DIALOG_EXIT); } }); }
     * 
     * if (_Yes != null) { _Yes.setOnClickListener(new OnClickListener() {
     * 
     * @Override public void onClick(View v) { // TODO Auto-generated method
     * stub // if (MainMenuActivity.this.isEnablePressBackey) { if
     * (Utils.IsNetworkAvailable(MainMenuActivity.this)) { new
     * Logout(MainMenuActivity.this) .execute(AppManager.getInstance().getUser()
     * .getUsername()); // MainMenuActivity.this.finish();
     * 
     * // Intent intent = new Intent(MainMenuActivity.this, //
     * MainActivity.class); // startActivityBack(intent); } else {
     * Utils.ShowToast(MainMenuActivity.this, R.string.networknot); }
     * 
     * } } });
     * 
     * }
     * 
     * return dialog; }
     */

    private void checkNewMail()
    {
        btnNewmail = (Button) this.findViewById(R.id.btnmail);

        boolean newmail = AppManager.getInstance().HaveNewMail(this);

        if (newmail)
        {
            btnNewmail.setBackgroundResource(R.drawable.mh);
        }

        btnNewmail.setOnClickListener(new OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                // TODO Auto-generated method stub
                // MainMenuActivity.this.finish();

                // Intent intent = new Intent(MainMenuActivity.this,
                // InfoActivity.class);
                // startActivity(intent);

                // if(AppManager.getInstance().HaveNewMail())
                // {
                // btnNewmail.setBackgroundResource(R.drawable.m);
                // AppManager.getInstance().setHaveNewMail(false);

                MainMenuActivity.this.finish();

                Intent intent = new Intent(MainMenuActivity.this,
                        NewMsgActivity.class);
                startActivity(intent);
                // }
                // else
                // {
                // Utils.ShowToast(MainMenuActivity.this,
                // "Khong co thong bao moi");
                // }
            }
        });
    }

}
