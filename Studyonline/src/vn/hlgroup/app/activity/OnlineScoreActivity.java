package vn.hlgroup.app.activity;

import vn.hlgroup.app.IDefine;
import vn.hlgroup.app.R;
import vn.hlgroup.app.controller.Logout;
import vn.hlgroup.app.singleton.AppManager;
import vn.hlgroup.app.utils.Utils;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;

public class OnlineScoreActivity extends CoreActivity
{

    private int backeyNumPress = 0;

    private long timeResetbackey = 0;

    // private LinearLayout layoutblank;
    private LinearLayout linearLayout1;
    // private LinearLayout linearLayout0;
    // private LinearLayout layoutfooter;
    private LinearLayout adViewLayout;

    private AdView mAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        View mainView = getLayoutInflater().inflate(
                R.layout.activity_online_score, null);

        AppManager app = AppManager.getInstance();

        // ads
        adViewLayout = (LinearLayout) mainView.findViewById(R.id.adViewLayout);

        // Create a banner ad. The ad size and ad unit ID must be set before
        // calling loadAd.
        mAdView = new AdView(this);
        mAdView.setAdSize(AdSize.SMART_BANNER);
        mAdView.setAdUnitId(IDefine.ADMOB_ID);

        // Create an ad request.
        AdRequest.Builder adRequestBuilder = new AdRequest.Builder();

        // Optionally populate the ad request builder.
        // adRequestBuilder.addTestDevice(AdRequest.DEVICE_ID_EMULATOR);

        // Add the AdView to the view hierarchy.
        adViewLayout.addView(mAdView);

        // Start loading the ad.
        mAdView.loadAd(adRequestBuilder.build());

        // TextView score =
        // (TextView)mainView.findViewById(R.id.txvScoremainexamresult);
        // score.setText(app.getRightAnswer()+"/"+app.getQuestions().size());

        // TextView score =
        // (TextView)mainView.findViewById(R.id.txvScoremainexamresult);
        // String resultScore = getResources().getString(R.string.resultScore);
        // String scoreStr = getResources().getString(R.string.main_exam_score);
        // score.setText(resultScore+": "+app.getRightAnswer()+"/"+app.getQuestions().size()+" "+scoreStr+": "+(app.getRightAnswer()*100f/app.getQuestions().size()));

        /*
         * TextView score =
         * (TextView)mainView.findViewById(R.id.txvScoremainexamresult); String
         * resultScore = getResources().getString(R.string.resultScore); String
         * scoreStr = getResources().getString(R.string.main_exam_score); String
         * ladderStr = getResources().getString(R.string.scoreladder);
         * score.setText
         * (resultScore+": "+app.getRightAnswer()+"/"+app.getQuestions
         * ().size());
         * 
         * TextView scoreladder =
         * (TextView)mainView.findViewById(R.id.txvScoreLadder);
         * scoreladder.setText
         * (scoreStr+": "+(app.getRightAnswer()*100f/app.getQuestions
         * ().size()));
         * 
         * TextView txvScoreLadderStr =
         * (TextView)mainView.findViewById(R.id.txvScoreLadderStr);
         * txvScoreLadderStr.setText(ladderStr);
         */

        TextView score = (TextView) mainView.findViewById(R.id.txvScore);
        String resultScore = getResources().getString(R.string.resultScore);
        String scoreStr = getResources().getString(R.string.main_exam_score);
        String ladderStr = getResources().getString(R.string.scoreladder);
        score.setText(resultScore
                + ": "
                + app.getRightAnswer()
                + "/"
                + app.getQuestions().size()
                + ". "
                + scoreStr
                + ": "
                + Utils.roundTwoDigit(String.valueOf(app.getRightAnswer()
                        * 100f / app.getQuestions().size())) + ". " + ladderStr);

        // linearLayout0 = (LinearLayout)
        // mainView.findViewById(R.id.linearLayout0);
        linearLayout1 = (LinearLayout) mainView
                .findViewById(R.id.linearLayout1);
        // layoutblank = (LinearLayout) mainView.findViewById(R.id.layoutblank);
        // layoutfooter = (LinearLayout)
        // mainView.findViewById(R.id.layoutfooter);

        setContentView(mainView);

        Button btnLogout = (Button) this.findViewById(R.id.btnLogout);
        btnLogout.setOnClickListener(new OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                showDialog(DIALOG_EXIT);
            }
        });

        if (!IDefine.HAVE_ADS)
        {
            RelativeLayout v = (RelativeLayout) this.findViewById(R.id.wrapper);

            final int INDEX_FOOTER_VIEW = 2;
            v.removeViewAt(INDEX_FOOTER_VIEW);
        }
    }

    @Override
    public void onResume()
    {
        super.onResume();

        // Resume the AdView.
        mAdView.resume();

        if (timeHideNotify != 0)
        {
            if (timeShowNotify - timeHideNotify > IDefine.SESSION_TIME_OUT)
            {
                if (Utils.IsNetworkAvailable(this))
                {
                    new Logout(this).execute(AppManager.getInstance().getUser()
                            .getUsername());
                }
                // else
                // {

                // this.finish();

                // Intent intent = new Intent(this, MainActivity.class);
                // startActivityBack(intent);
                // }
            }
        }
    }

    @Override
    public void onPause()
    {
        // Pause the AdView.
        mAdView.pause();

        super.onPause();
    }

    @Override
    public void onBackPressed()
    {
        /*
         * if (System.currentTimeMillis() - timeResetbackey > 3000) {
         * backeyNumPress = 0; } if (backeyNumPress == 0) { String text =
         * this.getResources().getString(R.string.backey);
         * Toast.makeText(OnlineScoreActivity.this, text, Toast.LENGTH_SHORT)
         * .show(); backeyNumPress = 1; timeResetbackey =
         * System.currentTimeMillis(); } else if (backeyNumPress == 1) {
         * backeyNumPress = 0; this.finish();
         * 
         * Intent intent = new Intent(this, MainMenuActivity.class);
         * startActivityBack(intent);
         * 
         * }
         */
        currentClassName = "OnlineScoreActivity";
        showDialog(DIALOG_EXIT);
    }

    @Override
    public void startActivity(Intent intent)
    {
        super.startActivity(intent);
        this.overridePendingTransition(R.anim.anim_activity_go_to_left,
                R.anim.anim_activity_go_to_right);
    }

    @Override
    public void startActivityBack(Intent intent)
    {
        super.startActivity(intent);
        this.overridePendingTransition(R.anim.anim_activity_go_to_left_back,
                R.anim.anim_activity_go_to_right_back);
    }
}
