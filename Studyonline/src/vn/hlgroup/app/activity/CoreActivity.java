package vn.hlgroup.app.activity;

import vn.hlgroup.app.R;
import vn.hlgroup.app.controller.Logout;
import vn.hlgroup.app.singleton.AppManager;
import vn.hlgroup.app.utils.Utils;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

public abstract class CoreActivity extends Activity
{
    public static final int DIALOG_EXIT = 1;

    protected long timeHideNotify;

    protected long timeShowNotify;

    protected String currentClassName;

    @Override
    protected void onPause()
    {
        super.onPause();
        timeHideNotify = System.currentTimeMillis();
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        timeShowNotify = System.currentTimeMillis();
    }

    public abstract void startActivityBack(Intent intent);

    @Override
    protected Dialog onCreateDialog(int id)
    {
        // TODO Auto-generated method stub
        Dialog dialog = null;

        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_exit);

        if (currentClassName.equals("MainActivity"))
        {

        }
        else if (currentClassName.equals("MainMenuActivity"))
        {
            TextView titleExit = (TextView) dialog.findViewById(R.id.dialog_title);
            titleExit.setText(R.string.title_back_dialog);
        }
        else
        {
            TextView titleExit = (TextView) dialog.findViewById(R.id.dialog_title);
            titleExit.setText(R.string.title_go_back_dialog);
        }

        Button _No = (Button) dialog.findViewById(R.id.btnDialogNo);
        Button _Yes = (Button) dialog.findViewById(R.id.btnDialogYes);

        if (_No != null)
        {
            _No.setOnClickListener(new OnClickListener()
            {

                @SuppressWarnings("deprecation")
                @Override
                public void onClick(View v)
                {
                    // TODO Auto-generated method stub
                    dismissDialog(DIALOG_EXIT);
                }
            });
        }

        if (_Yes != null)
        {
            _Yes.setOnClickListener(new OnClickListener()
            {

                @Override
                public void onClick(View v)
                {
                    // TODO Auto-generated method stub
                    // if (MainMenuActivity.this.isEnablePressBackey)
                    {
                        if (Utils.IsNetworkAvailable(CoreActivity.this))
                        {
                            if (currentClassName.equals("MainActivity"))
                            {

                            }
                            else if (currentClassName.equals("MainMenuActivity"))
                            {
                                new Logout(CoreActivity.this).execute(AppManager.getInstance().getUser().getUsername());

                            }
                            else if (currentClassName.equals("AvgScoreActivity"))
                            {
                                CoreActivity.this.finish();

                                Intent intent = new Intent(CoreActivity.this, RankActivity.class);
                                startActivityBack(intent);
                            }
                            else if (currentClassName.equals("AvgScoreOnlineActivity"))
                            {
                                CoreActivity.this.finish();

                                Intent intent = new Intent(CoreActivity.this, RankActivity.class);
                                startActivityBack(intent);
                            }
                            else if (currentClassName.equals("DetailNotificationActivity"))
                            {
                                CoreActivity.this.finish();

                                Intent intent = new Intent(CoreActivity.this, NewMsgActivity.class);
                                startActivityBack(intent);
                            }
                            else if (currentClassName.equals("InfoActivity"))
                            {
                                CoreActivity.this.finish();

                                Intent intent = new Intent(CoreActivity.this, MainMenuActivity.class);
                                startActivityBack(intent);
                            }
                            else if (currentClassName.equals("ListResultOnlineActivity"))
                            {
                                CoreActivity.this.finish();

                                Intent intent = new Intent(CoreActivity.this, OnlineExamMenuActivity.class);
                                startActivityBack(intent);
                            }
                            else if (currentClassName.equals("MainExamActivity"))
                            {
                                CoreActivity.this.finish();

                                Intent intent = new Intent(CoreActivity.this, MainMenuActivity.class);
                                startActivityBack(intent);
                            }
                            else if (currentClassName.equals("MainExamOptionActivity"))
                            {
                                CoreActivity.this.finish();

                                Intent intent = new Intent(CoreActivity.this, MainMenuActivity.class);
                                startActivityBack(intent);
                            }
                            else if (currentClassName.equals("MainExamResultActivity"))
                            {
                                CoreActivity.this.finish();

                                Intent intent = new Intent(CoreActivity.this, MainMenuActivity.class);
                                startActivityBack(intent);
                            }
                            else if (currentClassName.equals("NewMsgActivity"))
                            {
                                CoreActivity.this.finish();

                                Intent intent = new Intent(CoreActivity.this, MainMenuActivity.class);
                                startActivityBack(intent);
                            }
                            else if (currentClassName.equals("OnlineExamActivity"))
                            {
                                CoreActivity.this.finish();

                                Intent intent = new Intent(CoreActivity.this, MainMenuActivity.class);
                                startActivityBack(intent);
                            }
                            else if (currentClassName.equals("OnlineExamMenuActivity"))
                            {
                                CoreActivity.this.finish();

                                Intent intent = new Intent(CoreActivity.this, MainMenuActivity.class);
                                startActivityBack(intent);
                            }
                            else if (currentClassName.equals("OnlineExamResultActivity"))
                            {
                                CoreActivity.this.finish();

                                Intent intent = new Intent(CoreActivity.this, ListResultOnlineActivity.class);
                                startActivityBack(intent);
                            }
                            else if (currentClassName.equals("OnlineOptionActivity"))
                            {
                                CoreActivity.this.finish();

                                Intent intent = new Intent(CoreActivity.this, OnlineExamMenuActivity.class);
                                startActivityBack(intent);
                            }
                            else if (currentClassName.equals("OnlineScoreActivity"))
                            {
                                CoreActivity.this.finish();

                                Intent intent = new Intent(CoreActivity.this, MainMenuActivity.class);
                                startActivityBack(intent);
                            }
                            else if (currentClassName.equals("RankActivity"))
                            {
                                CoreActivity.this.finish();

                                Intent intent = new Intent(CoreActivity.this, MainMenuActivity.class);
                                startActivityBack(intent);
                            }
                            else if (currentClassName.equals("RegisterActivity"))
                            {
                                CoreActivity.this.finish();

                                Intent intent = new Intent(CoreActivity.this, MainActivity.class);
                                startActivityBack(intent);
                            }
                            else if (currentClassName.equals("ResultActivity"))
                            {
                                CoreActivity.this.finish();

                                Intent intent = new Intent(CoreActivity.this, MainMenuActivity.class);
                                startActivityBack(intent);
                            }
                            else if (currentClassName.equals("ReviewPreOnlineResultActivity"))
                            {
                                CoreActivity.this.finish();

                                Intent intent = new Intent(CoreActivity.this, ListResultOnlineActivity.class);
                                startActivityBack(intent);
                            }
                            else if (currentClassName.equals("SendOpinionActivity"))
                            {
                                CoreActivity.this.finish();

                                Intent intent = new Intent(CoreActivity.this, InfoActivity.class);
                                startActivityBack(intent);
                            }
                            else if (currentClassName.equals("SolutionActivity"))
                            {
                                CoreActivity.this.finish();

                                Intent intent = new Intent(CoreActivity.this, MainExamResultActivity.class);
                                startActivityBack(intent);
                            }
                            else if (currentClassName.equals("TrainningActivity"))
                            {
                                CoreActivity.this.finish();

                                Intent intent = new Intent(CoreActivity.this, MainMenuActivity.class);
                                startActivityBack(intent);
                            }
                            else if (currentClassName.equals("TrainOptionActivity"))
                            {
                                CoreActivity.this.finish();

                                Intent intent = new Intent(CoreActivity.this, MainMenuActivity.class);
                                startActivityBack(intent);
                            }
                            else if (currentClassName.equals("TutChooseLevelActivity"))
                            {
                                CoreActivity.this.finish();

                                Intent intent = new Intent(CoreActivity.this, TutorialsActivity.class);
                                startActivityBack(intent);
                            }
                            else if (currentClassName.equals("TutChooseSolutionActivity"))
                            {
                                CoreActivity.this.finish();

                                Intent intent = new Intent(CoreActivity.this, TutorialsActivity.class);
                                startActivityBack(intent);
                            }
                            else if (currentClassName.equals("TutorialsActivity"))
                            {
                                CoreActivity.this.finish();

                                Intent intent = new Intent(CoreActivity.this, MainActivity.class);
                                startActivityBack(intent);
                            }
                            else if (currentClassName.equals("TutRegisterActivity"))
                            {
                                CoreActivity.this.finish();

                                Intent intent = new Intent(CoreActivity.this, TutorialsActivity.class);
                                startActivityBack(intent);
                            }
                            else if (currentClassName.equals("UpdateInfoActivity"))
                            {
                                CoreActivity.this.finish();

                                Intent intent = new Intent(CoreActivity.this, InfoActivity.class);
                                startActivityBack(intent);
                            }
                            else
                            {
                                Utils.ShowToast(CoreActivity.this, R.string.error);
                            }
                        }
                        else
                        {
                            Utils.ShowToast(CoreActivity.this, R.string.networknot);
                        }

                    }
                }
            });

        }

        return dialog;
    }
}
