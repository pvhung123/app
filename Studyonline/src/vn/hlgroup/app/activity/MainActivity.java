package vn.hlgroup.app.activity;

import vn.hlgroup.app.IDefine;
import vn.hlgroup.app.R;
import vn.hlgroup.app.controller.GetLevelId;
import vn.hlgroup.app.controller.Login;
import vn.hlgroup.app.utils.Utils;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;

//import vn.hlgroup.app.controller.RestApi;

public class MainActivity extends Activity
{

    private EditText userText;
    private EditText passText;
    Button registerButton;
    Button loginButton;

    CheckBox savePass;

    private TextView help;

    public static final int DIALOG_EXIT = 1;
    private Button _No, _Yes;

    private AdView mAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // ads
        LinearLayout layout = (LinearLayout) this
                .findViewById(R.id.adViewLayout);

        // Create a banner ad. The ad size and ad unit ID must be set before
        // calling loadAd.
        mAdView = new AdView(this);
        mAdView.setAdSize(AdSize.SMART_BANNER);
        mAdView.setAdUnitId("ca-app-pub-8820372249061974/8285606845");

        // Create an ad request.
        AdRequest.Builder adRequestBuilder = new AdRequest.Builder();

        // Optionally populate the ad request builder.
        // adRequestBuilder.addTestDevice(AdRequest.DEVICE_ID_EMULATOR);

        // Add the AdView to the view hierarchy.
        layout.addView(mAdView);

        // Start loading the ad.
        mAdView.loadAd(adRequestBuilder.build());

        registerButton = (Button) this.findViewById(R.id.btnRes);
        loginButton = (Button) this.findViewById(R.id.btnLog);
        userText = (EditText) this.findViewById(R.id.txtU);
        passText = (EditText) this.findViewById(R.id.txtP);

        savePass = (CheckBox) this.findViewById(R.id.chkSavepass);

        savePass.setOnCheckedChangeListener(new OnCheckedChangeListener()
        {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                    boolean isChecked)
            {
                // TODO Auto-generated method stub
                Utils.WriteIsSavedPassword(MainActivity.this, isChecked);

            }
        });
        boolean issavePass = Utils.IsSavedPassword(this);
        savePass.setChecked(issavePass);
        Utils.WriteIsSavedPassword(this, issavePass);

        if (issavePass)
        {

            String username = Utils.ReadData(this, "username");
            String password = Utils.ReadData(this, "password");

            userText.setText(username);
            passText.setText(password);
        }

        help = (TextView) this.findViewById(R.id.help);
        help.setOnClickListener(new OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                // TODO Auto-generated method stub
                MainActivity.this.finish();

                Intent intent = new Intent(MainActivity.this,
                        TutorialsActivity.class);
                startActivity(intent);
            }
        });

        registerButton.setOnClickListener(new OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                // TODO Auto-generated method stub
                if (!Utils.IsNetworkAvailable(MainActivity.this))
                {
                    Utils.ShowToast(MainActivity.this, R.string.networknot);
                    return;
                }
                new GetLevelId(MainActivity.this).execute(0);// 0 switch to
                                                             // Register Screen

            }
        });

        loginButton.setOnClickListener(new OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                // TODO Auto-generated method stub

                String username = userText.getText().toString();
                String password = passText.getText().toString();

                // save passtmp
                // if(Utils.IsSavedPassword(MainActivity.this))
                {
                    Utils.WriteData(MainActivity.this, "passwordtmp", password);
                }

                String passHash = Utils.getMd5Hash(password);

                if (username == null || username.trim().equals(""))
                {

                    Toast.makeText(MainActivity.this,
                            getResources().getString(R.string.nhapusername),
                            Toast.LENGTH_SHORT).show();

                }
                else
                    if (password == null || password.trim().equals(""))
                    {

                        Toast.makeText(
                                MainActivity.this,
                                getResources().getString(R.string.nhappassword),
                                Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        if (IDefine.isDebugLogin)
                        {
                            MainActivity.this.finish();
                            Intent intent = new Intent(MainActivity.this,
                                    MainMenuActivity.class);
                            MainActivity.this.startActivity(intent);
                        }
                        else
                        {

                            if (!Utils.IsNetworkAvailable(MainActivity.this))
                            {
                                Utils.ShowToast(MainActivity.this,
                                        R.string.networknot);
                                return;
                            }
                            new Login(MainActivity.this).execute(username,
                                    passHash);
                        }
                    }

            }
        });

        if (!IDefine.HAVE_ADS)
        {
            RelativeLayout v = (RelativeLayout) this.findViewById(R.id.wrapper);

            final int INDEX_FOOTER_VIEW = 2;
            v.removeViewAt(INDEX_FOOTER_VIEW);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public void onBackPressed()
    {
        showDialog(DIALOG_EXIT);
    }

    @Override
    public void startActivity(Intent intent)
    {
        super.startActivity(intent);
        this.overridePendingTransition(R.anim.anim_activity_go_to_left,
                R.anim.anim_activity_go_to_right);
    }

    public void startActivityBack(Intent intent)
    {
        super.startActivity(intent);
        this.overridePendingTransition(R.anim.anim_activity_go_to_left_back,
                R.anim.anim_activity_go_to_right_back);
    }

    @Override
    protected Dialog onCreateDialog(int id)
    {
        // TODO Auto-generated method stub
        Dialog dialog = null;

        dialog = new Dialog(MainActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_exit);

        TextView titleExit = (TextView) dialog.findViewById(R.id.dialog_title);
        titleExit.setText(R.string.title_exit_dialog);

        _No = (Button) dialog.findViewById(R.id.btnDialogNo);
        _Yes = (Button) dialog.findViewById(R.id.btnDialogYes);

        if (_No != null)
        {
            _No.setOnClickListener(new OnClickListener()
            {

                @SuppressWarnings("deprecation")
                @Override
                public void onClick(View v)
                {
                    // TODO Auto-generated method stub
                    dismissDialog(DIALOG_EXIT);
                }
            });
        }

        if (_Yes != null)
        {
            _Yes.setOnClickListener(new OnClickListener()
            {

                @Override
                public void onClick(View v)
                {
                    // TODO Auto-generated method stub
                    // if (MainActivity.this.isEnablePressBackey)
                    {
                        MainActivity.this.finish();

                    }
                }
            });

        }

        return dialog;
    }

}
