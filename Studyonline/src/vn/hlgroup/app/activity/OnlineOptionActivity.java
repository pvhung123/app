package vn.hlgroup.app.activity;

import java.util.ArrayList;

import vn.hlgroup.app.IDefine;
import vn.hlgroup.app.R;
import vn.hlgroup.app.controller.GetQuestionOnline;
import vn.hlgroup.app.controller.Logout;
import vn.hlgroup.app.model.ExamOnline;
import vn.hlgroup.app.model.Subject;
import vn.hlgroup.app.singleton.AppManager;
import vn.hlgroup.app.utils.Utils;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;

public class OnlineOptionActivity extends CoreActivity
{

    private int backeyNumPress = 0;

    private long timeResetbackey = 0;

    TextView titleCompetition;

    EditText titleSubject;
    EditText titleTime;
    EditText titlenum;

    Button btnStart;
    Button btnPre;
    Button btnNext;

    int competitionPage = 0;
    private AdView mAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_online_option);

        competitionPage = getIntent().getExtras().getInt("selectitem");

        // ads
        LinearLayout layout = (LinearLayout) this
                .findViewById(R.id.adViewLayout);

        // Create a banner ad. The ad size and ad unit ID must be set before
        // calling loadAd.
        mAdView = new AdView(this);
        mAdView.setAdSize(AdSize.SMART_BANNER);
        mAdView.setAdUnitId(IDefine.ADMOB_ID);

        // Create an ad request.
        AdRequest.Builder adRequestBuilder = new AdRequest.Builder();

        // Optionally populate the ad request builder.
        // adRequestBuilder.addTestDevice(AdRequest.DEVICE_ID_EMULATOR);

        // Add the AdView to the view hierarchy.
        layout.addView(mAdView);

        // Start loading the ad.
        mAdView.loadAd(adRequestBuilder.build());

        titleCompetition = (TextView) this
                .findViewById(R.id.txvtitleCompetition);

        titleSubject = (EditText) this.findViewById(R.id.txvSubject);
        titleTime = (EditText) this.findViewById(R.id.txvTime);
        titlenum = (EditText) this.findViewById(R.id.txvNumberQOnline);
        btnStart = (Button) this.findViewById(R.id.btnStartOnlineOption);
        btnPre = (Button) this.findViewById(R.id.btnPreOnlineOption);
        btnNext = (Button) this.findViewById(R.id.btnNextOnlineOption);

        OnClickListener onClick = new OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                // TODO Auto-generated method stub
                AppManager app = AppManager.getInstance();

                int id = ((Button) v).getId();
                switch (id)
                {
                    case R.id.btnStartOnlineOption:

                        app.setIdOfOnlineExam(competitionPage);

                        if (!Utils
                                .IsNetworkAvailable(OnlineOptionActivity.this))
                        {
                            Utils.ShowToast(OnlineOptionActivity.this,
                                    R.string.networknot);
                            return;
                        }

                        new GetQuestionOnline(OnlineOptionActivity.this)
                                .execute(app.getOnlineExams()
                                        .get(competitionPage).getId());
                        break;
                    case R.id.btnPreOnlineOption:
                        if (competitionPage > 0)
                        {
                            competitionPage--;
                            showCompetition(competitionPage);

                            if (competitionPage == 0)
                            {
                                btnPre.setEnabled(false);
                                btnNext.setEnabled(true);
                            }
                        }
                        break;
                    case R.id.btnNextOnlineOption:
                        if (competitionPage < app.getOnlineExams().size() - 1)
                        {
                            competitionPage++;
                            showCompetition(competitionPage);

                            if (competitionPage == app.getOnlineExams().size() - 1)
                            {
                                btnPre.setEnabled(true);
                                btnNext.setEnabled(false);
                            }
                        }
                        break;
                }

            }
        };

        btnStart.setOnClickListener(onClick);
        btnPre.setOnClickListener(onClick);
        btnNext.setOnClickListener(onClick);

        AppManager app = AppManager.getInstance();
        if (app.getOnlineExams().size() > 1)
        {

            if (competitionPage == 0)
            {
                btnPre.setEnabled(false);
                btnNext.setEnabled(true);
            }
            else
                if (competitionPage == app.getOnlineExams().size() - 1)
                {
                    btnPre.setEnabled(true);
                    btnNext.setEnabled(false);
                }
                else
                {
                    btnPre.setEnabled(true);
                    btnNext.setEnabled(true);
                }
        }
        else
        {
            btnPre.setEnabled(false);
            btnNext.setEnabled(false);
        }

        showCompetition(competitionPage);

        Button btnLogout = (Button) this.findViewById(R.id.btnLogout);
        btnLogout.setOnClickListener(new OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                showDialog(DIALOG_EXIT);
            }
        });

        if (!IDefine.HAVE_ADS)
        {
            RelativeLayout v = (RelativeLayout) this.findViewById(R.id.wrapper);

            final int INDEX_FOOTER_VIEW = 2;
            v.removeViewAt(INDEX_FOOTER_VIEW);
        }

    }

    @Override
    public void onResume()
    {
        super.onResume();

        // Resume the AdView.
        mAdView.resume();

        if (timeHideNotify != 0)
        {
            if (timeShowNotify - timeHideNotify > IDefine.SESSION_TIME_OUT)
            {
                if (Utils.IsNetworkAvailable(this))
                {
                    new Logout(this).execute(AppManager.getInstance().getUser()
                            .getUsername());
                }
                // else
                // {

                // this.finish();

                // Intent intent = new Intent(this, MainActivity.class);
                // startActivityBack(intent);
                // }
            }
        }
    }

    @Override
    public void onPause()
    {
        // Pause the AdView.
        mAdView.pause();

        super.onPause();
    }

    @Override
    public void startActivity(Intent intent)
    {
        super.startActivity(intent);
        this.overridePendingTransition(R.anim.anim_activity_go_to_left,
                R.anim.anim_activity_go_to_right);
    }

    @Override
    public void startActivityBack(Intent intent)
    {
        super.startActivity(intent);
        this.overridePendingTransition(R.anim.anim_activity_go_to_left_back,
                R.anim.anim_activity_go_to_right_back);
    }

    @Override
    public void onBackPressed()
    {
        /*
         * if (System.currentTimeMillis() - timeResetbackey > 3000) {
         * backeyNumPress = 0; } if (backeyNumPress == 0) { String text =
         * this.getResources().getString(R.string.backey);
         * Toast.makeText(OnlineOptionActivity.this, text, Toast.LENGTH_SHORT)
         * .show(); backeyNumPress = 1; timeResetbackey =
         * System.currentTimeMillis(); } else if (backeyNumPress == 1) {
         * backeyNumPress = 0; this.finish();
         * 
         * Intent intent = new Intent(this, OnlineExamMenuActivity.class);
         * startActivityBack(intent);
         * 
         * }
         */
        currentClassName = "OnlineOptionActivity";
        showDialog(DIALOG_EXIT);
    }

    private void showCompetition(int index)
    {
        AppManager app = AppManager.getInstance();
        ArrayList<ExamOnline> exams = app.getOnlineExams();

        ExamOnline e = exams.get(index);

        titleCompetition.setText(e.getTitle());

        if (e.getSubjectId() == 0)
        {
            titleSubject.setText("Tong hop");
        }
        else
        {
            ArrayList<Subject> subjectAll = app.getSubjectsAll();
            int len = subjectAll.size();
            for (int i = 0; i < len; i++)
            {
                if (e.getSubjectId() == subjectAll.get(i).getId())
                {
                    titleSubject.setText(subjectAll.get(i).getName());
                    break;
                }
            }
        }

        titleTime.setText(String.valueOf(e.getTimeToTest()) + " "
                + getResources().getString(R.string.phut));
        titlenum.setText(String.valueOf(e.getNumberOfQuestion()) + " "
                + getResources().getString(R.string.cau));

    }
}
