package vn.hlgroup.app.activity;

import vn.hlgroup.app.IDefine;
import vn.hlgroup.app.R;
import vn.hlgroup.app.controller.GetImages;
import vn.hlgroup.app.controller.Logout;
import vn.hlgroup.app.model.ExamDetail;
import vn.hlgroup.app.model.Question;
import vn.hlgroup.app.singleton.AppManager;
import vn.hlgroup.app.utils.Utils;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;

public class MainExamResultActivity extends CoreActivity
{

    private Button pre;
    private Button next;

    private RadioButton result1;

    private RadioButton result2;

    private RadioButton result3;

    private RadioButton result4;

    private RadioGroup rGroup;

    private TextView titleText = null;
    private ImageView titleImg = null;

    private Button _No, _Yes;

    public static final int DIALOG_EXIT = 1;

    private int backeyNumPress = 0;

    private long timeResetbackey = 0;
    private AdView mAdView;

    // private TextView txtResult;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_exam_result);

        AppManager app = AppManager.getInstance();

        // ads
        LinearLayout layout = (LinearLayout) this
                .findViewById(R.id.adViewLayout);

        // Create a banner ad. The ad size and ad unit ID must be set before
        // calling loadAd.
        mAdView = new AdView(this);
        mAdView.setAdSize(AdSize.SMART_BANNER);
        mAdView.setAdUnitId(IDefine.ADMOB_ID);

        // Create an ad request.
        AdRequest.Builder adRequestBuilder = new AdRequest.Builder();

        // Optionally populate the ad request builder.
        // adRequestBuilder.addTestDevice(AdRequest.DEVICE_ID_EMULATOR);

        // Add the AdView to the view hierarchy.
        layout.addView(mAdView);

        // Start loading the ad.
        mAdView.loadAd(adRequestBuilder.build());

        // TextView score =
        // (TextView)this.findViewById(R.id.txvScoremainexamresult);
        // score.setText(app.getRightAnswer()+"/"+app.getQuestions().size());

        // txtResult = (TextView) this.findViewById(R.id.txtResult);

        /*
         * TextView score =
         * (TextView)this.findViewById(R.id.txvScoremainexamresult); String
         * resultScore = getResources().getString(R.string.resultScore); String
         * scoreStr = getResources().getString(R.string.main_exam_score); String
         * ladderStr = getResources().getString(R.string.scoreladder);
         * score.setText
         * (resultScore+": "+app.getRightAnswer()+"/"+app.getQuestions
         * ().size());
         * 
         * TextView scoreladder =
         * (TextView)this.findViewById(R.id.txvScoreLadder);
         * scoreladder.setText(
         * scoreStr+": "+(app.getRightAnswer()*100f/app.getQuestions().size()));
         * 
         * TextView txvScoreLadderStr =
         * (TextView)this.findViewById(R.id.txvScoreLadderStr);
         * txvScoreLadderStr.setText(ladderStr);
         */

        TextView score = (TextView) this.findViewById(R.id.txvScore);
        String resultScore = getResources().getString(R.string.resultScore);
        String scoreStr = getResources().getString(R.string.main_exam_score);
        String ladderStr = getResources().getString(R.string.scoreladder);
        score.setText(resultScore
                + ": "
                + app.getRightAnswer()
                + "/"
                + app.getQuestions().size()
                + ". "
                + scoreStr
                + ": "
                + Utils.roundTwoDigit(String.valueOf(app.getRightAnswer()
                        * 100f / app.getQuestions().size())) + ". " + ladderStr);

        pre = (Button) this.findViewById(R.id.btnPreResult);
        pre.setEnabled(false);
        pre.setOnClickListener(new OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                // TODO Auto-generated method stub

                AppManager app = AppManager.getInstance();
                Log.d("abc", app.getQuestions().size() + "");

                if (app.getQuestionPage() - 1 == 0)
                {
                    MainExamResultActivity.this.pre.setEnabled(false);
                }

                if (app.getQuestionPage() > 0)
                {
                    MainExamResultActivity.this.next.setEnabled(true);

                    int questionType = app.getQuestions()
                            .get(app.getQuestionPage() - 1).getIsImage();

                    if (questionType == 0)
                    {
                        addTitleQuestionText();

                        showQuestion(app.getQuestionPage() - 1);
                    }
                    else
                    {
                        addTitleQuestionImage();
                        showQuestionImage(app.getQuestionPage() - 1);
                    }
                }

            }
        });

        next = (Button) this.findViewById(R.id.btnNextResult);

        next.setOnClickListener(new OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                // TODO Auto-generated method stub

                AppManager app = AppManager.getInstance();
                if (app.getQuestionPage() + 1 == app.getQuestions().size() - 1)
                {
                    MainExamResultActivity.this.next.setEnabled(false);
                }

                if (app.getQuestionPage() < app.getQuestions().size() - 1)
                {
                    MainExamResultActivity.this.pre.setEnabled(true);

                    int questionType = app.getQuestions()
                            .get(app.getQuestionPage() + 1).getIsImage();

                    if (questionType == 0)
                    {
                        addTitleQuestionText();

                        showQuestion(app.getQuestionPage() + 1);
                    }
                    else
                    {
                        addTitleQuestionImage();
                        showQuestionImage(app.getQuestionPage() + 1);
                    }
                }

            }
        });
        rGroup = (RadioGroup) this.findViewById(R.id.rdoGroupResultsTranning);
        rGroup.clearCheck();

        result1 = (RadioButton) this.findViewById(R.id.radio0Result);
        result1.setEnabled(false);

        result2 = (RadioButton) this.findViewById(R.id.radio1Result);
        result2.setEnabled(false);

        result3 = (RadioButton) this.findViewById(R.id.radio2Result);
        result3.setEnabled(false);

        result4 = (RadioButton) this.findViewById(R.id.radio3Result);
        result4.setEnabled(false);

        int questionType = app.getQuestions().get(0).getIsImage();
        if (questionType == 0)
        {
            addTitleQuestionText();
            showQuestion(0);
        }
        else
        {
            addTitleQuestionImage();
            showQuestionImage(0);
        }

        Button btnLogout = (Button) this.findViewById(R.id.btnLogout);
        btnLogout.setOnClickListener(new OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                showDialog(DIALOG_EXIT);
            }
        });

        if (!IDefine.HAVE_ADS)
        {
            RelativeLayout v = (RelativeLayout) this.findViewById(R.id.wrapper);

            final int INDEX_FOOTER_VIEW = 2;
            v.removeViewAt(INDEX_FOOTER_VIEW);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_exam_result, menu);
        return true;
    }

    @Override
    public void onResume()
    {
        super.onResume();

        // Resume the AdView.
        mAdView.resume();

        if (timeHideNotify != 0)
        {
            if (timeShowNotify - timeHideNotify > IDefine.SESSION_TIME_OUT)
            {
                if (Utils.IsNetworkAvailable(this))
                {
                    new Logout(this).execute(AppManager.getInstance().getUser()
                            .getUsername());
                }
                // else
                // {

                // this.finish();

                // Intent intent = new Intent(this, MainActivity.class);
                // startActivityBack(intent);
                // }
            }
        }
    }

    @Override
    public void onPause()
    {
        // Pause the AdView.
        mAdView.pause();

        super.onPause();
    }

    @Override
    public void onBackPressed()
    {

        /*
         * if (System.currentTimeMillis() - timeResetbackey > 3000) {
         * backeyNumPress = 0; } if (backeyNumPress == 0) { String text =
         * this.getResources().getString(R.string.backey);
         * Toast.makeText(MainExamResultActivity.this, text,
         * Toast.LENGTH_SHORT).show(); backeyNumPress = 1; timeResetbackey =
         * System.currentTimeMillis(); } else if (backeyNumPress == 1) {
         * backeyNumPress = 0; this.finish();
         * 
         * Intent intent = new Intent(this, MainMenuActivity.class);
         * startActivityBack(intent);
         * 
         * }
         */
        currentClassName = "MainExamResultActivity";
        showDialog(DIALOG_EXIT);
    }

    @Override
    public void startActivity(Intent intent)
    {
        super.startActivity(intent);
        this.overridePendingTransition(R.anim.anim_activity_go_to_left,
                R.anim.anim_activity_go_to_right);
    }

    @Override
    public void startActivityBack(Intent intent)
    {
        super.startActivity(intent);
        this.overridePendingTransition(R.anim.anim_activity_go_to_left_back,
                R.anim.anim_activity_go_to_right_back);
    }

    private void showQuestion(int index)
    {
        AppManager mana = AppManager.getInstance();
        mana.setQuestionPage(index);

        Question q = mana.getQuestions().get(index);

        TextView textViewTitle = (TextView) this
                .findViewById(R.id.txvtitleResult);
        textViewTitle.setText(q.getTitle());

        String questionstr = this.getResources().getString(
                R.string.main_exam_result_question);

        TextView textViewCau = (TextView) this.findViewById(R.id.txvCauResult);
        textViewCau.setText(questionstr + ": " + (index + 1) + "/"
                + mana.getQuestions().size());

        result1.setText(q.getResult1());
        result1.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);

        result2.setText(q.getResult2());
        result2.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);

        result3.setText(q.getResult3());
        result3.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);

        result4.setText(q.getResult4());
        result4.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);

        if (q.getResult1() == null || q.getResult1().equals(""))
        {
            result1.setVisibility(View.INVISIBLE);
        }
        else
        {
            result1.setVisibility(View.VISIBLE);
        }
        if (q.getResult2() == null || q.getResult2().equals(""))
        {
            result2.setVisibility(View.INVISIBLE);
        }
        else
        {
            result2.setVisibility(View.VISIBLE);
        }
        if (q.getResult3() == null || q.getResult3().equals(""))
        {
            result3.setVisibility(View.INVISIBLE);
        }
        else
        {
            result3.setVisibility(View.VISIBLE);
        }
        if (q.getResult4() == null || q.getResult4().equals(""))
        {
            result4.setVisibility(View.INVISIBLE);
        }
        else
        {
            result4.setVisibility(View.VISIBLE);
        }

        // result1.setChecked(false);
        // result2.setChecked(false);
        // result3.setChecked(false);
        // result4.setChecked(false);

        result1.setTextColor(Color.rgb(0, 0, 0));
        result2.setTextColor(Color.rgb(0, 0, 0));
        result3.setTextColor(Color.rgb(0, 0, 0));
        result4.setTextColor(Color.rgb(0, 0, 0));

        // bind result
        // get answer of user
        ExamDetail exam = mana.getResultBoard().getExamDetails().get(index);
        int choose = exam.getResult();

        // get right answer
        Question currentQ = mana.getQuestions().get(index);
        int rightChoose = Integer.parseInt(currentQ.getResult());

        if (choose != rightChoose)
        {
            switch (choose)
            {
                case ExamDetail.ANSWER_ONE:
                    result1.setChecked(true);
                    result1.setTextColor(Color.rgb(255, 68, 68));
                    break;
                case ExamDetail.ANSWER_TWO:
                    result2.setChecked(true);
                    result2.setTextColor(Color.rgb(255, 68, 68));
                    break;
                case ExamDetail.ANSWER_THREE:
                    result3.setChecked(true);
                    result3.setTextColor(Color.rgb(255, 68, 68));
                    break;
                case ExamDetail.ANSWER_FOUR:
                    result4.setChecked(true);
                    result4.setTextColor(Color.rgb(255, 68, 68));
                    break;
                default:
                    rGroup.clearCheck();
            }
        }
        else
        {
            switch (rightChoose)
            {
                case ExamDetail.ANSWER_ONE:

                    result1.setChecked(true);
                    break;
                case ExamDetail.ANSWER_TWO:

                    result2.setChecked(true);
                    break;
                case ExamDetail.ANSWER_THREE:

                    result3.setChecked(true);
                    break;
                case ExamDetail.ANSWER_FOUR:

                    result4.setChecked(true);
                    break;
            }
        }

        switch (rightChoose)
        {
            case ExamDetail.ANSWER_ONE:

                result1.setTextColor(Color.rgb(153, 204, 0));
                // txtResult.setText("A");
                break;
            case ExamDetail.ANSWER_TWO:

                result2.setTextColor(Color.rgb(153, 204, 0));
                // txtResult.setText("B");
                break;
            case ExamDetail.ANSWER_THREE:

                result3.setTextColor(Color.rgb(153, 204, 0));
                // txtResult.setText("C");
                break;
            case ExamDetail.ANSWER_FOUR:

                result4.setTextColor(Color.rgb(153, 204, 0));
                // txtResult.setText("D");
                break;
        }

    }

    private void showQuestionImage(int index)
    {
        AppManager mana = AppManager.getInstance();
        mana.setQuestionPage(index);

        Question q = mana.getQuestions().get(index);

        // ImageView imgViewTitle =
        // (ImageView)this.findViewById(R.id.imgQuestionTitleResult);
        // imgViewTitle.setImageResource(R.drawable.q_1_1_1_1_1_1);

        TextView textViewCau = (TextView) this.findViewById(R.id.txvCauResult);
        textViewCau.setText("Question: " + (index + 1) + "/"
                + mana.getQuestions().size());

        result1.setText(null);
        // result1.setCompoundDrawablesWithIntrinsicBounds(R.drawable.a1_1_1_1_1_1_1,
        // 0, 0, 0 );

        result2.setText(null);
        // result2.setCompoundDrawablesWithIntrinsicBounds(R.drawable.a2_1_1_1_1_1_1,
        // 0, 0, 0 );

        result3.setText(null);
        // result3.setCompoundDrawablesWithIntrinsicBounds(R.drawable.a3_1_1_1_1_1_1,
        // 0, 0, 0 );

        result4.setText(null);
        // result4.setCompoundDrawablesWithIntrinsicBounds(R.drawable.a4_1_1_1_1_1_1,
        // 0, 0, 0 );

        if (q.getResult1() == null || q.getResult1().equals(""))
        {
            result1.setVisibility(View.INVISIBLE);
        }
        else
        {
            result1.setVisibility(View.VISIBLE);
        }
        if (q.getResult2() == null || q.getResult2().equals(""))
        {
            result2.setVisibility(View.INVISIBLE);
        }
        else
        {
            result2.setVisibility(View.VISIBLE);
        }
        if (q.getResult3() == null || q.getResult3().equals(""))
        {
            result3.setVisibility(View.INVISIBLE);
        }
        else
        {
            result3.setVisibility(View.VISIBLE);
        }
        if (q.getResult4() == null || q.getResult4().equals(""))
        {
            result4.setVisibility(View.INVISIBLE);
        }
        else
        {
            result4.setVisibility(View.VISIBLE);
        }

        new GetImages(this).execute(
                IDefine.HEADER_LINK_UPLOAD + "/upload/" + q.getTitle(),
                IDefine.HEADER_LINK_UPLOAD + "/upload/" + q.getResult1(),
                IDefine.HEADER_LINK_UPLOAD + "/upload/" + q.getResult2(),
                IDefine.HEADER_LINK_UPLOAD + "/upload/" + q.getResult3(),
                IDefine.HEADER_LINK_UPLOAD + "/upload/" + q.getResult4());

        // bind result
        ExamDetail exam = mana.getResultBoard().getExamDetails().get(index);
        int choose = exam.getResult();

        // get right answer
        Question currentQ = mana.getQuestions().get(index);
        int rightChoose = Integer.parseInt(currentQ.getResult());

        String sai = getResources().getString(R.string.wrong);
        String dung = getResources().getString(R.string.right);

        if (choose != rightChoose)
        {
            switch (choose)
            {
                case ExamDetail.ANSWER_ONE:
                    result1.setChecked(true);
                    result1.setTextColor(Color.rgb(255, 68, 68));
                    result1.setText(sai);
                    break;
                case ExamDetail.ANSWER_TWO:
                    result2.setChecked(true);
                    result2.setTextColor(Color.rgb(255, 68, 68));
                    result2.setText(sai);
                    break;
                case ExamDetail.ANSWER_THREE:
                    result3.setChecked(true);
                    result3.setTextColor(Color.rgb(255, 68, 68));
                    result3.setText(sai);
                    break;
                case ExamDetail.ANSWER_FOUR:
                    result4.setChecked(true);
                    result4.setTextColor(Color.rgb(255, 68, 68));
                    result4.setText(sai);
                    break;
                default:
                    rGroup.clearCheck();
            }
        }
        else
        {
            switch (rightChoose)
            {
                case ExamDetail.ANSWER_ONE:

                    result1.setChecked(true);
                    break;
                case ExamDetail.ANSWER_TWO:

                    result2.setChecked(true);
                    break;
                case ExamDetail.ANSWER_THREE:

                    result3.setChecked(true);
                    break;
                case ExamDetail.ANSWER_FOUR:

                    result4.setChecked(true);
                    break;
            }
        }

        switch (rightChoose)
        {
            case ExamDetail.ANSWER_ONE:

                result1.setTextColor(Color.rgb(153, 204, 0));
                result1.setText(dung);
                // txtResult.setText("A");
                break;
            case ExamDetail.ANSWER_TWO:

                result2.setTextColor(Color.rgb(153, 204, 0));
                result2.setText(dung);
                // txtResult.setText("B");
                break;
            case ExamDetail.ANSWER_THREE:

                result3.setTextColor(Color.rgb(153, 204, 0));
                result3.setText(dung);
                // txtResult.setText("C");
                break;
            case ExamDetail.ANSWER_FOUR:

                result4.setTextColor(Color.rgb(153, 204, 0));
                result4.setText(dung);
                // txtResult.setText("D");
                break;
        }
    }

    private void addTitleQuestionText()
    {
        if (titleText != null)
        {
            ((LinearLayout) (titleText.getParent())).removeViewAt(0);
            titleText = null;
        }

        if (titleImg != null)
        {
            ((LinearLayout) (titleImg.getParent())).removeViewAt(0);
            titleImg = null;
        }

        titleText = (TextView) View.inflate(this,
                R.drawable.txv_question_title_result, null);

        LinearLayout titleLayout = (LinearLayout) this
                .findViewById(R.id.txvtitlelayoutResult);
        titleLayout.addView(titleText);
    }

    private void addTitleQuestionImage()
    {
        if (titleText != null)
        {
            ((LinearLayout) (titleText.getParent())).removeViewAt(0);
            titleText = null;
        }

        if (titleImg != null)
        {
            ((LinearLayout) (titleImg.getParent())).removeViewAt(0);
            titleImg = null;
        }

        titleImg = (ImageView) View.inflate(this,
                R.drawable.img_question_title_result, null);

        LinearLayout titleLayout = (LinearLayout) this
                .findViewById(R.id.txvtitlelayoutResult);
        titleLayout.addView(titleImg);
    }

    /*
     * @Override protected Dialog onCreateDialog(int id) { // TODO
     * Auto-generated method stub Dialog dialog = null;
     * 
     * dialog = new Dialog(MainExamResultActivity.this);
     * dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
     * dialog.setContentView(R.layout.dialog_exit);
     * 
     * _No = (Button) dialog.findViewById(R.id.btnDialogNo); _Yes = (Button)
     * dialog.findViewById(R.id.btnDialogYes);
     * 
     * if (_No != null) { _No.setOnClickListener(new OnClickListener() {
     * 
     * @SuppressWarnings("deprecation")
     * 
     * @Override public void onClick(View v) { // TODO Auto-generated method
     * stub dismissDialog(DIALOG_EXIT); } }); }
     * 
     * if (_Yes != null) { _Yes.setOnClickListener(new OnClickListener() {
     * 
     * @Override public void onClick(View v) { // TODO Auto-generated method
     * stub // if (MainExamResultActivity.this.isEnablePressBackey) {
     * MainExamResultActivity.this.finish();
     * 
     * Intent intent = new Intent(MainExamResultActivity.this,
     * MainMenuActivity.class); startActivityBack(intent); } } });
     * 
     * }
     * 
     * return dialog; }
     */

}
