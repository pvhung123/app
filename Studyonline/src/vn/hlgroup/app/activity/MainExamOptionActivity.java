package vn.hlgroup.app.activity;

import java.util.ArrayList;
import java.util.List;

import vn.hlgroup.app.IDefine;
import vn.hlgroup.app.R;
import vn.hlgroup.app.adapter.CustomSpinnerAdapter;
import vn.hlgroup.app.adapter.MapItem;
import vn.hlgroup.app.controller.GetQuestionsMainExam;
import vn.hlgroup.app.controller.GetSubjectDependTypeMainExam;
import vn.hlgroup.app.controller.Logout;
import vn.hlgroup.app.model.Lesson;
import vn.hlgroup.app.model.Subject;
import vn.hlgroup.app.model.Type;
import vn.hlgroup.app.singleton.AppManager;
import vn.hlgroup.app.utils.Utils;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;

public class MainExamOptionActivity extends CoreActivity
{

    private Spinner spinKnowledge;

    private CustomSpinnerAdapter KnowledgedataAdapter;

    public Spinner spinSubject;

    public CustomSpinnerAdapter SubjectdataAdapter;

    private Spinner spinTopic;

    private CustomSpinnerAdapter TopicdataAdapter;

    private Button btnStartTranning;

    private Button _No, _Yes;

    public static final int DIALOG_EXIT = 1;

    private int backeyNumPress = 0;

    private long timeResetbackey = 0;
    private AdView mAdView;

    private Button btnopenSpin;
    private Button btnopenSpin1;
    private Button btnopenSpin2;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_exam_option);

        // ads
        LinearLayout layout = (LinearLayout) this
                .findViewById(R.id.adViewLayout);

        // Create a banner ad. The ad size and ad unit ID must be set before
        // calling loadAd.
        mAdView = new AdView(this);
        mAdView.setAdSize(AdSize.SMART_BANNER);
        mAdView.setAdUnitId(IDefine.ADMOB_ID);

        // Create an ad request.
        AdRequest.Builder adRequestBuilder = new AdRequest.Builder();

        // Optionally populate the ad request builder.
        // adRequestBuilder.addTestDevice(AdRequest.DEVICE_ID_EMULATOR);

        // Add the AdView to the view hierarchy.
        layout.addView(mAdView);

        // Start loading the ad.
        mAdView.loadAd(adRequestBuilder.build());

        spinKnowledge = (Spinner) this
                .findViewById(R.id.spinKnowledgemainexamOption);
        spinSubject = (Spinner) this
                .findViewById(R.id.spinSubjectmainexamOption);
        spinTopic = (Spinner) this.findViewById(R.id.spintpicmainexamOption);

        // for knowledge spinner
        List<MapItem> list = new ArrayList<MapItem>();

        ArrayList<Type> types = AppManager.getInstance().getTypes();

        int len = types.size();

        for (int i = 0; i < len; i++)
        {
            Type t = types.get(i);
            MapItem mi = new MapItem(t.getId(), t.getName());
            list.add(mi);
        }

        KnowledgedataAdapter = new CustomSpinnerAdapter(this,
                R.drawable.custom_spinner_item, list);
        KnowledgedataAdapter
                .setDropDownViewResource(R.drawable.custom_spinner_dropdown_item);
        spinKnowledge.setAdapter(KnowledgedataAdapter);

        spinKnowledge.setOnItemSelectedListener(new OnItemSelectedListener()
        {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                    int arg2, long arg3)
            {
                // TODO Auto-generated method stub
                MapItem item = MainExamOptionActivity.this.KnowledgedataAdapter
                        .getItem(arg2);
                new GetSubjectDependTypeMainExam(MainExamOptionActivity.this)
                        .execute(item.getId());
                // Toast.makeText(TrainOptionActivity.this,
                // String.valueOf(item.getId()), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0)
            {
                // TODO Auto-generated method stub

            }

        });

        // for subject spinner
        List<MapItem> listsubject = new ArrayList<MapItem>();

        ArrayList<Subject> subjects = AppManager.getInstance().getSubjects();

        int len1 = subjects.size();

        for (int i = 0; i < len1; i++)
        {
            Subject s = subjects.get(i);
            MapItem mi = new MapItem(s.getId(), s.getName());
            listsubject.add(mi);
        }

        SubjectdataAdapter = new CustomSpinnerAdapter(this,
                R.drawable.custom_spinner_item, listsubject);
        SubjectdataAdapter
                .setDropDownViewResource(R.drawable.custom_spinner_dropdown_item);
        spinSubject.setAdapter(SubjectdataAdapter);

        spinSubject.setOnItemSelectedListener(new OnItemSelectedListener()
        {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                    int arg2, long arg3)
            {
                MainExamOptionActivity.this.SubjectdataAdapter.getItem(arg2);

                // Toast.makeText(MainExamOptionActivity.this,
                // String.valueOf(item.getId()), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0)
            {
                // TODO Auto-generated method stub

            }

        });

        // for topic spinner
        List<MapItem> listtopic = new ArrayList<MapItem>();
        ArrayList<Lesson> lessons = AppManager.getInstance().getLessons();

        int len2 = lessons.size();

        for (int i = 0; i < len2; i++)
        {
            Lesson l = lessons.get(i);
            MapItem mi = new MapItem(l.getId(), l.getName());
            listtopic.add(mi);
        }

        TopicdataAdapter = new CustomSpinnerAdapter(this,
                R.drawable.custom_spinner_item, listtopic);
        TopicdataAdapter
                .setDropDownViewResource(R.drawable.custom_spinner_dropdown_item);
        spinTopic.setAdapter(TopicdataAdapter);

        spinTopic.setOnItemSelectedListener(new OnItemSelectedListener()
        {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                    int arg2, long arg3)
            {
                MainExamOptionActivity.this.TopicdataAdapter.getItem(arg2);

                // Toast.makeText(MainExamOptionActivity.this,
                // String.valueOf(item.getId()), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0)
            {
                // TODO Auto-generated method stub

            }

        });

        // for number Q spinner

        /*
         * List<MapItem> numQtopic = new ArrayList<MapItem>(); numQtopic.add(new
         * MapItem(5, "5 cau")); numQtopic.add(new MapItem(10, "10 cau"));
         * numQtopic.add(new MapItem(15, "15 cau"));
         * 
         * numberQdataAdapter = new SpinnerAdapter(this,
         * android.R.layout.simple_spinner_item, numQtopic);
         * numberQdataAdapter.setDropDownViewResource
         * (android.R.layout.simple_spinner_dropdown_item);
         * spinNumberQ.setAdapter(numberQdataAdapter);
         * 
         * spinNumberQ.setOnItemSelectedListener(new OnItemSelectedListener() {
         * 
         * @Override public void onItemSelected(AdapterView<?> arg0, View arg1,
         * int arg2, long arg3) { // TODO Auto-generated method stub MapItem
         * item =
         * (MapItem)MainExamOptionActivity.this.numberQdataAdapter.getItem
         * (arg2);
         * 
         * Toast.makeText(MainExamOptionActivity.this,
         * String.valueOf(item.getId()), Toast.LENGTH_SHORT).show(); }
         * 
         * @Override public void onNothingSelected(AdapterView<?> arg0) { //
         * TODO Auto-generated method stub
         * 
         * }
         * 
         * });
         */

        btnopenSpin = (Button) this.findViewById(R.id.btnopenSpin);
        btnopenSpin.setOnClickListener(new OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                // TODO Auto-generated method stub
                spinKnowledge.performClick();
            }
        });

        btnopenSpin1 = (Button) this.findViewById(R.id.btnopenSpin1);
        btnopenSpin1.setOnClickListener(new OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                // TODO Auto-generated method stub
                spinSubject.performClick();
            }
        });

        btnopenSpin2 = (Button) this.findViewById(R.id.btnopenSpin2);
        btnopenSpin2.setOnClickListener(new OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                // TODO Auto-generated method stub
                spinTopic.performClick();
            }
        });

        btnStartTranning = (Button) this
                .findViewById(R.id.btnStartmainexamOption);
        btnStartTranning.setOnClickListener(new OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                // TODO Auto-generated method stub

                // colllect select spinner and request link

                long knowledge = spinKnowledge.getSelectedItemId();
                long subject = spinSubject.getSelectedItemId();
                long topic = spinTopic.getSelectedItemId();

                // Toast.makeText(MainExamOptionActivity.this, knowledge + " " +
                // subject + " " + topic + " " + 50, Toast.LENGTH_SHORT).show();

                // TrainOptionActivity.this.finish();

                // Intent intent = new Intent(TrainOptionActivity.this,
                // TrainningActivity.class);
                // startActivity(intent);
                AppManager.getInstance().resetResultBoard();

                if (!Utils.IsNetworkAvailable(MainExamOptionActivity.this))
                {
                    Utils.ShowToast(MainExamOptionActivity.this,
                            R.string.networknot);
                    return;
                }

                new GetQuestionsMainExam(MainExamOptionActivity.this).execute(
                        IDefine.CATALOG_MAIN_TEST, AppManager.getInstance()
                                .getUser().getLevel_id(), (int) knowledge,
                        (int) subject, (int) topic, 50);
            }
        });

        Button btnLogout = (Button) this.findViewById(R.id.btnLogout);
        btnLogout.setOnClickListener(new OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                showDialog(DIALOG_EXIT);
            }
        });

        if (!IDefine.HAVE_ADS)
        {
            RelativeLayout v = (RelativeLayout) this.findViewById(R.id.wrapper);

            final int INDEX_FOOTER_VIEW = 2;
            v.removeViewAt(INDEX_FOOTER_VIEW);
        }
    }

    @Override
    public void onResume()
    {
        super.onResume();

        // Resume the AdView.
        mAdView.resume();

        if (timeHideNotify != 0)
        {
            if (timeShowNotify - timeHideNotify > IDefine.SESSION_TIME_OUT)
            {
                if (Utils.IsNetworkAvailable(this))
                {
                    new Logout(this).execute(AppManager.getInstance().getUser()
                            .getUsername());
                }
                // else
                // {

                // this.finish();

                // Intent intent = new Intent(this, MainActivity.class);
                // startActivityBack(intent);
                // }
            }
        }
    }

    @Override
    public void onPause()
    {
        // Pause the AdView.
        mAdView.pause();

        super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_exam, menu);
        return true;
    }

    @Override
    public void startActivity(Intent intent)
    {
        super.startActivity(intent);
        this.overridePendingTransition(R.anim.anim_activity_go_to_left,
                R.anim.anim_activity_go_to_right);
    }

    @Override
    public void startActivityBack(Intent intent)
    {
        super.startActivity(intent);
        this.overridePendingTransition(R.anim.anim_activity_go_to_left_back,
                R.anim.anim_activity_go_to_right_back);
    }

    @Override
    public void onBackPressed()
    {

        /*
         * if (System.currentTimeMillis() - timeResetbackey > 3000) {
         * backeyNumPress = 0; } if (backeyNumPress == 0) { String text =
         * this.getResources().getString(R.string.backey);
         * Toast.makeText(MainExamOptionActivity.this, text,
         * Toast.LENGTH_SHORT).show(); backeyNumPress = 1; timeResetbackey =
         * System.currentTimeMillis(); } else if (backeyNumPress == 1) {
         * backeyNumPress = 0; this.finish();
         * 
         * Intent intent = new Intent(this, MainMenuActivity.class);
         * startActivityBack(intent);
         * 
         * }
         */
        currentClassName = "MainExamOptionActivity";
        showDialog(DIALOG_EXIT);
    }

    /*
     * @Override protected Dialog onCreateDialog(int id) { // TODO
     * Auto-generated method stub Dialog dialog = null;
     * 
     * dialog = new Dialog(MainExamOptionActivity.this);
     * dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
     * dialog.setContentView(R.layout.dialog_exit);
     * 
     * _No = (Button) dialog.findViewById(R.id.btnDialogNo); _Yes = (Button)
     * dialog.findViewById(R.id.btnDialogYes);
     * 
     * if (_No != null) { _No.setOnClickListener(new OnClickListener() {
     * 
     * @SuppressWarnings("deprecation")
     * 
     * @Override public void onClick(View v) { // TODO Auto-generated method
     * stub dismissDialog(DIALOG_EXIT); } }); }
     * 
     * if (_Yes != null) { _Yes.setOnClickListener(new OnClickListener() {
     * 
     * @Override public void onClick(View v) { // TODO Auto-generated method
     * stub // if (MainExamOptionActivity.this.isEnablePressBackey) {
     * MainExamOptionActivity.this.finish();
     * 
     * Intent intent = new Intent(MainExamOptionActivity.this,
     * MainMenuActivity.class); startActivityBack(intent); } } });
     * 
     * }
     * 
     * return dialog; }
     */

}
