package vn.hlgroup.app.activity;

import vn.hlgroup.app.IDefine;
import vn.hlgroup.app.R;
import vn.hlgroup.app.controller.GetOnlineExamDid;
import vn.hlgroup.app.controller.Logout;
import vn.hlgroup.app.model.ExamOnline;
import vn.hlgroup.app.model.User;
import vn.hlgroup.app.singleton.AppManager;
import vn.hlgroup.app.utils.Utils;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;

public class OnlineExamMenuActivity extends CoreActivity
{

    // private LinearLayout layoutblank;
    private AdView mAdView;
    private LinearLayout adViewLayout;

    private LinearLayout linearLayout1;
    private LinearLayout linearLayout3;
    // private LinearLayout linearLayout0;
    // private LinearLayout layoutfooter;

    private int backeyNumPress = 0;

    private Button btnResultOnline;

    private long timeResetbackey = 0;

    LinearLayout listView;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        View mainView = getLayoutInflater().inflate(
                R.layout.activity_online_exam_menu, null);

        AppManager app = AppManager.getInstance();

        // ads
        adViewLayout = (LinearLayout) mainView.findViewById(R.id.adViewLayout);

        // Create a banner ad. The ad size and ad unit ID must be set before
        // calling loadAd.
        mAdView = new AdView(this);
        mAdView.setAdSize(AdSize.SMART_BANNER);
        mAdView.setAdUnitId(IDefine.ADMOB_ID);

        // Create an ad request.
        AdRequest.Builder adRequestBuilder = new AdRequest.Builder();

        // Optionally populate the ad request builder.
        // adRequestBuilder.addTestDevice(AdRequest.DEVICE_ID_EMULATOR);

        // Add the AdView to the view hierarchy.
        adViewLayout.addView(mAdView);

        // Start loading the ad.
        mAdView.loadAd(adRequestBuilder.build());

        listView = (LinearLayout) mainView.findViewById(R.id.listonline);

        if (app.getOnlineExams().size() == 0)
        {
            LinearLayout layoutChild;

            layoutChild = (LinearLayout) View.inflate(this,
                    R.drawable.template_layout_list_item_2_2, null);

            TextView t = (TextView) ((LinearLayout) layoutChild.getChildAt(0))
                    .getChildAt(0);
            t.setText(getResources().getString(R.string.online_dont_have));

            layoutChild.removeViewAt(1);
            // t.setId(i);
            listView.addView(layoutChild);

        }

        for (int i = 0; i < app.getOnlineExams().size(); i++)
        {
            ExamOnline e = app.getOnlineExams().get(i);
            String str = e.getTitle();

            // TextView t = new TextView(this);
            // t.setText(str);
            // t.setTextSize(20);
            // t.setId(i);
            // listView.addView(t);
            LinearLayout layoutChild;
            if (e.IsDidExam())
            {
                layoutChild = (LinearLayout) View.inflate(this,
                        R.drawable.template_layout_list_item_2, null);
            }
            else
            {
                layoutChild = (LinearLayout) View.inflate(this,
                        R.drawable.template_layout_list_item_2_2, null);

            }
            TextView t = (TextView) ((LinearLayout) layoutChild.getChildAt(0))
                    .getChildAt(0);
            t.setText(str);
            t.setId(i);
            listView.addView(layoutChild);

            if (i == app.getOnlineExams().size() - 1)
            {

                layoutChild.removeViewAt(1);
            }

            if (e.IsDidExam())
            {
                t.setTextColor(getResources().getColor(R.color.SRed));

                TextView t1 = (TextView) ((LinearLayout) layoutChild
                        .getChildAt(0)).getChildAt(1);
                t1.setTextColor(getResources().getColor(R.color.SRed));
                t1.setText(getResources().getString(R.string.dathi));
                t1.setTextSize(13);
                t1.setTypeface(null, Typeface.BOLD);
            }
            else
            {

                t.setOnClickListener(new OnClickListener()
                {

                    @Override
                    public void onClick(View v)
                    {
                        // TODO Auto-generated method stub

                        // if(!Utils.IsNetworkAvailable(OnlineExamMenuActivity.this))
                        // {
                        // Utils.ShowToast(OnlineExamMenuActivity.this,
                        // R.string.networknot);
                        // return;
                        // }

                        User u = AppManager.getInstance().getUser();

                        Log.d("abc", u.getUpdated_date() + "");
                        Log.d("abc", u.getCreated_date() + "");

                        Log.d("abc", System.currentTimeMillis() + "");

                        if (!IDefine.IS_THREE_DAY_ONLINE_STUDY_LIMIT)
                        {
                            OnlineExamMenuActivity.this.finish();

                            Intent intent = new Intent(
                                    OnlineExamMenuActivity.this,
                                    OnlineOptionActivity.class);
                            intent.putExtra("selectitem", v.getId());
                            startActivity(intent);
                        }
                        else
                        {
                            if (u.getCreated_date() != u.getUpdated_date()
                                    && System.currentTimeMillis() / 1000
                                            - u.getUpdated_date() < 259200)
                            {

                                Utils.ShowToast(
                                        OnlineExamMenuActivity.this,
                                        getResources().getString(
                                                R.string.bangaysaumoiduocthi));
                            }
                            else
                                if (u.getCreated_date() == u.getUpdated_date()
                                        && System.currentTimeMillis() / 1000
                                                - u.getCreated_date() < 259200)
                                {
                                    Utils.ShowToast(
                                            OnlineExamMenuActivity.this,
                                            getResources()
                                                    .getString(
                                                            R.string.bangaysaumoiduocthi));
                                }
                                else
                                {
                                    OnlineExamMenuActivity.this.finish();

                                    Intent intent = new Intent(
                                            OnlineExamMenuActivity.this,
                                            OnlineOptionActivity.class);
                                    intent.putExtra("selectitem", v.getId());
                                    startActivity(intent);
                                }
                        }
                    }
                });

            }

            // View v = new View(this);
            // v.setBackgroundColor(Color.rgb(231, 235, 231));

            // listView.addView(v);
        }

        btnResultOnline = (Button) mainView.findViewById(R.id.btnResultOnline);
        btnResultOnline.setOnClickListener(new OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                // TODO Auto-generated method stub

                // User u = AppManager.getInstance().getUser();

                // if(u.getCreated_date() != u.getUpdated_date() &&
                // System.currentTimeMillis()/1000 - u.getUpdated_date() <
                // 259200) Utils.ShowToast(OnlineExamMenuActivity.this,
                // "Sau ba ngay moi duoc thi");

                // else
                // {
                if (!Utils.IsNetworkAvailable(OnlineExamMenuActivity.this))
                {
                    Utils.ShowToast(OnlineExamMenuActivity.this,
                            R.string.networknot);
                    return;
                }

                new GetOnlineExamDid(OnlineExamMenuActivity.this).execute();
                // }
            }
        });

        // linearLayout0 = (LinearLayout)
        // mainView.findViewById(R.id.linearLayout0);
        linearLayout1 = (LinearLayout) mainView
                .findViewById(R.id.linearLayout1);
        linearLayout3 = (LinearLayout) mainView
                .findViewById(R.id.linearLayout3);
        // layoutblank = (LinearLayout) mainView.findViewById(R.id.layoutblank);
        // layoutfooter = (LinearLayout)
        // mainView.findViewById(R.id.layoutfooter);

        setContentView(mainView);

        Button btnLogout = (Button) this.findViewById(R.id.btnLogout);
        btnLogout.setOnClickListener(new OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                showDialog(DIALOG_EXIT);
            }
        });

        if (!IDefine.HAVE_ADS)
        {
            RelativeLayout v = (RelativeLayout) this.findViewById(R.id.wrapper);

            final int INDEX_FOOTER_VIEW = 2;
            v.removeViewAt(INDEX_FOOTER_VIEW);
        }

    }

    @Override
    public void onResume()
    {
        super.onResume();

        // Resume the AdView.
        mAdView.resume();

        if (timeHideNotify != 0)
        {
            if (timeShowNotify - timeHideNotify > IDefine.SESSION_TIME_OUT)
            {
                if (Utils.IsNetworkAvailable(this))
                {
                    new Logout(this).execute(AppManager.getInstance().getUser()
                            .getUsername());
                }
                // else
                // {

                // this.finish();

                // Intent intent = new Intent(this, MainActivity.class);
                // startActivityBack(intent);
                // }
            }
        }
    }

    @Override
    public void onPause()
    {
        // Pause the AdView.
        mAdView.pause();

        super.onPause();
    }

    @Override
    public void onBackPressed()
    {
        /*
         * if (System.currentTimeMillis() - timeResetbackey > 3000) {
         * backeyNumPress = 0; } if (backeyNumPress == 0) { String text =
         * this.getResources().getString(R.string.backey); Toast.makeText(this,
         * text, Toast.LENGTH_SHORT).show(); backeyNumPress = 1; timeResetbackey
         * = System.currentTimeMillis(); } else if (backeyNumPress == 1) {
         * backeyNumPress = 0; this.finish();
         * 
         * Intent intent = new Intent(this, MainMenuActivity.class);
         * startActivityBack(intent);
         * 
         * }
         */
        currentClassName = "OnlineExamMenuActivity";
        showDialog(DIALOG_EXIT);
    }

    @Override
    public void startActivity(Intent intent)
    {
        super.startActivity(intent);
        this.overridePendingTransition(R.anim.anim_activity_go_to_left,
                R.anim.anim_activity_go_to_right);
    }

    @Override
    public void startActivityBack(Intent intent)
    {
        super.startActivity(intent);
        this.overridePendingTransition(R.anim.anim_activity_go_to_left_back,
                R.anim.anim_activity_go_to_right_back);
    }
}
