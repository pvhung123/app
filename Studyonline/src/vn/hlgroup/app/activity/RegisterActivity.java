package vn.hlgroup.app.activity;

import java.util.ArrayList;
import java.util.List;

import vn.hlgroup.app.IDefine;
import vn.hlgroup.app.R;
import vn.hlgroup.app.adapter.CustomSpinnerAdapter;
import vn.hlgroup.app.adapter.MapItem;
import vn.hlgroup.app.controller.Register;
import vn.hlgroup.app.model.LevelId;
import vn.hlgroup.app.singleton.AppManager;
import vn.hlgroup.app.utils.Utils;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;

public class RegisterActivity extends CoreActivity
{
    private Button register;

    private EditText usernameText;

    private EditText passwordText;

    private EditText passwordTextConfirm;

    private Spinner levelText;

    private CustomSpinnerAdapter levelTextAdap;

    private EditText emailText;

    private EditText phoneText;

    private boolean isEnablePressBackey = true;
    private Button _No, _Yes;

    public static final int DIALOG_EXIT = 1;

    private int backeyNumPress = 0;

    private long timeResetbackey = 0;

    private AdView mAdView;

    private Button btnopenSpin;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        // ads
        LinearLayout layout = (LinearLayout) this
                .findViewById(R.id.adViewLayout);

        // Create a banner ad. The ad size and ad unit ID must be set before
        // calling loadAd.
        mAdView = new AdView(this);
        mAdView.setAdSize(AdSize.SMART_BANNER);
        mAdView.setAdUnitId("ca-app-pub-8820372249061974/8285606845");

        // Create an ad request.
        AdRequest.Builder adRequestBuilder = new AdRequest.Builder();

        // Optionally populate the ad request builder.
        // adRequestBuilder.addTestDevice(AdRequest.DEVICE_ID_EMULATOR);

        // Add the AdView to the view hierarchy.
        layout.addView(mAdView);

        // Start loading the ad.
        mAdView.loadAd(adRequestBuilder.build());

        usernameText = (EditText) this.findViewById(R.id.txtURe);
        passwordText = (EditText) this.findViewById(R.id.txtPRe);
        passwordTextConfirm = (EditText) this.findViewById(R.id.txtPReConfirm);
        levelText = (Spinner) this.findViewById(R.id.spinLevelRe);

        // for knowledge spinner
        List<MapItem> list = new ArrayList<MapItem>();

        ArrayList<LevelId> levelid = AppManager.getInstance().getLevelId();

        int len = levelid.size();

        for (int i = 0; i < len; i++)
        {
            LevelId t = levelid.get(i);
            MapItem mi = new MapItem(t.getId(), t.getName());
            list.add(mi);
        }

        levelTextAdap = new CustomSpinnerAdapter(this,
                R.drawable.custom_spinner_item, list);// SpinnerAdapter don't
                                                      // use
                                                      // R.drawable.custom_spinner_item
        levelTextAdap
                .setDropDownViewResource(R.drawable.custom_spinner_dropdown_item);// SpinnerAdapter
                                                                                  // don't
                                                                                  // use
                                                                                  // R.drawable.custom_spinner_dropdown_item
        levelText.setAdapter(levelTextAdap);

        levelText.setOnItemSelectedListener(new OnItemSelectedListener()
        {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                    int arg2, long arg3)
            {
                RegisterActivity.this.levelTextAdap.getItem(arg2);

                // Toast.makeText(TrainOptionActivity.this,
                // String.valueOf(item.getId()), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0)
            {
                // TODO Auto-generated method stub

            }

        });

        btnopenSpin = (Button) this.findViewById(R.id.btnopenSpin);
        btnopenSpin.setOnClickListener(new OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                // TODO Auto-generated method stub
                levelText.performClick();
            }
        });

        emailText = (EditText) this.findViewById(R.id.txtEmailRe);
        phoneText = (EditText) this.findViewById(R.id.txtPhoneRe);

        register = (Button) this.findViewById(R.id.btnRegisterRe);
        register.setOnClickListener(new OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                // TODO Auto-generated method stub

                String username = usernameText.getText().toString();
                String password = passwordText.getText().toString();
                String passwordConfirm = passwordTextConfirm.getText()
                        .toString();
                String level = String.valueOf(((MapItem) levelText
                        .getSelectedItem()).getId());
                String email = emailText.getText().toString();
                String phone = phoneText.getText().toString();

                Register res = new Register(RegisterActivity.this);

                StringBuilder error = new StringBuilder();

                if (level.equals("-1"))
                {
                    Utils.ShowToast(RegisterActivity.this,
                            R.string.chooselevelnotyet);
                }
                else
                    if (res.validate(username, password, passwordConfirm,
                            level, email, phone, error))
                    {
                        if (!Utils.IsNetworkAvailable(RegisterActivity.this))
                        {
                            Utils.ShowToast(RegisterActivity.this,
                                    R.string.networknot);
                            return;
                        }

                        res.execute(username, password, level, email, phone);
                    }

                    else
                    {
                        Toast.makeText(RegisterActivity.this, error,
                                Toast.LENGTH_SHORT).show();
                    }

            }
        });

        if (!IDefine.HAVE_ADS)
        {
            RelativeLayout v = (RelativeLayout) this.findViewById(R.id.wrapper);

            final int INDEX_FOOTER_VIEW = 2;
            v.removeViewAt(INDEX_FOOTER_VIEW);
        }

    }

    @Override
    public void onBackPressed()
    {
        /*
         * if (System.currentTimeMillis() - timeResetbackey > 3000) {
         * backeyNumPress = 0; } if (backeyNumPress == 0) { String text =
         * this.getResources().getString(R.string.backey);
         * Toast.makeText(RegisterActivity.this, text, Toast.LENGTH_SHORT)
         * .show(); backeyNumPress = 1; timeResetbackey =
         * System.currentTimeMillis(); } else if (backeyNumPress == 1) {
         * backeyNumPress = 0; this.finish();
         * 
         * Intent intent = new Intent(this, MainActivity.class);
         * startActivityBack(intent);
         * 
         * }
         */
        currentClassName = "RegisterActivity";
        showDialog(DIALOG_EXIT);
    }

    public void setPressBackey(boolean i)
    {
        this.isEnablePressBackey = i;
    }

    @Override
    public void startActivity(Intent intent)
    {
        super.startActivity(intent);
        this.overridePendingTransition(R.anim.anim_activity_go_to_left,
                R.anim.anim_activity_go_to_right);
    }

    @Override
    public void startActivityBack(Intent intent)
    {
        super.startActivity(intent);
        this.overridePendingTransition(R.anim.anim_activity_go_to_left_back,
                R.anim.anim_activity_go_to_right_back);
    }

    /*
     * @Override protected Dialog onCreateDialog(int id) { // TODO
     * Auto-generated method stub Dialog dialog = null;
     * 
     * dialog = new Dialog(RegisterActivity.this);
     * dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
     * dialog.setContentView(R.layout.dialog_exit);
     * 
     * _No = (Button) dialog.findViewById(R.id.btnDialogNo); _Yes = (Button)
     * dialog.findViewById(R.id.btnDialogYes);
     * 
     * if (_No != null) { _No.setOnClickListener(new OnClickListener() {
     * 
     * @SuppressWarnings("deprecation")
     * 
     * @Override public void onClick(View v) { // TODO Auto-generated method
     * stub dismissDialog(DIALOG_EXIT); } }); }
     * 
     * if (_Yes != null) { _Yes.setOnClickListener(new OnClickListener() {
     * 
     * @Override public void onClick(View v) { // TODO Auto-generated method
     * stub if (RegisterActivity.this.isEnablePressBackey) {
     * RegisterActivity.this.finish();
     * 
     * Intent intent = new Intent(RegisterActivity.this, MainActivity.class);
     * startActivityBack(intent); } } });
     * 
     * }
     * 
     * return dialog; }
     */
}
