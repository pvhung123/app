package vn.hlgroup.app.activity;

import vn.hlgroup.app.IDefine;
import vn.hlgroup.app.R;
import vn.hlgroup.app.controller.GetImages;
import vn.hlgroup.app.controller.Logout;
import vn.hlgroup.app.controller.PostScoreTrain;
import vn.hlgroup.app.model.ExamDetail;
import vn.hlgroup.app.model.Question;
import vn.hlgroup.app.singleton.AppManager;
import vn.hlgroup.app.utils.Utils;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;

public class TrainningActivity extends CoreActivity
{
    private Button pre;
    private Button next;

    private RadioButton result1;

    private RadioButton result2;

    private RadioButton result3;

    private RadioButton result4;

    private RadioGroup rGroup;

    private Button finish;

    private Button btnShowResult;

    private TextView titleText = null;
    private ImageView titleImg = null;
    private AdView mAdView;

    private int backeyNumPress = 0;

    private long timeResetbackey = 0;

    public ImageView iView;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trainning);

        iView = new ImageView(this);

        AppManager mana = AppManager.getInstance();

        // ads
        LinearLayout layout = (LinearLayout) this
                .findViewById(R.id.adViewLayout);

        // Create a banner ad. The ad size and ad unit ID must be set before
        // calling loadAd.
        mAdView = new AdView(this);
        mAdView.setAdSize(AdSize.SMART_BANNER);
        mAdView.setAdUnitId("ca-app-pub-8820372249061974/8285606845");

        // Create an ad request.
        AdRequest.Builder adRequestBuilder = new AdRequest.Builder();

        // Optionally populate the ad request builder.
        // adRequestBuilder.addTestDevice(AdRequest.DEVICE_ID_EMULATOR);

        // Add the AdView to the view hierarchy.
        layout.addView(mAdView);

        // Start loading the ad.
        mAdView.loadAd(adRequestBuilder.build());

        pre = (Button) this.findViewById(R.id.btnPreTranning);
        pre.setEnabled(false);
        pre.setOnClickListener(new OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                // TODO Auto-generated method stub
                AppManager app = AppManager.getInstance();

                Log.d("abc", app.getQuestions().size() + "");

                clearResult();

                if (app.getQuestionPage() - 1 == 0)
                {
                    TrainningActivity.this.pre.setEnabled(false);
                }

                if (app.getQuestionPage() > 0)
                {
                    TrainningActivity.this.next.setEnabled(true);

                    int questionType = app.getQuestions()
                            .get(app.getQuestionPage() - 1).getIsImage();

                    if (questionType == 0)
                    {
                        addTitleQuestionText();

                        showQuestion(app.getQuestionPage() - 1);
                    }
                    else
                    {
                        addTitleQuestionImage();
                        showQuestionImage(app.getQuestionPage() - 1);
                    }
                }

            }
        });

        next = (Button) this.findViewById(R.id.btnNextTranning);

        next.setOnClickListener(new OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                // TODO Auto-generated method stub

                // TextView textViewTitle =
                // (TextView)TrainningActivity.this.findViewById(R.id.txvtitleTrainning);
                // Log.d("abc1", textViewTitle.getHeight()+"");

                AppManager app = AppManager.getInstance();

                clearResult();

                if (app.getQuestionPage() + 1 == app.getQuestions().size() - 1)
                {
                    TrainningActivity.this.next.setEnabled(false);
                }

                if (app.getQuestionPage() < app.getQuestions().size() - 1)
                {
                    TrainningActivity.this.pre.setEnabled(true);

                    int questionType = app.getQuestions()
                            .get(app.getQuestionPage() + 1).getIsImage();

                    if (questionType == 0)
                    {
                        addTitleQuestionText();

                        showQuestion(app.getQuestionPage() + 1);
                    }
                    else
                    {
                        addTitleQuestionImage();
                        showQuestionImage(app.getQuestionPage() + 1);
                    }

                }

                // Log.d("abc2", textViewTitle.getHeight()+"");
            }
        });

        btnShowResult = (Button) this.findViewById(R.id.btnShowResult);

        btnShowResult.setOnClickListener(new OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                // TODO Auto-generated method stub
                LinearLayout layoutResult = (LinearLayout) TrainningActivity.this
                        .findViewById(R.id.layoutResult);
                layoutResult.removeAllViews();
                layoutResult.setGravity(Gravity.CENTER);
                // LinearLayout.LayoutParams
                // lllp=(LinearLayout.LayoutParams)layoutResult.getLayoutParams();
                // lllp.gravity=Gravity.CENTER;
                // layoutResult.setLayoutParams(lllp);

                Question q = AppManager.getInstance().getQuestions()
                        .get(AppManager.getInstance().getQuestionPage());

                TextView tView = new TextView(TrainningActivity.this);
                tView.setGravity(Gravity.CENTER);
                tView.setTextSize(17);

                tView.setTextColor(getResources().getColor(R.color.SGreen));
                int trueIndex = Integer.parseInt(q.getResult());
                switch (trueIndex)
                {
                    case 1:
                        tView.setText("A");
                        break;
                    case 2:
                        tView.setText("B");
                        break;
                    case 3:
                        tView.setText("C");
                        break;
                    case 4:
                        tView.setText("D");
                        break;
                }

                layoutResult.addView(tView);

                /*
                 * if(q.getIsImage()==1) {
                 * 
                 * layoutResult.addView(iView); } else { TextView tView = new
                 * TextView(TrainningActivity.this); tView.setTextSize(15);
                 * 
                 * int trueIndex = Integer.parseInt(q.getResult());
                 * 
                 * switch(trueIndex) { case ExamDetail.ANSWER_ONE:
                 * tView.setText(q.getResult1()); break; case
                 * ExamDetail.ANSWER_TWO: tView.setText(q.getResult2()); break;
                 * case ExamDetail.ANSWER_THREE: tView.setText(q.getResult3());
                 * break; case ExamDetail.ANSWER_FOUR:
                 * tView.setText(q.getResult4()); break; }
                 * 
                 * layoutResult.addView(tView); }
                 */
            }
        });

        rGroup = (RadioGroup) this.findViewById(R.id.rdoGroupResultsTranning);
        rGroup.clearCheck();

        result1 = (RadioButton) this.findViewById(R.id.radio0Tranning);
        result1.setOnCheckedChangeListener(new OnCheckedChangeListener()
        {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                    boolean isChecked)
            {
                // TODO Auto-generated method stub
                if (isChecked)
                {
                    ExamDetail ex = AppManager.getInstance().getResultBoard()
                            .getExamDetails()
                            .get(AppManager.getInstance().getQuestionPage());

                    Log.d("abc", "1" + ex.getResult());
                    ex.setResult(ExamDetail.ANSWER_ONE);
                }
            }
        });

        result2 = (RadioButton) this.findViewById(R.id.radio1Tranning);
        result2.setOnCheckedChangeListener(new OnCheckedChangeListener()
        {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                    boolean isChecked)
            {
                // TODO Auto-generated method stub
                if (isChecked)
                {
                    ExamDetail ex = AppManager.getInstance().getResultBoard()
                            .getExamDetails()
                            .get(AppManager.getInstance().getQuestionPage());

                    Log.d("abc", "2" + ex.getResult());
                    ex.setResult(ExamDetail.ANSWER_TWO);
                }
            }
        });

        result3 = (RadioButton) this.findViewById(R.id.radio2Tranning);
        result3.setOnCheckedChangeListener(new OnCheckedChangeListener()
        {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                    boolean isChecked)
            {
                // TODO Auto-generated method stub
                if (isChecked)
                {
                    ExamDetail ex = AppManager.getInstance().getResultBoard()
                            .getExamDetails()
                            .get(AppManager.getInstance().getQuestionPage());

                    Log.d("abc", "3" + ex.getResult());
                    ex.setResult(ExamDetail.ANSWER_THREE);
                }
            }
        });

        result4 = (RadioButton) this.findViewById(R.id.radio3Tranning);
        result4.setOnCheckedChangeListener(new OnCheckedChangeListener()
        {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                    boolean isChecked)
            {
                // TODO Auto-generated method stub
                if (isChecked)
                {
                    ExamDetail ex = AppManager.getInstance().getResultBoard()
                            .getExamDetails()
                            .get(AppManager.getInstance().getQuestionPage());

                    Log.d("abc", "4" + ex.getResult());
                    ex.setResult(ExamDetail.ANSWER_FOUR);
                }

            }
        });

        finish = (Button) this.findViewById(R.id.btnFinishTranning);
        finish.setOnClickListener(new OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                // TODO Auto-generated method stub

                AppManager.getInstance().getResultBoard().caculateTotalScore();

                if (!Utils.IsNetworkAvailable(TrainningActivity.this))
                {
                    Utils.ShowToast(TrainningActivity.this, R.string.networknot);
                    return;
                }

                new PostScoreTrain(TrainningActivity.this).execute();

                // TrainningActivity.this.finish();

                // Intent intent = new Intent(TrainningActivity.this,
                // ResultActivity.class);
                // TrainningActivity.this.startActivity(intent);
            }
        });

        mana.setTimeRemain(0);

        int questionType = mana.getQuestions().get(0).getIsImage();
        if (questionType == 0)
        {
            addTitleQuestionText();
            showQuestion(0);
        }
        else
        {
            addTitleQuestionImage();
            showQuestionImage(0);
        }

        TableLayout tableQuestions = (TableLayout) this
                .findViewById(R.id.tableQuestions);

        int numberOfQuestions = mana.getQuestions().size();
        int row = numberOfQuestions / 6;
        int exQuestion = numberOfQuestions % 6;

        for (int i = 0; i < row; i++)
        {
            TableRow tr = (TableRow) View.inflate(this,
                    R.drawable.table_row_train, null);
            for (int j = 0; j < 6; j++)
            {
                LinearLayout bl = (LinearLayout) View.inflate(this,
                        R.drawable.table_row_button_train, null);
                Button b = (Button) bl.getChildAt(0);
                b.setText(String.valueOf(i * 6 + j + 1));
                b.setOnClickListener(new OnClickListener()
                {

                    @Override
                    public void onClick(View v)
                    {
                        // TODO Auto-generated method stub
                        AppManager app = AppManager.getInstance();

                        clearResult();

                        String label = ((Button) v).getText().toString();
                        int value = Integer.parseInt(label) - 1;

                        int questionType = app.getQuestions().get(value)
                                .getIsImage();

                        if (questionType == 0)
                        {
                            addTitleQuestionText();

                            showQuestion(value);
                        }
                        else
                        {
                            addTitleQuestionImage();
                            showQuestionImage(value);
                        }

                        disableEnableButtonPreNext(value == AppManager
                                .getInstance().getQuestions().size() - 1 ? -1
                                : value);
                    }
                });

                tr.addView(bl);
            }
            tableQuestions.addView(tr);
        }

        if (exQuestion > 0)
        {
            TableRow tr = (TableRow) View.inflate(this,
                    R.drawable.table_row_train, null);
            for (int j = 0; j < exQuestion; j++)
            {
                LinearLayout bl = (LinearLayout) View.inflate(this,
                        R.drawable.table_row_button_train, null);
                Button b = (Button) bl.getChildAt(0);
                b.setText(String.valueOf(row * 6 + j + 1));
                b.setOnClickListener(new OnClickListener()
                {

                    @Override
                    public void onClick(View v)
                    {
                        // TODO Auto-generated method stub
                        AppManager app = AppManager.getInstance();

                        String label = ((Button) v).getText().toString();
                        int value = Integer.parseInt(label) - 1;

                        int questionType = app.getQuestions().get(value)
                                .getIsImage();

                        if (questionType == 0)
                        {
                            addTitleQuestionText();

                            showQuestion(value);
                        }
                        else
                        {
                            addTitleQuestionImage();
                            showQuestionImage(value);
                        }

                        disableEnableButtonPreNext(value == AppManager
                                .getInstance().getQuestions().size() - 1 ? -1
                                : value);
                    }
                });
                tr.addView(bl);
            }
            tableQuestions.addView(tr);
        }

        Button btnLogout = (Button) this.findViewById(R.id.btnLogout);
        btnLogout.setOnClickListener(new OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                showDialog(DIALOG_EXIT);
            }
        });

        if (!IDefine.HAVE_ADS)
        {
            RelativeLayout v = (RelativeLayout) this.findViewById(R.id.wrapper);

            final int INDEX_FOOTER_VIEW = 2;
            v.removeViewAt(INDEX_FOOTER_VIEW);
        }
    }

    @Override
    public void onBackPressed()
    {

        /*
         * if (System.currentTimeMillis() - timeResetbackey > 3000) {
         * backeyNumPress = 0; } if (backeyNumPress == 0) { String text =
         * this.getResources().getString(R.string.backey); Toast.makeText(this,
         * text, Toast.LENGTH_SHORT).show(); backeyNumPress = 1; timeResetbackey
         * = System.currentTimeMillis(); } else if (backeyNumPress == 1) {
         * backeyNumPress = 0; this.finish();
         * 
         * Intent intent = new Intent(this, MainMenuActivity.class);
         * startActivityBack(intent);
         * 
         * }
         */

        currentClassName = "TrainningActivity";
        showDialog(DIALOG_EXIT);
    }

    @Override
    public void onResume()
    {
        super.onResume();

        // Resume the AdView.
        mAdView.resume();

        if (timeHideNotify != 0)
        {
            if (timeShowNotify - timeHideNotify > IDefine.SESSION_TIME_OUT)
            {
                if (Utils.IsNetworkAvailable(this))
                {
                    new Logout(this).execute(AppManager.getInstance().getUser()
                            .getUsername());
                }
                // else
                // {

                // this.finish();

                // Intent intent = new Intent(this, MainActivity.class);
                // startActivityBack(intent);
                // }
            }
        }
    }

    @Override
    public void onPause()
    {
        // Pause the AdView.
        mAdView.pause();

        super.onPause();
    }

    private void showQuestion(int index)
    {
        AppManager mana = AppManager.getInstance();
        mana.setQuestionPage(index);

        Question q = mana.getQuestions().get(index);

        TextView textViewTitle = (TextView) this
                .findViewById(R.id.txvtitleTrainning);
        textViewTitle.setText(q.getTitle());

        String questionstr = this.getResources().getString(
                R.string.training_question_titile);

        TextView textViewCau = (TextView) this.findViewById(R.id.txvCau);
        textViewCau.setText(questionstr + ": " + (index + 1) + "/"
                + mana.getQuestions().size());

        result1.setText(q.getResult1());
        result1.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);

        result2.setText(q.getResult2());
        result2.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);

        result3.setText(q.getResult3());
        result3.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);

        result4.setText(q.getResult4());
        result4.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);

        if (q.getResult1() == null || q.getResult1().equals(""))
        {
            result1.setVisibility(View.INVISIBLE);
        }
        else
        {
            result1.setVisibility(View.VISIBLE);
        }
        if (q.getResult2() == null || q.getResult2().equals(""))
        {
            result2.setVisibility(View.INVISIBLE);
        }
        else
        {
            result2.setVisibility(View.VISIBLE);
        }
        if (q.getResult3() == null || q.getResult3().equals(""))
        {
            result3.setVisibility(View.INVISIBLE);
        }
        else
        {
            result3.setVisibility(View.VISIBLE);
        }
        if (q.getResult4() == null || q.getResult4().equals(""))
        {
            result4.setVisibility(View.INVISIBLE);
        }
        else
        {
            result4.setVisibility(View.VISIBLE);
        }
        // result1.setChecked(false);
        // result2.setChecked(false);
        // result3.setChecked(false);
        // result4.setChecked(false);

        // bind result
        ExamDetail exam = mana.getResultBoard().getExamDetails().get(index);
        int choose = exam.getResult();
        Log.w("abc", choose + "");
        switch (choose)
        {
            case ExamDetail.ANSWER_ONE:
                result1.setChecked(true);
                break;
            case ExamDetail.ANSWER_TWO:
                result2.setChecked(true);
                break;
            case ExamDetail.ANSWER_THREE:
                result3.setChecked(true);
                break;
            case ExamDetail.ANSWER_FOUR:
                result4.setChecked(true);
                break;
            default:
                rGroup.clearCheck();
                break;
        }
    }

    private void showQuestionImage(int index)
    {
        AppManager mana = AppManager.getInstance();
        mana.setQuestionPage(index);

        Question q = mana.getQuestions().get(index);

        // ImageView imgViewTitle =
        // (ImageView)this.findViewById(R.id.imgQuestionTitle);
        // imgViewTitle.setImageResource(R.drawable.q_1_1_1_1_1_1);

        // imgViewTitle.setImageBitmap(Utils.getImageBitmap("http://app.camemis.vn/upload/1_1_1_1_1/q_1.png"));

        String questionstr = this.getResources().getString(
                R.string.training_question_titile);

        TextView textViewCau = (TextView) this.findViewById(R.id.txvCau);
        textViewCau.setText(questionstr + ": " + (index + 1) + "/"
                + mana.getQuestions().size());

        result1.setText(null);
        // Drawable animg =
        // Utils.drawableFromUrl("http://app.camemis.vn/upload/A1_1_1_1_1_1_1.png");
        // result1.setCompoundDrawablesWithIntrinsicBounds(null, null, null,
        // animg);

        result2.setText(null);
        // Drawable animg1 =
        // Utils.drawableFromUrl("http://app.camemis.vn/upload/A1_1_1_1_1_1_1.png");
        // result2.setCompoundDrawablesWithIntrinsicBounds(null, null, null,
        // animg1);

        result3.setText(null);
        // Drawable animg2 =
        // Utils.drawableFromUrl("http://app.camemis.vn/upload/A1_1_1_1_1_1_1.png");
        // result3.setCompoundDrawablesWithIntrinsicBounds(null, null, null,
        // animg2);

        result4.setText(null);
        // Drawable animg3 =
        // Utils.drawableFromUrl("http://app.camemis.vn/upload/A1_1_1_1_1_1_1.png");
        // result4.setCompoundDrawablesWithIntrinsicBounds(null, null, null,
        // animg3);

        if (q.getResult1() == null || q.getResult1().equals(""))
        {
            result1.setVisibility(View.INVISIBLE);
        }
        else
        {
            result1.setVisibility(View.VISIBLE);
        }
        if (q.getResult2() == null || q.getResult2().equals(""))
        {
            result2.setVisibility(View.INVISIBLE);
        }
        else
        {
            result2.setVisibility(View.VISIBLE);
        }
        if (q.getResult3() == null || q.getResult3().equals(""))
        {
            result3.setVisibility(View.INVISIBLE);
        }
        else
        {
            result3.setVisibility(View.VISIBLE);
        }
        if (q.getResult4() == null || q.getResult4().equals(""))
        {
            result4.setVisibility(View.INVISIBLE);
        }
        else
        {
            result4.setVisibility(View.VISIBLE);
        }

        new GetImages(this).execute(
                IDefine.HEADER_LINK_UPLOAD + "/upload/" + q.getTitle(),
                IDefine.HEADER_LINK_UPLOAD + "/upload/" + q.getResult1(),
                IDefine.HEADER_LINK_UPLOAD + "/upload/" + q.getResult2(),
                IDefine.HEADER_LINK_UPLOAD + "/upload/" + q.getResult3(),
                IDefine.HEADER_LINK_UPLOAD + "/upload/" + q.getResult4());

        // bind result
        ExamDetail exam = mana.getResultBoard().getExamDetails().get(index);
        int choose = exam.getResult();
        switch (choose)
        {
            case ExamDetail.ANSWER_ONE:
                result1.setChecked(true);
                break;
            case ExamDetail.ANSWER_TWO:
                result2.setChecked(true);
                break;
            case ExamDetail.ANSWER_THREE:
                result3.setChecked(true);
                break;
            case ExamDetail.ANSWER_FOUR:
                result4.setChecked(true);
                break;
            default:
                rGroup.clearCheck();
                break;
        }
    }

    @Override
    public void startActivity(Intent intent)
    {
        super.startActivity(intent);
        this.overridePendingTransition(R.anim.anim_activity_go_to_left,
                R.anim.anim_activity_go_to_right);
    }

    @Override
    public void startActivityBack(Intent intent)
    {
        super.startActivity(intent);
        this.overridePendingTransition(R.anim.anim_activity_go_to_left_back,
                R.anim.anim_activity_go_to_right_back);
    }

    private void disableEnableButtonPreNext(int index)
    {
        final int FIRST_QUESTION = 0;
        // final int MID_QUESTION = 1;
        final int LAST_QUESTION = -1;

        switch (index)
        {
            case FIRST_QUESTION:
                TrainningActivity.this.pre.setEnabled(false);
                TrainningActivity.this.next.setEnabled(true);
                break;
            case LAST_QUESTION:
                TrainningActivity.this.pre.setEnabled(true);
                TrainningActivity.this.next.setEnabled(false);
                break;
            default:
                TrainningActivity.this.pre.setEnabled(true);
                TrainningActivity.this.next.setEnabled(true);
                break;
        }
    }

    private void addTitleQuestionText()
    {
        if (titleText != null)
        {
            ((LinearLayout) (titleText.getParent())).removeViewAt(0);
            titleText = null;
        }

        if (titleImg != null)
        {
            ((LinearLayout) (titleImg.getParent())).removeViewAt(0);
            titleImg = null;
        }

        titleText = (TextView) View.inflate(this,
                R.drawable.txv_question_title_train, null);

        LinearLayout titleLayout = (LinearLayout) this
                .findViewById(R.id.txvtitlelayoutTrainning);
        titleLayout.addView(titleText);
    }

    private void addTitleQuestionImage()
    {
        if (titleText != null)
        {
            ((LinearLayout) (titleText.getParent())).removeViewAt(0);
            titleText = null;
        }

        if (titleImg != null)
        {
            ((LinearLayout) (titleImg.getParent())).removeViewAt(0);
            titleImg = null;
        }

        titleImg = (ImageView) View.inflate(this,
                R.drawable.img_question_title_train, null);

        LinearLayout titleLayout = (LinearLayout) this
                .findViewById(R.id.txvtitlelayoutTrainning);
        titleLayout.addView(titleImg);
    }

    private void clearResult()
    {
        LinearLayout layoutResult = (LinearLayout) TrainningActivity.this
                .findViewById(R.id.layoutResult);
        layoutResult.removeAllViews();
    }
}
