package vn.hlgroup.app.activity;

import vn.hlgroup.app.IDefine;
import vn.hlgroup.app.R;
import vn.hlgroup.app.singleton.AppManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;

public class TutorialsActivity extends CoreActivity
{

    private Button tutRegister;
    private Button tutChooseLevel;
    private Button tutChooseSolution;

    private Button _No, _Yes;

    public static final int DIALOG_EXIT = 1;

    private int backeyNumPress = 0;

    private long timeResetbackey = 0;
    private AdView mAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorials);

        // ads
        LinearLayout layout = (LinearLayout) this
                .findViewById(R.id.adViewLayout);

        // Create a banner ad. The ad size and ad unit ID must be set before
        // calling loadAd.
        mAdView = new AdView(this);
        mAdView.setAdSize(AdSize.SMART_BANNER);
        mAdView.setAdUnitId(IDefine.ADMOB_ID);

        // Create an ad request.
        AdRequest.Builder adRequestBuilder = new AdRequest.Builder();

        // Optionally populate the ad request builder.
        // adRequestBuilder.addTestDevice(AdRequest.DEVICE_ID_EMULATOR);

        // Add the AdView to the view hierarchy.
        layout.addView(mAdView);

        // Start loading the ad.
        mAdView.loadAd(adRequestBuilder.build());

        tutRegister = (Button) this.findViewById(R.id.btnTutRegister);
        tutRegister.setOnClickListener(new OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                // TODO Auto-generated method stub
                AppManager.getInstance().getResultBoard().caculateTotalScore();

                TutorialsActivity.this.finish();

                Intent intent = new Intent(TutorialsActivity.this,
                        TutRegisterActivity.class);
                TutorialsActivity.this.startActivity(intent);
            }
        });

        tutChooseLevel = (Button) this.findViewById(R.id.btnTutChooselevel);
        tutChooseLevel.setOnClickListener(new OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                // TODO Auto-generated method stub
                AppManager.getInstance().getResultBoard().caculateTotalScore();

                TutorialsActivity.this.finish();

                Intent intent = new Intent(TutorialsActivity.this,
                        TutChooseLevelActivity.class);
                TutorialsActivity.this.startActivity(intent);
            }
        });

        tutChooseSolution = (Button) this
                .findViewById(R.id.btnTutChooseSolution);
        tutChooseSolution.setOnClickListener(new OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                // TODO Auto-generated method stub
                AppManager.getInstance().getResultBoard().caculateTotalScore();

                TutorialsActivity.this.finish();

                Intent intent = new Intent(TutorialsActivity.this,
                        TutChooseSolutionActivity.class);
                TutorialsActivity.this.startActivity(intent);
            }
        });

        if (!IDefine.HAVE_ADS)
        {
            RelativeLayout v = (RelativeLayout) this.findViewById(R.id.wrapper);

            final int INDEX_FOOTER_VIEW = 2;
            v.removeViewAt(INDEX_FOOTER_VIEW);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.tutorials, menu);
        return true;
    }

    @Override
    public void onBackPressed()
    {

        /*
         * if (System.currentTimeMillis() - timeResetbackey > 3000) {
         * backeyNumPress = 0; } if (backeyNumPress == 0) { String text =
         * this.getResources().getString(R.string.backey);
         * Toast.makeText(TutorialsActivity.this, text, Toast.LENGTH_SHORT)
         * .show(); backeyNumPress = 1; timeResetbackey =
         * System.currentTimeMillis(); } else if (backeyNumPress == 1) {
         * backeyNumPress = 0; this.finish();
         * 
         * Intent intent = new Intent(this, MainActivity.class);
         * startActivityBack(intent);
         * 
         * }
         */

        currentClassName = "TutorialsActivity";
        showDialog(DIALOG_EXIT);
    }

    @Override
    public void startActivity(Intent intent)
    {
        super.startActivity(intent);
        this.overridePendingTransition(R.anim.anim_activity_go_to_left,
                R.anim.anim_activity_go_to_right);
    }

    @Override
    public void startActivityBack(Intent intent)
    {
        super.startActivity(intent);
        this.overridePendingTransition(R.anim.anim_activity_go_to_left_back,
                R.anim.anim_activity_go_to_right_back);
    }

    /*
     * @Override protected Dialog onCreateDialog(int id) { // TODO
     * Auto-generated method stub Dialog dialog = null;
     * 
     * dialog = new Dialog(TutorialsActivity.this);
     * dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
     * dialog.setContentView(R.layout.dialog_exit);
     * 
     * _No = (Button) dialog.findViewById(R.id.btnDialogNo); _Yes = (Button)
     * dialog.findViewById(R.id.btnDialogYes);
     * 
     * if (_No != null) { _No.setOnClickListener(new OnClickListener() {
     * 
     * @SuppressWarnings("deprecation")
     * 
     * @Override public void onClick(View v) { // TODO Auto-generated method
     * stub dismissDialog(DIALOG_EXIT); } }); }
     * 
     * if (_Yes != null) { _Yes.setOnClickListener(new OnClickListener() {
     * 
     * @Override public void onClick(View v) { // TODO Auto-generated method
     * stub // if (RegisterActivity.this.isEnablePressBackey) {
     * TutorialsActivity.this.finish();
     * 
     * Intent intent = new Intent(TutorialsActivity.this,
     * MainMenuActivity.class); startActivityBack(intent); } } });
     * 
     * }
     * 
     * return dialog; }
     */

}
