package vn.hlgroup.app.activity;

import java.util.ArrayList;

import vn.hlgroup.app.IDefine;
import vn.hlgroup.app.R;
import vn.hlgroup.app.controller.Logout;
import vn.hlgroup.app.model.HighScore;
import vn.hlgroup.app.singleton.AppManager;
import vn.hlgroup.app.utils.Utils;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;

public class AvgScoreOnlineActivity extends CoreActivity
{// Thi Online

    private int backeyNumPress = 0;

    private long timeResetbackey = 0;
    private AdView mAdView;

    private LinearLayout layoutlisthighmainexam;
    // private LinearLayout layoutblank;
    private LinearLayout linearLayout1;
    private LinearLayout adViewLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        // setContentView(R.layout.activity_avg_score);
        View mainView = getLayoutInflater().inflate(
                R.layout.activity_avg_score_online, null);

        AppManager app = AppManager.getInstance();

        // ads
        adViewLayout = (LinearLayout) mainView.findViewById(R.id.adViewLayout);

        // Create a banner ad. The ad size and ad unit ID must be set before
        // calling loadAd.
        mAdView = new AdView(this);
        mAdView.setAdSize(AdSize.SMART_BANNER);
        mAdView.setAdUnitId(IDefine.ADMOB_ID);

        // Create an ad request.
        AdRequest.Builder adRequestBuilder = new AdRequest.Builder();

        // Optionally populate the ad request builder.
        // adRequestBuilder.addTestDevice(AdRequest.DEVICE_ID_EMULATOR);

        // Add the AdView to the view hierarchy.
        adViewLayout.addView(mAdView);

        // Start loading the ad.
        mAdView.loadAd(adRequestBuilder.build());

        layoutlisthighmainexam = (LinearLayout) mainView
                .findViewById(R.id.layoutlisthighmainexam);

        ArrayList<HighScore> highscoremainexam = app.getHighScoreonline();

        for (int i = 0; i < highscoremainexam.size(); i++)
        {
            // TextView t = (TextView) View.inflate(this,
            // R.drawable.custom_spinner_item, null);
            // t.setText(highscoremainexam.get(i).getUsername()+": "+highscoremainexam.get(i).getScore());
            // t.setTextSize(15);
            // layoutlisthighmainexam.addView(t);

            LinearLayout layoutChild = (LinearLayout) View.inflate(this,
                    R.drawable.template_layout_list_item, null);
            TextView t = (TextView) ((LinearLayout) layoutChild.getChildAt(0))
                    .getChildAt(0);
            t.setText((i + 1) + ". " + highscoremainexam.get(i).getUsername());

            TextView t1 = (TextView) ((LinearLayout) layoutChild.getChildAt(0))
                    .getChildAt(1);
            t1.setText(Utils.roundTwoDigit(highscoremainexam.get(i).getScore()));

            if (i == 0)
            {
                t.setTextColor(getResources().getColor(R.color.SLightRed));
                t1.setTextColor(getResources().getColor(R.color.SLightRed));
            }
            else
                if (i == 1)
                {
                    t.setTextColor(getResources().getColor(R.color.SOrange));
                    t1.setTextColor(getResources().getColor(R.color.SOrange));
                }
                else
                    if (i == 2)
                    {
                        t.setTextColor(getResources().getColor(R.color.SGreen));
                        t1.setTextColor(getResources().getColor(R.color.SGreen));
                    }
            layoutlisthighmainexam.addView(layoutChild);

            if (i == highscoremainexam.size() - 1)
            {
                layoutChild.removeViewAt(1);
            }
        }

        /*
         * layoutlisthighonline = (LinearLayout)
         * mainView.findViewById(R.id.layoutlisthighonline);
         * 
         * ArrayList<HighScore> listhighonline = app.getHighScoreonline();
         * 
         * for(int i=0;i<listhighonline.size();i++) { TextView t = (TextView)
         * View.inflate(this, R.drawable.custom_spinner_item, null);
         * t.setText(listhighonline
         * .get(i).getUsername()+": "+listhighonline.get(i).getScore());
         * 
         * layoutlisthighonline.addView(t); }
         */

        // linearLayout0 = (LinearLayout)
        // mainView.findViewById(R.id.linearLayout0);
        linearLayout1 = (LinearLayout) mainView
                .findViewById(R.id.linearLayout1);
        // layoutblank = (LinearLayout) mainView.findViewById(R.id.layoutblank);
        // layoutfooter = (LinearLayout)
        // mainView.findViewById(R.id.layoutfooter);

        setContentView(mainView);

        Button btnLogout = (Button) this.findViewById(R.id.btnLogout);
        btnLogout.setOnClickListener(new OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                showDialog(DIALOG_EXIT);
            }
        });

        if (!IDefine.HAVE_ADS)
        {
            RelativeLayout v = (RelativeLayout) this.findViewById(R.id.wrapper);

            final int INDEX_FOOTER_VIEW = 2;
            v.removeViewAt(INDEX_FOOTER_VIEW);
        }

    }

    @Override
    public void onResume()
    {
        super.onResume();

        // Resume the AdView.
        mAdView.resume();

        if (timeHideNotify != 0)
        {
            if (timeShowNotify - timeHideNotify > IDefine.SESSION_TIME_OUT)
            {
                if (Utils.IsNetworkAvailable(this))
                {
                    new Logout(this).execute(AppManager.getInstance().getUser()
                            .getUsername());
                }
                // else
                // {

                // this.finish();

                // Intent intent = new Intent(this, MainActivity.class);
                // startActivityBack(intent);
                // }
            }
        }
    }

    @Override
    public void onPause()
    {
        // Pause the AdView.
        mAdView.pause();

        super.onPause();
    }

    @Override
    public void onBackPressed()
    {
        /*
         * if (System.currentTimeMillis() - timeResetbackey > 3000) {
         * backeyNumPress = 0; } if (backeyNumPress == 0) { String text =
         * this.getResources().getString(R.string.backey);
         * Toast.makeText(AvgScoreOnlineActivity.this, text,
         * Toast.LENGTH_SHORT).show(); backeyNumPress = 1; timeResetbackey =
         * System.currentTimeMillis(); } else if (backeyNumPress == 1) {
         * backeyNumPress = 0; this.finish();
         * 
         * Intent intent = new Intent(this, RankActivity.class);
         * startActivityBack(intent);
         * 
         * }
         */
        currentClassName = "AvgScoreOnlineActivity";
        showDialog(DIALOG_EXIT);
    }

    @Override
    public void startActivity(Intent intent)
    {
        super.startActivity(intent);
        this.overridePendingTransition(R.anim.anim_activity_go_to_left,
                R.anim.anim_activity_go_to_right);
    }

    @Override
    public void startActivityBack(Intent intent)
    {
        super.startActivity(intent);
        this.overridePendingTransition(R.anim.anim_activity_go_to_left_back,
                R.anim.anim_activity_go_to_right_back);
    }
}
