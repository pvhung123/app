package vn.hlgroup.app.activity;

import vn.hlgroup.app.IDefine;
import vn.hlgroup.app.R;
import vn.hlgroup.app.controller.Logout;
import vn.hlgroup.app.singleton.AppManager;
import vn.hlgroup.app.utils.Utils;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;

public class InfoActivity extends CoreActivity
{

    TextView txvUsername;

    TextView txvLevel;

    TextView txvEmail;

    TextView txvPhone;

    Button btnSendOp;

    Button btnUpdateInfo;

    private Button _No, _Yes;

    public static final int DIALOG_EXIT = 1;

    private int backeyNumPress = 0;

    private long timeResetbackey = 0;
    private AdView mAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);

        AppManager mana = AppManager.getInstance();

        // ads
        LinearLayout layout = (LinearLayout) this
                .findViewById(R.id.adViewLayout);

        // Create a banner ad. The ad size and ad unit ID must be set before
        // calling loadAd.
        mAdView = new AdView(this);
        mAdView.setAdSize(AdSize.SMART_BANNER);
        mAdView.setAdUnitId(IDefine.ADMOB_ID);

        // Create an ad request.
        AdRequest.Builder adRequestBuilder = new AdRequest.Builder();

        // Optionally populate the ad request builder.
        // adRequestBuilder.addTestDevice(AdRequest.DEVICE_ID_EMULATOR);

        // Add the AdView to the view hierarchy.
        layout.addView(mAdView);

        // Start loading the ad.
        mAdView.loadAd(adRequestBuilder.build());

        txvUsername = (TextView) this.findViewById(R.id.txvUserInfo);
        txvUsername.setText(mana.getUser().getUsername());
        txvLevel = (TextView) this.findViewById(R.id.txvLevelInfo);
        // txvLevel.setText(String.valueOf(mana.getUser().get));

        for (int i = 0; i < mana.getLevelId().size(); i++)
        {
            int id = mana.getLevelId().get(i).getId();
            Log.d("abc", "qqqqqq " + id);
            if (id == mana.getUser().getLevel_id())
            {
                Log.d("abc", "eeeeee " + id);
                txvLevel.setText(mana.getLevelId().get(i).getName());
                break;
            }
        }

        txvEmail = (TextView) this.findViewById(R.id.txvEmailInfo);
        txvEmail.setText(mana.getUser().getEmail());
        txvPhone = (TextView) this.findViewById(R.id.txvPhoneInfo);
        txvPhone.setText(mana.getUser().getPhone());

        btnSendOp = (Button) this.findViewById(R.id.btnSendOpinion);
        btnSendOp.setOnClickListener(new OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                // TODO Auto-generated method stub
                InfoActivity.this.finish();

                Intent intent = new Intent(InfoActivity.this,
                        SendOpinionActivity.class);
                startActivity(intent);
            }
        });

        btnUpdateInfo = (Button) this.findViewById(R.id.btnUpdateInfor);
        btnUpdateInfo.setOnClickListener(new OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                // TODO Auto-generated method stub
                InfoActivity.this.finish();

                Intent intent = new Intent(InfoActivity.this,
                        UpdateInfoActivity.class);
                startActivity(intent);
            }
        });

        Button btnLogout = (Button) this.findViewById(R.id.btnLogout);
        btnLogout.setOnClickListener(new OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                showDialog(DIALOG_EXIT);
            }
        });

        if (!IDefine.HAVE_ADS)
        {
            RelativeLayout v = (RelativeLayout) this.findViewById(R.id.wrapper);

            final int INDEX_FOOTER_VIEW = 2;
            v.removeViewAt(INDEX_FOOTER_VIEW);
        }
    }

    @Override
    public void onResume()
    {
        super.onResume();

        // Resume the AdView.
        mAdView.resume();

        if (timeHideNotify != 0)
        {
            if (timeShowNotify - timeHideNotify > IDefine.SESSION_TIME_OUT)
            {
                if (Utils.IsNetworkAvailable(this))
                {
                    new Logout(this).execute(AppManager.getInstance().getUser()
                            .getUsername());
                }
                // else
                // {

                // this.finish();

                // Intent intent = new Intent(this, MainActivity.class);
                // startActivityBack(intent);
                // }
            }
        }
    }

    @Override
    public void onPause()
    {
        // Pause the AdView.
        mAdView.pause();

        super.onPause();
    }

    @Override
    public void onBackPressed()
    {

        /*
         * if (System.currentTimeMillis() - timeResetbackey > 3000) {
         * backeyNumPress = 0; } if (backeyNumPress == 0) { String text =
         * this.getResources().getString(R.string.backey);
         * Toast.makeText(InfoActivity.this, text, Toast.LENGTH_SHORT).show();
         * backeyNumPress = 1; timeResetbackey = System.currentTimeMillis(); }
         * else if (backeyNumPress == 1) { backeyNumPress = 0; this.finish();
         * 
         * Intent intent = new Intent(this, MainMenuActivity.class);
         * startActivityBack(intent);
         * 
         * }
         */
        currentClassName = "InfoActivity";
        showDialog(DIALOG_EXIT);
    }

    @Override
    public void startActivity(Intent intent)
    {
        super.startActivity(intent);
        this.overridePendingTransition(R.anim.anim_activity_go_to_left,
                R.anim.anim_activity_go_to_right);
    }

    @Override
    public void startActivityBack(Intent intent)
    {
        super.startActivity(intent);
        this.overridePendingTransition(R.anim.anim_activity_go_to_left_back,
                R.anim.anim_activity_go_to_right_back);
    }

    /*
     * @Override protected Dialog onCreateDialog(int id) { // TODO
     * Auto-generated method stub Dialog dialog = null;
     * 
     * dialog = new Dialog(InfoActivity.this);
     * dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
     * dialog.setContentView(R.layout.dialog_exit);
     * 
     * _No = (Button) dialog.findViewById(R.id.btnDialogNo); _Yes = (Button)
     * dialog.findViewById(R.id.btnDialogYes);
     * 
     * if (_No != null) { _No.setOnClickListener(new OnClickListener() {
     * 
     * @SuppressWarnings("deprecation")
     * 
     * @Override public void onClick(View v) { // TODO Auto-generated method
     * stub dismissDialog(DIALOG_EXIT); } }); }
     * 
     * if (_Yes != null) { _Yes.setOnClickListener(new OnClickListener() {
     * 
     * @Override public void onClick(View v) { // TODO Auto-generated method
     * stub // if (InfoActivity.this.isEnablePressBackey) {
     * InfoActivity.this.finish();
     * 
     * Intent intent = new Intent(InfoActivity.this, MainMenuActivity.class);
     * startActivityBack(intent); } } });
     * 
     * }
     * 
     * return dialog; }
     */
}
