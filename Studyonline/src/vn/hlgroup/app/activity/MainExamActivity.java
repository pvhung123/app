package vn.hlgroup.app.activity;

import vn.hlgroup.app.IDefine;
import vn.hlgroup.app.R;
import vn.hlgroup.app.controller.GetImages;
import vn.hlgroup.app.controller.Logout;
import vn.hlgroup.app.controller.PostScoreMainExam;
import vn.hlgroup.app.model.ExamDetail;
import vn.hlgroup.app.model.Question;
import vn.hlgroup.app.singleton.AppManager;
import vn.hlgroup.app.utils.Utils;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;

public class MainExamActivity extends CoreActivity implements IDefine
{

    private Button pre;
    private Button next;

    private RadioButton result1;

    private RadioButton result2;

    private RadioButton result3;

    private RadioButton result4;
    private RadioGroup rGroup;

    private Button finish;
    private TextView time;

    private long remainTime = -1;

    private TextView titleText = null;
    private ImageView titleImg = null;
    private AdView mAdView;

    private long timeResetbackey = 0;

    private int backeyNumPress = 0;

    protected Handler handler = new Handler()
    {
        @Override
        public void handleMessage(Message msg)
        {
            int rt = msg.what;

            long s = (TEST_TIME_DEFAULT - rt);
            if (s < 0)
            {
                s = 0;
            }
            AppManager.getInstance().setTimeRemain(s);
            time.setText(String.valueOf(Utils.convertToTime(s)));
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_exam);

        AppManager mana = AppManager.getInstance();

        // ads
        LinearLayout layout = (LinearLayout) this
                .findViewById(R.id.adViewLayout);

        // Create a banner ad. The ad size and ad unit ID must be set before
        // calling loadAd.
        mAdView = new AdView(this);
        mAdView.setAdSize(AdSize.SMART_BANNER);
        mAdView.setAdUnitId(IDefine.ADMOB_ID);

        // Create an ad request.
        AdRequest.Builder adRequestBuilder = new AdRequest.Builder();

        // Optionally populate the ad request builder.
        // adRequestBuilder.addTestDevice(AdRequest.DEVICE_ID_EMULATOR);

        // Add the AdView to the view hierarchy.
        layout.addView(mAdView);

        // Start loading the ad.
        mAdView.loadAd(adRequestBuilder.build());

        pre = (Button) this.findViewById(R.id.btnPreMainExam);
        pre.setEnabled(false);
        pre.setOnClickListener(new OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                // TODO Auto-generated method stub
                AppManager app = AppManager.getInstance();

                Log.d("abc", app.getQuestions().size() + "");

                if (app.getQuestionPage() - 1 == 0)
                {
                    MainExamActivity.this.pre.setEnabled(false);
                }

                if (app.getQuestionPage() > 0)
                {
                    MainExamActivity.this.next.setEnabled(true);

                    int questionType = app.getQuestions()
                            .get(app.getQuestionPage() - 1).getIsImage();

                    if (questionType == 0)
                    {
                        addTitleQuestionText();

                        showQuestion(app.getQuestionPage() - 1);
                    }
                    else
                    {
                        addTitleQuestionImage();
                        showQuestionImage(app.getQuestionPage() - 1);
                    }
                }

            }
        });

        next = (Button) this.findViewById(R.id.btnNextMainExam);

        next.setOnClickListener(new OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                // TODO Auto-generated method stub

                // TextView textViewTitle =
                // (TextView)TrainningActivity.this.findViewById(R.id.txvtitleTrainning);
                // Log.d("abc1", textViewTitle.getHeight()+"");

                AppManager app = AppManager.getInstance();
                if (app.getQuestionPage() + 1 == app.getQuestions().size() - 1)
                {
                    MainExamActivity.this.next.setEnabled(false);
                }

                if (app.getQuestionPage() < app.getQuestions().size() - 1)
                {
                    MainExamActivity.this.pre.setEnabled(true);

                    int questionType = app.getQuestions()
                            .get(app.getQuestionPage() + 1).getIsImage();

                    if (questionType == 0)
                    {
                        addTitleQuestionText();

                        showQuestion(app.getQuestionPage() + 1);
                    }
                    else
                    {
                        addTitleQuestionImage();
                        showQuestionImage(app.getQuestionPage() + 1);
                    }

                }

                // Log.d("abc2", textViewTitle.getHeight()+"");
            }
        });

        rGroup = (RadioGroup) this.findViewById(R.id.rdoGroupResultsMainExam);
        rGroup.clearCheck();

        result1 = (RadioButton) this.findViewById(R.id.radio0MainExam);
        result1.setOnCheckedChangeListener(new OnCheckedChangeListener()
        {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                    boolean isChecked)
            {
                // TODO Auto-generated method stub
                if (isChecked)
                {
                    ExamDetail ex = AppManager.getInstance().getResultBoard()
                            .getExamDetails()
                            .get(AppManager.getInstance().getQuestionPage());

                    Log.d("abc", "1" + ex.getResult());
                    ex.setResult(ExamDetail.ANSWER_ONE);
                }
            }
        });

        result2 = (RadioButton) this.findViewById(R.id.radio1MainExam);
        result2.setOnCheckedChangeListener(new OnCheckedChangeListener()
        {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                    boolean isChecked)
            {
                // TODO Auto-generated method stub
                if (isChecked)
                {
                    ExamDetail ex = AppManager.getInstance().getResultBoard()
                            .getExamDetails()
                            .get(AppManager.getInstance().getQuestionPage());

                    Log.d("abc", "2" + ex.getResult());
                    ex.setResult(ExamDetail.ANSWER_TWO);
                }
            }
        });

        result3 = (RadioButton) this.findViewById(R.id.radio2MainExam);
        result3.setOnCheckedChangeListener(new OnCheckedChangeListener()
        {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                    boolean isChecked)
            {
                // TODO Auto-generated method stub
                if (isChecked)
                {
                    ExamDetail ex = AppManager.getInstance().getResultBoard()
                            .getExamDetails()
                            .get(AppManager.getInstance().getQuestionPage());

                    Log.d("abc", "3" + ex.getResult());
                    ex.setResult(ExamDetail.ANSWER_THREE);
                }
            }
        });

        result4 = (RadioButton) this.findViewById(R.id.radio3MainExam);
        result4.setOnCheckedChangeListener(new OnCheckedChangeListener()
        {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                    boolean isChecked)
            {
                // TODO Auto-generated method stub
                if (isChecked)
                {
                    ExamDetail ex = AppManager.getInstance().getResultBoard()
                            .getExamDetails()
                            .get(AppManager.getInstance().getQuestionPage());

                    Log.d("abc", "4" + ex.getResult());
                    ex.setResult(ExamDetail.ANSWER_FOUR);
                }

            }
        });

        finish = (Button) this.findViewById(R.id.btnFinishMainExam);
        finish.setOnClickListener(new OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                // TODO Auto-generated method stub

                switchToResultScreen();

                // AppManager.getInstance().getResultBoard().caculateTotalScore();

                // new PostScoreTrain(MainExamActivity.this).execute();

                // TrainningActivity.this.finish();

                // Intent intent = new Intent(TrainningActivity.this,
                // ResultActivity.class);
                // TrainningActivity.this.startActivity(intent);
            }
        });

        mana.setTimeRemain(0);

        int questionType = mana.getQuestions().get(0).getIsImage();
        if (questionType == 0)
        {
            addTitleQuestionText();
            showQuestion(0);
        }
        else
        {
            addTitleQuestionImage();
            showQuestionImage(0);
        }

        // //

        TableLayout tableQuestions = (TableLayout) this
                .findViewById(R.id.tableQuestions);

        int numberOfQuestions = mana.getQuestions().size();
        int row = numberOfQuestions / 6;
        int exQuestion = numberOfQuestions % 6;

        for (int i = 0; i < row; i++)
        {
            TableRow tr = (TableRow) View.inflate(this,
                    R.drawable.table_row_train, null);
            for (int j = 0; j < 6; j++)
            {
                LinearLayout bl = (LinearLayout) View.inflate(this,
                        R.drawable.table_row_button_train, null);
                Button b = (Button) bl.getChildAt(0);
                b.setText(String.valueOf(i * 6 + j + 1));
                b.setOnClickListener(new OnClickListener()
                {

                    @Override
                    public void onClick(View v)
                    {
                        // TODO Auto-generated method stub
                        AppManager app = AppManager.getInstance();

                        String label = ((Button) v).getText().toString();
                        int value = Integer.parseInt(label) - 1;

                        int questionType = app.getQuestions().get(value)
                                .getIsImage();

                        if (questionType == 0)
                        {
                            addTitleQuestionText();

                            showQuestion(value);
                        }
                        else
                        {
                            addTitleQuestionImage();
                            showQuestionImage(value);
                        }

                        disableEnableButtonPreNext(value == AppManager
                                .getInstance().getQuestions().size() - 1 ? -1
                                : value);
                    }
                });

                tr.addView(bl);
            }
            tableQuestions.addView(tr);
        }

        if (exQuestion > 0)
        {
            TableRow tr = (TableRow) View.inflate(this,
                    R.drawable.table_row_train, null);
            for (int j = 0; j < exQuestion; j++)
            {
                LinearLayout bl = (LinearLayout) View.inflate(this,
                        R.drawable.table_row_button_train, null);
                Button b = (Button) bl.getChildAt(0);
                b.setText(String.valueOf(row * 6 + j + 1));
                b.setOnClickListener(new OnClickListener()
                {

                    @Override
                    public void onClick(View v)
                    {
                        // TODO Auto-generated method stub
                        AppManager app = AppManager.getInstance();

                        String label = ((Button) v).getText().toString();
                        int value = Integer.parseInt(label) - 1;

                        int questionType = app.getQuestions().get(value)
                                .getIsImage();

                        if (questionType == 0)
                        {
                            addTitleQuestionText();

                            showQuestion(value);
                        }
                        else
                        {
                            addTitleQuestionImage();
                            showQuestionImage(value);
                        }

                        disableEnableButtonPreNext(value == AppManager
                                .getInstance().getQuestions().size() - 1 ? -1
                                : value);
                    }
                });
                tr.addView(bl);
            }
            tableQuestions.addView(tr);
        }

        // //
        time = (TextView) this.findViewById(R.id.txvRemainTime);
        remainTime = System.currentTimeMillis();

        new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                while (true)
                {

                    long rt = System.currentTimeMillis() - remainTime;

                    handler.sendEmptyMessage((int) (rt / 1000));

                    if (rt >= TEST_TIME_DEFAULT * 1000)
                    {
                        switchToResultScreen();
                        break;
                    }
                    try
                    {
                        Thread.sleep(10);
                    }
                    catch (InterruptedException e)
                    {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
        }).start();

        Button btnLogout = (Button) this.findViewById(R.id.btnLogout);
        btnLogout.setOnClickListener(new OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                showDialog(DIALOG_EXIT);
            }
        });

        if (!IDefine.HAVE_ADS)
        {
            RelativeLayout v = (RelativeLayout) this.findViewById(R.id.wrapper);

            final int INDEX_FOOTER_VIEW = 2;
            v.removeViewAt(INDEX_FOOTER_VIEW);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_exam, menu);
        return true;
    }

    private void switchToResultScreen()
    {

        AppManager.getInstance().getResultBoard().caculateTotalScore();

        // MainExamActivity.this.finish();

        // Intent intent = new Intent(MainExamActivity.this,
        // MainExamResultActivity.class);
        // MainExamActivity.this.startActivity(intent);

        if (!Utils.IsNetworkAvailable(MainExamActivity.this))
        {
            Utils.ShowToast(MainExamActivity.this, R.string.networknot);
            return;
        }

        new PostScoreMainExam(MainExamActivity.this).execute(Integer
                .valueOf((int) AppManager.getInstance().getTimeRemain()));
    }

    @Override
    public void onResume()
    {
        super.onResume();

        // Resume the AdView.
        mAdView.resume();

        if (timeHideNotify != 0)
        {
            if (timeShowNotify - timeHideNotify > IDefine.SESSION_TIME_OUT)
            {
                if (Utils.IsNetworkAvailable(this))
                {
                    new Logout(this).execute(AppManager.getInstance().getUser()
                            .getUsername());
                }
                // else
                // {

                // this.finish();

                // Intent intent = new Intent(this, MainActivity.class);
                // startActivityBack(intent);
                // }
            }
        }
    }

    @Override
    public void onPause()
    {
        // Pause the AdView.
        mAdView.pause();

        super.onPause();
    }

    @Override
    public void onBackPressed()
    {

        /*
         * if (System.currentTimeMillis() - timeResetbackey > 3000) {
         * backeyNumPress = 0; } if (backeyNumPress == 0) { String text =
         * this.getResources().getString(R.string.backey);
         * Toast.makeText(MainExamActivity.this, text, Toast.LENGTH_SHORT)
         * .show(); backeyNumPress = 1; timeResetbackey =
         * System.currentTimeMillis(); } else if (backeyNumPress == 1) {
         * backeyNumPress = 0; this.finish();
         * 
         * Intent intent = new Intent(this, MainMenuActivity.class);
         * startActivityBack(intent);
         * 
         * }
         */
        currentClassName = "MainExamActivity";
        showDialog(DIALOG_EXIT);
    }

    private void showQuestion(int index)
    {
        AppManager mana = AppManager.getInstance();
        mana.setQuestionPage(index);

        Question q = mana.getQuestions().get(index);

        TextView textViewTitle = (TextView) this
                .findViewById(R.id.txvtitleTrainning);
        textViewTitle.setText(q.getTitle());

        String questionstr = this.getResources().getString(
                R.string.training_question_titile);

        TextView textViewCau = (TextView) this
                .findViewById(R.id.txvCauMainExam);
        textViewCau.setText(questionstr + ": " + (index + 1) + "/"
                + mana.getQuestions().size());

        result1.setText(q.getResult1());
        result1.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);

        result2.setText(q.getResult2());
        result2.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);

        result3.setText(q.getResult3());
        result3.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);

        result4.setText(q.getResult4());
        result4.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);

        if (q.getResult1() == null || q.getResult1().equals(""))
        {
            result1.setVisibility(View.INVISIBLE);
        }
        else
        {
            result1.setVisibility(View.VISIBLE);
        }
        if (q.getResult2() == null || q.getResult2().equals(""))
        {
            result2.setVisibility(View.INVISIBLE);
        }
        else
        {
            result2.setVisibility(View.VISIBLE);
        }
        if (q.getResult3() == null || q.getResult3().equals(""))
        {
            result3.setVisibility(View.INVISIBLE);
        }
        else
        {
            result3.setVisibility(View.VISIBLE);
        }
        if (q.getResult4() == null || q.getResult4().equals(""))
        {
            result4.setVisibility(View.INVISIBLE);
        }
        else
        {
            result4.setVisibility(View.VISIBLE);
        }
        // result1.setChecked(false);
        // result2.setChecked(false);
        // result3.setChecked(false);
        // result4.setChecked(false);

        // bind result
        ExamDetail exam = mana.getResultBoard().getExamDetails().get(index);
        int choose = exam.getResult();
        switch (choose)
        {
            case ExamDetail.ANSWER_ONE:
                result1.setChecked(true);
                break;
            case ExamDetail.ANSWER_TWO:
                result2.setChecked(true);
                break;
            case ExamDetail.ANSWER_THREE:
                result3.setChecked(true);
                break;
            case ExamDetail.ANSWER_FOUR:
                result4.setChecked(true);
                break;
            default:
                rGroup.clearCheck();
                break;
        }
    }

    private void showQuestionImage(int index)
    {
        AppManager mana = AppManager.getInstance();
        mana.setQuestionPage(index);

        Question q = mana.getQuestions().get(index);

        // ImageView imgViewTitle =
        // (ImageView)this.findViewById(R.id.imgQuestionTitle);
        // imgViewTitle.setImageResource(R.drawable.q_1_1_1_1_1_1);

        String questionstr = this.getResources().getString(
                R.string.training_question_titile);

        TextView textViewCau = (TextView) this
                .findViewById(R.id.txvCauMainExam);
        textViewCau.setText(questionstr + ": " + (index + 1) + "/"
                + mana.getQuestions().size());

        result1.setText(null);
        // result1.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0,
        // R.drawable.a1_1_1_1_1_1_1);

        result2.setText(null);
        // result2.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0,
        // R.drawable.a2_1_1_1_1_1_1);

        result3.setText(null);
        // result3.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0,
        // R.drawable.a3_1_1_1_1_1_1);

        result4.setText(null);
        // result4.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0,
        // R.drawable.a4_1_1_1_1_1_1);

        if (q.getResult1() == null || q.getResult1().equals(""))
        {
            result1.setVisibility(View.INVISIBLE);
        }
        else
        {
            result1.setVisibility(View.VISIBLE);
        }
        if (q.getResult2() == null || q.getResult2().equals(""))
        {
            result2.setVisibility(View.INVISIBLE);
        }
        else
        {
            result2.setVisibility(View.VISIBLE);
        }
        if (q.getResult3() == null || q.getResult3().equals(""))
        {
            result3.setVisibility(View.INVISIBLE);
        }
        else
        {
            result3.setVisibility(View.VISIBLE);
        }
        if (q.getResult4() == null || q.getResult4().equals(""))
        {
            result4.setVisibility(View.INVISIBLE);
        }
        else
        {
            result4.setVisibility(View.VISIBLE);
        }

        new GetImages(this).execute(
                IDefine.HEADER_LINK_UPLOAD + "/upload/" + q.getTitle(),
                IDefine.HEADER_LINK_UPLOAD + "/upload/" + q.getResult1(),
                IDefine.HEADER_LINK_UPLOAD + "/upload/" + q.getResult2(),
                IDefine.HEADER_LINK_UPLOAD + "/upload/" + q.getResult3(),
                IDefine.HEADER_LINK_UPLOAD + "/upload/" + q.getResult4());

        // bind result
        ExamDetail exam = mana.getResultBoard().getExamDetails().get(index);
        int choose = exam.getResult();
        switch (choose)
        {
            case ExamDetail.ANSWER_ONE:
                result1.setChecked(true);
                break;
            case ExamDetail.ANSWER_TWO:
                result2.setChecked(true);
                break;
            case ExamDetail.ANSWER_THREE:
                result3.setChecked(true);
                break;
            case ExamDetail.ANSWER_FOUR:
                result4.setChecked(true);
                break;
            default:
                rGroup.clearCheck();
                break;
        }
    }

    @Override
    public void startActivity(Intent intent)
    {
        super.startActivity(intent);
        this.overridePendingTransition(R.anim.anim_activity_go_to_left,
                R.anim.anim_activity_go_to_right);
    }

    @Override
    public void startActivityBack(Intent intent)
    {
        super.startActivity(intent);
        this.overridePendingTransition(R.anim.anim_activity_go_to_left_back,
                R.anim.anim_activity_go_to_right_back);
    }

    private void addTitleQuestionText()
    {
        if (titleText != null)
        {
            ((LinearLayout) (titleText.getParent())).removeViewAt(0);
            titleText = null;
        }

        if (titleImg != null)
        {
            ((LinearLayout) (titleImg.getParent())).removeViewAt(0);
            titleImg = null;
        }

        titleText = (TextView) View.inflate(this,
                R.drawable.txv_question_title_train, null);

        LinearLayout titleLayout = (LinearLayout) this
                .findViewById(R.id.txvtitlelayoutMainExam);
        titleLayout.addView(titleText);
    }

    private void addTitleQuestionImage()
    {
        if (titleText != null)
        {
            ((LinearLayout) (titleText.getParent())).removeViewAt(0);
            titleText = null;
        }

        if (titleImg != null)
        {
            ((LinearLayout) (titleImg.getParent())).removeViewAt(0);
            titleImg = null;
        }

        titleImg = (ImageView) View.inflate(this,
                R.drawable.img_question_title_train, null);

        LinearLayout titleLayout = (LinearLayout) this
                .findViewById(R.id.txvtitlelayoutMainExam);
        titleLayout.addView(titleImg);
    }

    private void disableEnableButtonPreNext(int index)
    {
        final int FIRST_QUESTION = 0;
        // final int MID_QUESTION = 1;
        final int LAST_QUESTION = -1;

        switch (index)
        {
            case FIRST_QUESTION:
                MainExamActivity.this.pre.setEnabled(false);
                MainExamActivity.this.next.setEnabled(true);
                break;
            case LAST_QUESTION:
                MainExamActivity.this.pre.setEnabled(true);
                MainExamActivity.this.next.setEnabled(false);
                break;
            default:
                MainExamActivity.this.pre.setEnabled(true);
                MainExamActivity.this.next.setEnabled(true);
                break;
        }
    }

}
