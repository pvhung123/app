package vn.hlgroup.app.activity;

import java.util.ArrayList;
import java.util.List;

import vn.hlgroup.app.IDefine;
import vn.hlgroup.app.R;
import vn.hlgroup.app.adapter.CustomSpinnerAdapter;
import vn.hlgroup.app.adapter.MapItem;
import vn.hlgroup.app.controller.Logout;
import vn.hlgroup.app.controller.UpdateInfo;
import vn.hlgroup.app.model.LevelId;
import vn.hlgroup.app.singleton.AppManager;
import vn.hlgroup.app.utils.Utils;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;

public class UpdateInfoActivity extends CoreActivity
{

    private Button btnUpdateInfo;

    private Spinner levelText;

    private CustomSpinnerAdapter levelTextAdap;
    private int backeyNumPress = 0;

    private long timeResetbackey = 0;
    private AdView mAdView;

    private EditText passwordText;
    private EditText emailText;
    private EditText phoneText;

    private Button btnopenSpin;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_info);

        AppManager app = AppManager.getInstance();

        // ads
        LinearLayout layout = (LinearLayout) this
                .findViewById(R.id.adViewLayout);

        // Create a banner ad. The ad size and ad unit ID must be set before
        // calling loadAd.
        mAdView = new AdView(this);
        mAdView.setAdSize(AdSize.SMART_BANNER);
        mAdView.setAdUnitId(IDefine.ADMOB_ID);

        // Create an ad request.
        AdRequest.Builder adRequestBuilder = new AdRequest.Builder();

        // Optionally populate the ad request builder.
        // adRequestBuilder.addTestDevice(AdRequest.DEVICE_ID_EMULATOR);

        // Add the AdView to the view hierarchy.
        layout.addView(mAdView);

        // Start loading the ad.
        mAdView.loadAd(adRequestBuilder.build());

        btnUpdateInfo = (Button) this.findViewById(R.id.btnUpdateInfor);
        btnUpdateInfo.setOnClickListener(new OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                // TODO Auto-generated method stub
                // InfoActivity.this.finish();

                // Intent intent = new Intent(InfoActivity.this,
                // SendOpinionActivity.class);
                // startActivity(intent);
                // Utils.ShowToast(UpdateInfoActivity.this, "abc");

                EditText txtPReConfirm = (EditText) UpdateInfoActivity.this
                        .findViewById(R.id.txtPReConfirm);

                int levelId = ((MapItem) levelText.getSelectedItem()).getId();

                if (levelId == -1)
                {
                    Utils.ShowToast(
                            UpdateInfoActivity.this,
                            UpdateInfoActivity.this.getResources().getString(
                                    R.string.chooselevelnotyet));
                }
                else
                    if (!txtPReConfirm.getText().toString()
                            .equals(passwordText.getText().toString()))
                    {
                        Utils.ShowToast(UpdateInfoActivity.this,
                                UpdateInfoActivity.this.getResources()
                                        .getString(R.string.wrongconfirmpass));
                    }
                    else
                    {

                        Utils.WriteData(UpdateInfoActivity.this, "passwordtmp",
                                passwordText.getText().toString());

                        StringBuilder error = new StringBuilder();

                        UpdateInfo u = new UpdateInfo(UpdateInfoActivity.this);
                        if (u.validate(passwordText.getText().toString(),
                                emailText.getText().toString(), phoneText
                                        .getText().toString(), error))
                        {
                            if (!Utils
                                    .IsNetworkAvailable(UpdateInfoActivity.this))
                            {
                                Utils.ShowToast(UpdateInfoActivity.this,
                                        R.string.networknot);
                                return;
                            }

                            u.execute(passwordText.getText().toString(), String
                                    .valueOf(((MapItem) levelText
                                            .getSelectedItem()).getId()),
                                    emailText.getText().toString(), phoneText
                                            .getText().toString());
                        }
                        else
                        {
                            Toast.makeText(UpdateInfoActivity.this, error,
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
            }
        });

        // for knowledge spinner
        List<MapItem> list = new ArrayList<MapItem>();

        ArrayList<LevelId> levelid = AppManager.getInstance().getLevelId();

        int len = levelid.size();

        for (int i = 0; i < len; i++)
        {
            LevelId t = levelid.get(i);
            MapItem mi = new MapItem(t.getId(), t.getName());
            list.add(mi);

            if (app.getUser().getLevel_id() == t.getId())
            {

            }
        }
        levelText = (Spinner) this.findViewById(R.id.spinLevelUp);
        levelTextAdap = new CustomSpinnerAdapter(this,
                R.drawable.custom_spinner_item, list);// SpinnerAdapter don't
                                                      // use
                                                      // R.drawable.custom_spinner_item
        levelTextAdap
                .setDropDownViewResource(R.drawable.custom_spinner_dropdown_item);// SpinnerAdapter
                                                                                  // don't
                                                                                  // use
                                                                                  // R.drawable.custom_spinner_dropdown_item
        levelText.setAdapter(levelTextAdap);

        levelText.setOnItemSelectedListener(new OnItemSelectedListener()
        {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                    int arg2, long arg3)
            {
                UpdateInfoActivity.this.levelTextAdap.getItem(arg2);

                // Toast.makeText(TrainOptionActivity.this,
                // String.valueOf(item.getId()), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0)
            {
                // TODO Auto-generated method stub

            }

        });

        for (int i = 0; i < len; i++)
        {
            LevelId t = levelid.get(i);

            if (app.getUser().getLevel_id() == t.getId())
            {
                levelText.setSelection(i);
                break;
            }
        }

        btnopenSpin = (Button) this.findViewById(R.id.btnopenSpin);
        btnopenSpin.setOnClickListener(new OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                // TODO Auto-generated method stub
                levelText.performClick();
            }
        });

        passwordText = (EditText) this.findViewById(R.id.txtPRe);
        passwordText.setText(Utils.ReadData(this, "password"));

        EditText txtPReConfirm = (EditText) this
                .findViewById(R.id.txtPReConfirm);
        txtPReConfirm.setText(Utils.ReadData(this, "password"));

        emailText = (EditText) this.findViewById(R.id.txtEmailRe);
        emailText.setText(app.getUser().getEmail());

        phoneText = (EditText) this.findViewById(R.id.txtPhoneRe);
        phoneText.setText(app.getUser().getPhone());

        Button btnLogout = (Button) this.findViewById(R.id.btnLogout);
        btnLogout.setOnClickListener(new OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                showDialog(DIALOG_EXIT);
            }
        });

        if (!IDefine.HAVE_ADS)
        {
            RelativeLayout v = (RelativeLayout) this.findViewById(R.id.wrapper);

            final int INDEX_FOOTER_VIEW = 2;
            v.removeViewAt(INDEX_FOOTER_VIEW);
        }
    }

    @Override
    public void onBackPressed()
    {
        /*
         * if (System.currentTimeMillis() - timeResetbackey > 3000) {
         * backeyNumPress = 0; } if (backeyNumPress == 0) { String text =
         * this.getResources().getString(R.string.backey); //
         * Toast.makeText(RegisterActivity.this, //
         * "Ban co muon thoat khoi man hinh", Toast.LENGTH_SHORT).show();
         * Utils.ShowToast(this, text); backeyNumPress = 1; timeResetbackey =
         * System.currentTimeMillis(); } else if (backeyNumPress == 1) {
         * backeyNumPress = 0; this.finish();
         * 
         * Intent intent = new Intent(this, InfoActivity.class);
         * startActivityBack(intent);
         * 
         * }
         */

        currentClassName = "UpdateInfoActivity";
        showDialog(DIALOG_EXIT);

    }

    @Override
    public void onResume()
    {
        super.onResume();

        // Resume the AdView.
        mAdView.resume();

        if (timeHideNotify != 0)
        {
            if (timeShowNotify - timeHideNotify > IDefine.SESSION_TIME_OUT)
            {
                if (Utils.IsNetworkAvailable(this))
                {
                    new Logout(this).execute(AppManager.getInstance().getUser()
                            .getUsername());
                }
                // else
                // {

                // this.finish();

                // Intent intent = new Intent(this, MainActivity.class);
                // startActivityBack(intent);
                // }
            }
        }
    }

    @Override
    public void onPause()
    {
        // Pause the AdView.
        mAdView.pause();

        super.onPause();
    }

    @Override
    public void startActivity(Intent intent)
    {
        super.startActivity(intent);
        this.overridePendingTransition(R.anim.anim_activity_go_to_left,
                R.anim.anim_activity_go_to_right);
    }

    @Override
    public void startActivityBack(Intent intent)
    {
        super.startActivity(intent);
        this.overridePendingTransition(R.anim.anim_activity_go_to_left_back,
                R.anim.anim_activity_go_to_right_back);
    }
}
