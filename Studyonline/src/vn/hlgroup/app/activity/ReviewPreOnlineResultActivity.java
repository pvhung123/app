package vn.hlgroup.app.activity;

import vn.hlgroup.app.IDefine;
import vn.hlgroup.app.R;
import vn.hlgroup.app.controller.Logout;
import vn.hlgroup.app.singleton.AppManager;
import vn.hlgroup.app.utils.Utils;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;

public class ReviewPreOnlineResultActivity extends CoreActivity
{

    private int backeyNumPress = 0;

    private long timeResetbackey = 0;

    // TextView titleCompetition;

    TextView txvScore;
    // EditText titleTime;
    // EditText titlenum;

    // Button btnStart;
    // Button btnPre;
    // Button btnNext;

    // int competitionPage = 0;
    private AdView mAdView;

    // Button btnDetailResult;

    Toast mToast = null;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review_pre_online_result);

        // competitionPage = getIntent().getExtras().getInt("selectitem");

        // ads
        LinearLayout layout = (LinearLayout) this
                .findViewById(R.id.adViewLayout);

        // Create a banner ad. The ad size and ad unit ID must be set before
        // calling loadAd.
        mAdView = new AdView(this);
        mAdView.setAdSize(AdSize.SMART_BANNER);
        mAdView.setAdUnitId(IDefine.ADMOB_ID);

        // Create an ad request.
        AdRequest.Builder adRequestBuilder = new AdRequest.Builder();

        // Optionally populate the ad request builder.
        // adRequestBuilder.addTestDevice(AdRequest.DEVICE_ID_EMULATOR);

        // Add the AdView to the view hierarchy.
        layout.addView(mAdView);

        // Start loading the ad.
        mAdView.loadAd(adRequestBuilder.build());

        this.getIntent().getExtras().getBoolean("isExpired");

        String title = this.getIntent().getExtras().getString("score");
        this.getIntent().getExtras().getInt("id");

        txvScore = (TextView) this.findViewById(R.id.txvScore);
        // titleTime = (EditText)this.findViewById(R.id.txvTime);
        // titlenum = (EditText)this.findViewById(R.id.txvNumberQOnline);

        // if (!isExpired)
        // {
        // btnDetailResult.setEnabled(false);
        // }

        // btnDetailResult.setId(id);
        /*
         * btnDetailResult.setOnClickListener(new OnClickListener() {
         * 
         * @Override public void onClick(View v) { // TODO Auto-generated method
         * stub if (!Utils
         * .IsNetworkAvailable(ReviewPreOnlineResultActivity.this)) {
         * Utils.ShowToast(ReviewPreOnlineResultActivity.this,
         * R.string.networknot); return; }
         * 
         * new GetOnlineQuestionDid(ReviewPreOnlineResultActivity.this)
         * .execute(v.getId()); } });
         */

        txvScore.setText(title);
        // titleTime.setText(time);
        // titlenum.setText(num);

        // titleCompetition =
        // (TextView)this.findViewById(R.id.txvtitleCompetition);

        // titleSubject = (EditText)this.findViewById(R.id.txvSubject);
        // titleTime = (EditText)this.findViewById(R.id.txvTime);
        // titlenum = (EditText)this.findViewById(R.id.txvNumberQOnline);
        // btnStart = (Button)this.findViewById(R.id.btnStartOnlineOption);
        // btnPre = (Button)this.findViewById(R.id.btnPreOnlineOption);
        // btnNext = (Button)this.findViewById(R.id.btnNextOnlineOption);

        /*
         * OnClickListener onClick = new OnClickListener() {
         * 
         * @Override public void onClick(View v) { // TODO Auto-generated method
         * stub AppManager app = AppManager.getInstance();
         * 
         * int id = ((Button)v).getId(); switch(id) { case
         * R.id.btnStartOnlineOption:
         * 
         * 
         * app.setIdOfOnlineExam(competitionPage);
         * 
         * if(!Utils.IsNetworkAvailable(ReviewPreOnlineResultActivity.this)) {
         * Utils.ShowToast(ReviewPreOnlineResultActivity.this,
         * R.string.networknot); return; }
         * 
         * new
         * GetQuestionOnline(ReviewPreOnlineResultActivity.this).execute(app.
         * getOnlineExams().get(competitionPage).getId()); break; case
         * R.id.btnPreOnlineOption: if(competitionPage > 0) { competitionPage--;
         * showCompetition(competitionPage);
         * 
         * if(competitionPage == 0) { btnPre.setEnabled(false);
         * btnNext.setEnabled(true); } } break; case R.id.btnNextOnlineOption:
         * if(competitionPage < app.getOnlineExams().size() - 1) {
         * competitionPage++; showCompetition(competitionPage);
         * 
         * if(competitionPage == app.getOnlineExams().size() - 1) {
         * btnPre.setEnabled(true); btnNext.setEnabled(false); } } break; }
         * 
         * 
         * 
         * 
         * } };
         */

        // btnStart.setOnClickListener(onClick);
        // btnPre.setOnClickListener(onClick);
        // btnNext.setOnClickListener(onClick);

        /*
         * AppManager app = AppManager.getInstance();
         * if(app.getOnlineExams().size()>1) {
         * 
         * if(competitionPage == 0) { btnPre.setEnabled(false);
         * btnNext.setEnabled(true); } else if(competitionPage ==
         * app.getOnlineExams().size() - 1) { btnPre.setEnabled(true);
         * btnNext.setEnabled(false); } else { btnPre.setEnabled(true);
         * btnNext.setEnabled(true); } } else { btnPre.setEnabled(false);
         * btnNext.setEnabled(false); }
         * 
         * showCompetition(competitionPage);
         */

        Button btnLogout = (Button) this.findViewById(R.id.btnLogout);
        btnLogout.setOnClickListener(new OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                showDialog(DIALOG_EXIT);
            }
        });

        if (!IDefine.HAVE_ADS)
        {
            RelativeLayout v = (RelativeLayout) this.findViewById(R.id.wrapper);

            final int INDEX_FOOTER_VIEW = 2;
            v.removeViewAt(INDEX_FOOTER_VIEW);
        }

    }

    @Override
    public void onResume()
    {
        super.onResume();

        // Resume the AdView.
        mAdView.resume();

        if (timeHideNotify != 0)
        {
            if (timeShowNotify - timeHideNotify > IDefine.SESSION_TIME_OUT)
            {
                if (Utils.IsNetworkAvailable(this))
                {
                    new Logout(this).execute(AppManager.getInstance().getUser()
                            .getUsername());
                }
                // else
                // {

                // this.finish();

                // Intent intent = new Intent(this, MainActivity.class);
                // startActivityBack(intent);
                // }
            }
        }
    }

    @Override
    public void onPause()
    {
        // Pause the AdView.
        mAdView.pause();

        super.onPause();
    }

    @Override
    public void startActivity(Intent intent)
    {
        super.startActivity(intent);
        this.overridePendingTransition(R.anim.anim_activity_go_to_left,
                R.anim.anim_activity_go_to_right);
    }

    @Override
    public void startActivityBack(Intent intent)
    {
        super.startActivity(intent);
        this.overridePendingTransition(R.anim.anim_activity_go_to_left_back,
                R.anim.anim_activity_go_to_right_back);
    }

    @Override
    public void onBackPressed()
    {
        /*
         * if (System.currentTimeMillis() - timeResetbackey > 3000) {
         * backeyNumPress = 0; } if (backeyNumPress == 0) { String text =
         * this.getResources().getString(R.string.backey); mToast =
         * Toast.makeText(ReviewPreOnlineResultActivity.this, text,
         * Toast.LENGTH_SHORT); mToast.show(); backeyNumPress = 1;
         * timeResetbackey = System.currentTimeMillis(); } else if
         * (backeyNumPress == 1) { backeyNumPress = 0; this.finish();
         * 
         * Intent intent = new Intent(this, ListResultOnlineActivity.class);
         * startActivityBack(intent);
         * 
         * }
         */
        currentClassName = "ReviewPreOnlineResultActivity";
        showDialog(DIALOG_EXIT);
    }

    /*
     * private void showCompetition(int index) { AppManager app =
     * AppManager.getInstance(); ArrayList<ExamOnline> exams =
     * app.getOnlineExams();
     * 
     * ExamOnline e = exams.get(index);
     * 
     * titleCompetition.setText(e.getTitle());
     * 
     * if(e.getSubjectId() == 0) { titleSubject.setText("Tong hop"); } else {
     * ArrayList<Subject> subjectAll = app.getSubjectsAll(); int len =
     * subjectAll.size(); for(int i=0;i<len;i++) { if(e.getSubjectId() ==
     * subjectAll.get(i).getId()) {
     * titleSubject.setText(subjectAll.get(i).getName()); break; } } }
     * 
     * titleTime.setText(String.valueOf(e.getTimeToTest())+" phut");
     * titlenum.setText(String.valueOf(e.getNumberOfQuestion())+" cau");
     * 
     * }
     */
}
